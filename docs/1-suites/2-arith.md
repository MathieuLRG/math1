# Suites arithmétiques

!!! example "Exemple simple"
    ![graph](./images/4-graph.png){ .bordure }

    Graphique de la suite que l'on peut définir par

    - $u_n = 2n-3$ pour $n\in\mathbb N$
        - comme montré à la page précédente
    - ou alors, de manière équivalente, par
        - $u_0 = -3$
        - $u_{n+1} = u_n + 2$ pour $n\in\mathbb N$
    
    ![rec1](./images/rec-1.png){ .bordure } ![rec2](./images/rec-2.png){ .bordure }
    

Les **suites arithmétiques**[^1] sont les suites que l'on peut définir :

[^1]: :material-wikipedia: [Suite arithmétique](https://fr.wikipedia.org/wiki/Suite_arithm%C3%A9tique)


- avec une fonction affine explicite ;
- ou bien, de manière équivalente,
    - le premier terme est donné
    - un terme suivant se calcule comme la somme du terme courant et d'une valeur constante, nommée la raison.

!!! note "Propriétés"
    * $(u)$ est arithmétique $\iff$ il existe $a, b \in \mathbb R$ tel que pour tout $n\in\mathbb N$, $u_n = a\times n + b$.
    
        Dans ce cas, on a $u_0 = b$, et pour tout $n\in \mathbb N$,  
        $u_{n+1}= a\times(n+1) + b$  
        $u_{n+1}= an + a + b$  
        $u_{n+1}= an+b + a$.  
        $u_{n+1}= u_n + a$.
        
        On dit que cette suite est arithmétique de raison $a$ et de premier terme $b$.
    
    * On peut définir une suite arithmétique par la donnée de sa raison (souvent notée $r$) et de son premier terme $u_0$.
    
        Dans ce cas, on a pour tout $n\in \mathbb N \quad u_{n+1} - u_n = r \quad \textrm{et} \quad u_n = u_0 + rn$.  
        On a aussi pour tout $n\in \mathbb N \quad u_{n+1} = u_n + r$
        


!!! question "Exercice type"
    $(u_n)$ est une suite arithmétique telle que $u_{10} = 1757$ et $u_{100} = 5537$, déterminer $u_n$ en fonction de $n$.

!!! success "Réponse"
    $u_n$ est arithmétique, donc il existe des nombres $a$ et $b$ tels que pour tout $n\in \mathbb N \quad u_n = an+b$

    On a $u_{100} = 100a +b = 5537$, et  $u_{10} = 10a +b = 1757$, par soustraction on obtient :  
    $u_{100} - u_{10} = 100a - 10a = 5537 - 1757$, d'où l'on tire  
    $90a = 3780$, et donc $a = \dfrac{3780}{90} = 42$.  

    On a donc, pour tout $n\in \mathbb N \quad u_n = 42n+b$, et avec $n=10$, on a:  
    $u_{10} = 42\times10 + b = 1757$, d'où $b = 1757 - 420 = 1337$.  
    On aurait également trouvé $b=1337$, de la même manière, en utilisant $u_{100} = 5537$.

    _Conclusion_ : pour tout $n\in \mathbb N \quad u_n = 42n + 1337$.

    !!! note "Vérification"

        - $u_{10} = 42×10 + 1337 = 420+1337 = 1757$ ; OK
        - $u_{100} = 42×100 + 1337 = 4200 + 1337 = 5537$ ; OK

        :warning: Penser à faire ce genre de vérification, c'est rapide et rassurant !

!!! info "Culture _geek_"
    * $42$ est la réponse à [la grande question sur la vie, l'univers et le reste](https://fr.wikipedia.org/wiki/La_grande_question_sur_la_vie,_l%27univers_et_le_reste).
    * $1337$ est un [code](https://fr.wikipedia.org/wiki/Leet_speak) qui se prononce _Elite_, où l'on remplace les lettres par d'autres symboles.
        * L est remplacé par `1`, en anglais on prononce comme : elle
        * E est remplacé par `3`, en anglais on prononce comme : i
        * T est remplacé par `7`, en anglais on prononce comme : t
        * (elle, i, i, t) se prononce alors comme _elite_ en anglais.



## Somme de termes consécutifs d'une suite arithmétique

### Étape 1 : Sn = 0+1+2+3+4+…+n

Notre objectif est d'établir une formule pour $S_n$. On l'écrit dans un sens, puis à l'envers, on ajoute les termes qui se correspondent de façon à obtenir $2S_n$ qui est la somme de $(n+1)$ termes tous égaux.

|$S_n$|$=0$|$+1$|$+2$|$\cdots$|$+(n-2)$|$+(n-1)$|$+n$|
|----:|----|----|----|--------|--------|--------|----|
|$+S_n$|$=n$|$+(n-1)$|$+(n-2)$|$\cdots$|$+3$|$+2$|$+1$|
|$2S_n$|$=n$|$+n$|$+n$|$\cdots$|$+n$|$+n$|$+n$|

:warning: Il y a $(n+1)$ termes de $0$ à $n$ inclus.

On tire $2S_n = n(n+1)$.

!!! success "Conclusion"
    Pour tout $n\in \mathbb N \quad 0+1+2+3+4+\cdots+n = \dfrac{n(n+1)}2$.

### Cas général

Pour une suite arithmétique $(u)$ de premier terme $u_0$ et de raison $r$, la somme des premiers termes est :

$$S_n = u_0 + u_1 + u_2 + \cdots + u_{n-2}+ u_{n-1}+ u_n$$

$$S_n = u_n + u_{n-1} + u_{n-2} + \cdots + u_{2}+ u_{1}+ u_0$$

Remarquons une chose :

- $u_{n} + u_0 = u_0+n×r+u_0 = 2u_0+u_n$
- $u_{n-1} + u_1 = \left(u_0 + (n-1)×r\right) + (u_0 + 1×r) = 2u_0 + n×r = u_0 + u_n$
- $u_{n-2} + u_2 = \left(u_0 + (n-2)×r\right) + (u_0 + 2×r) = 2u_0 + n×r = u_0 + u_n$
- $u_{n-i} + u_i = \left(u_0 + (n-i)×r\right) + (u_0 + i×r) = 2u_0 + n×r = u_0 + u_n$, de manière générale.

De sorte que :

$$2S_n = (n+1)×(u_0+u_n)$$

$$S_n = (n+1)×\dfrac{u_0+u_n}2$$

Que l'on peut traduire en :

!!! tip "À retenir"
    La somme de termes d'une suite **arithmétique** est la moyenne entre le premier et le dernier terme, multiplié par le nombre de termes.

!!! question "Exemple"
    Quelle est la somme $2000 + 2100 + 2200 + \cdots + 50000$ ?

!!! success "Réponse"
    Il s'agit bien d'une somme de termes d'une suite arithmétique de raison $100$. La question délicate étant : combien y a-t-il de termes ?

    Cette somme vaut $\dfrac{2000 + 50000}2 × q$ où $q$ est le nombre de termes de cette somme.

    On peut dire que la suite arithmétique associée est $(u)$, avec $u_0=2000$ et de raison $100$.

    Le dernier terme considéré est $u_n = 2000 + n×100 = 50000$, d'où $n = \dfrac{48000}{100} = 480$.

    Il y a donc $q=81$ termes de $u_0$ à $u_{480}$.

    La somme vaut donc $\dfrac{2000 + 50000}2 × 481 = 26000×481 = 12\,506\,000$.

!!! success "Variante"
    La somme est constituée des multiples de $100$, de $2000$ à $50000$, on peut dire que :

    - $S = 2000 + 2100 + 2200 + \cdots + 50000$
    - $S = (0 + 100 + 200 + \cdots + 50000) - (0 + 100 + 200 + \cdots + 1900)$
    - $S = 100(0 + 1 + 2 + \cdots + 500) - 100×(0 + 1 + 2 + \cdots + 19)$
    - $S = 100×\dfrac{500×501}2 - 100×\dfrac{19×20}2$
    - $S = 12\,525\,000 - 19\,000 = 12\,506\,000$


!!! success "Variante avec Python"

    Cliquez sur chaque bulle pour avoir une explication.

    ```python
    somme = 0  # (1)!
    u = 2000  # (2)!
    while u <= 50000:  # (3)!
        somme = somme + u  # (4)!
        u = u + 100  # (5)!

    print(somme)  # (6)!
    ```

    1. On initialise `somme` à `0`
    2. On initialise `u` à `200`
    3. On répète en boucle **tant que** `u < 50000`
    4. `somme` augmente de `u`
    5. `u` augmente de `100`
    6. On affiche `somme`

    ```output
    12506000
    ```

    Pour augmenter la valeur d'une variable, on écrit que sa nouvelle valeur est égale à son ancienne valeur plus la quantité à augmenter, par exemple `u = u + 100` n'est pas une équation, c'est une affectation. La nouvelle valeur de `u` est égale à l'ancienne valeur plus 100. Ainsi `u` a augmenté de 100.

!!! info "Quelle méthode choisir ?"
    - La version Python est généralisable **facilement** à tout type de suite.
    - Les versions mathématiques permettent d'obtenir des résultats théoriques dans certains **cas particuliers**.

    Il faut savoir faire les deux méthodes !
    
