# Générations avec une fonction

Dans la suite, on prendra pour exemple la suite définie avec $u_n = f(n)$, où $f(x) = \sqrt{x^2+1} - x$

## Calcul de termes

On peut, si la suite est définie par une fonction, calculer facilement un terme quelconque.

Voici plusieurs méthodes.

### Exemple 1

Cliquer sur les bulles pour des explications

```python
from math import sqrt   # (1)!

def u(x):   # (2)!
    return sqrt(x**2 + 1) - x

# exemple 1
for n in [10, 100, 1000]:   # (3)!
    print("Avec n =", n, " , on a u(n) =", u(n))
```

1. La première ligne permet d'utiliser _sqrt_, à partir du module _math_.
(_**sq**uare **r**oo**t**_ : la racine carrée, en anglais.)
2. On définit ensuite la suite $(u_n)_{n\in \mathbb N}$ avec une fonction.
3. On calcule ensuite trois termes, et on les affiche.
  
    - Avec n = 10  , on a u(n) = 0.049875621120889946
    - Avec n = 100  , on a u(n) = 0.004999875006248544
    - Avec n = 1000  , on a u(n) = 0.0004999998750463419


### Exemple 2

```python
U = []  # (1)!
for n in range(4):
    U.append(u(n))  # (2)!

print(U)  # (3)!
print(U[3]) # (4)!
```

1. On peut construire une liste vide, et ensuite
2. y ajouter élément par élément, les 4 premiers termes de la suite.
3. On affiche ensuite la liste construite,
4. puis le quatrième terme. ($u_0$ en est le premier terme.)


```output
[1.0, 0.41421356237309515, 0.2360679774997898, 0.16227766016837952]
0.16227766016837952
```


### Exemple 3

```python
V = [u(n) for n in range(4)]
print(V)
print(V[3])
```

```output
[1.0, 0.41421356237309515, 0.2360679774997898, 0.16227766016837952]
0.16227766016837952
```

Il s'agit d'une variante où l'on construit la [liste en compréhension](https://fr.wikipedia.org/wiki/Liste_en_compr%C3%A9hension). Cette méthode est aussi au programme de la **spé maths en première**.

### Exemple 4 (spécial NSI)

```python
W = list(map(u, range(4)))
print(W)
print(W[3])
```

```output
[1.0, 0.41421356237309515, 0.2360679774997898, 0.16227766016837952]
0.16227766016837952
```

Il s'agit d'une variante fonctionnelle. Cette méthode peut être très intéressante si on ne veut pas, justement, construire la liste, mais plutôt égrainer chaque terme l'un après l'autre. Cette méthode est pour les élèves en terminale NSI.

## À vous

Voici un IDE (un environnement de travail) pour faire vos expériences :

{{ IDE() }}
