# Symbole Sigma

!!! cite inline "Une lettre grecque"
    $\Sigma$ est la lettre grecque qui correspond au S capital.

    $\sigma$ est la lettre grecque qui correspond au s minuscule. On s'en sert en statistiques.

Le symbole $\Sigma$ permet d'écrire des sommes de manière plus concise.

$$\sum_{i=0}^{14} 3^i = 3^0 +3^1 +3^2 +3^3 + \cdots +3^{14}$$

> La somme pour $i$ allant de $0$ à $14$ (inclus) de $3^i$

Le symbole sigma permet d'écrire de manière plus rigoureuse les sommes décrites avec le symbole $\cdots$.

## Avec des suites arithmétiques

### Exemples

$$\sum_{i=0}^4 100i+3 = 3+103+203+303+403 = \frac{3+403}2×5$$

$$\sum_{i=10}^{14} 100i+3 = 1003+1103+1203+1303+1403 = \frac{1003+1403}2×5$$



### Cas général

Si $(u)$ est une suite arithmétique, alors

$$\sum_{i=n}^m u_i = \frac{u_n + u_m}2×(m-n+1)$$

!!! note "Explication"

    $\sum_{i=n}^m u_i = u_n + u_{n+1} + u_{n+2} + u_{n+3} + \cdots + u_{m-1} + u_{m}$

    Il y a $m-n+1$ termes. **Vérifiez !**

    La somme est égale à la moyenne du premier et du dernier terme, multiplié par le nombre de termes. Formule déjà vue.

    :warning: **Uniquement** pour des suites arithmétiques.

## Avec une suite géométrique

### Exemples

$$\sum_{i=0}^4 3^i = 1 + 3 + 9 + 27 + 81 = \frac{3×81 - 1}{3-1}$$

$$\sum_{i=0}^4 10×3^i = 10 + 30 + 90 + 270 + 810 = 10×\frac{3×81 - 1}{3-1}$$

### Cas presque général

Si $q\neq 1$, alors

$$\sum_{i=0}^n a×q^i = a×\frac{q^{n+1} - 1}{q - 1}$$


## Techniques usuelles

### Termes constants

$$\sum_{i=n}^m k = k×(m-n+1)$$

Il s'agit de faire la somme de $(m-n+1)$ termes constants égaux à $k$

Exemple :

$$\sum_{i=10}^{14} 7 = 7×(14-10+1)$$

### Factorisation

$$\sum_{i=n}^m (k×u_i) = k×\sum_{i=n}^m u_i$$

Il s'agit ici de la distributivité de la multiplication par rapport à l'addition.

Exemple :

$$\sum_{i=0}^4 10×3^i = 10×\sum_{i=0}^4 3^i$$


### Découpage des termes

$$\sum_{i=n}^m (u_i + v_i) = \sum_{i=n}^m u_i + \sum_{i=n}^m v_i$$

Exemple : on peut, par exemple, donner une autre démonstration pour les sommes de suites arithmétiques, telles que $u_i = u_0+r×i$.

$$\sum_{i=0}^n (u_0 + r×i) = \sum_{i=0}^n u_0 + \sum_{i=0}^n r×i$$

$$\sum_{i=0}^n (u_0 + r×i) = (n+1)×u_0 + r×\sum_{i=0}^n i$$

$$\sum_{i=0}^n (u_0 + r×i) = (n+1)×u_0 + r×\frac{n(n+1)}2$$

$$\sum_{i=0}^n (u_0 + r×i) = \frac{n+1}2 × (2u_0 + r×n)$$

$$\sum_{i=0}^n (u_0 + r×i) = \frac{(n+1) × (u_0 + u_n)}2$$

$$\sum_{i=0}^n (u_0 + r×i) = (n+1) × \frac{u_0 + u_n}2$$

On retrouve une formule déjà vue.

### Découpage pour les indices

$$\sum_{i=n}^m u_i = \sum_{i=0}^m u_i - \sum_{i=0}^{n-1} u_i$$

Cette technique permet de transformer une somme entre deux indices en deux sommes qui débutent à l'indice zéro.
