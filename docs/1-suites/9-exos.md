# Exercices corrigés

## Suites - très simple

1. $(u_n)_n$ est une suite arithmétique de premier terme $u_0=55$ et de raison $r=13$. Donner $u_{261}$.
  
    ??? success "Réponse"
        $(u_n)_n$ est une suite **arithmétique** donc de la forme $u_n = u_0+r×n$, où $r$ est la raison. On déduit

        $$u_{261} = 55+13×261 = 3448$$

2. $(v_n)_n$ est une suite géométrique de premier terme $v_0=1234$ et de raison $q=0,\!789$. Donner un arrondi de $v_{65}$ avec 4 chiffres significatifs.
  
    ??? success "Réponse"
        $(v_n)_n$ est une suite **géométrique** donc de la forme $v_n = v_0×q^n$, où $q$ est la raison. On déduit

        $$v_{65} = 1234×0,\!789^{65} \approx 2,\!520×10^{-4} \quad \text{(arrondi à 4 chiffres significatifs)}$$

3. La suite $(w_n)_n$ est définie par $w_0 = 0$, et  $w_{n+1} = 3×w_n + 2$ pour tout $n\in \mathbb N$. Donner $w_6$.
  
    ??? success "Réponse"
        Calculons de proche en proche. (Ne pas écrire « Il faut ...» ; techniquement il existe d'autres méthodes que celle-ci).

        * $w_1 = 3×0+2 = 2$
        * $w_2 = 3×2+2 = 8$
        * $w_3 = 3×8+2 = 26$
        * $w_4 = 3×26+2 = 80$
        * $w_5 = 3×80+2 = 242$
        * $w_6 = 3×242+2 = 728$
        
## Suites - simple

1. $(u_n)_n$ est une suite arithmétique telle que $u_{10}=-5$, et $u_{100}=40$. Calculer $u_1$ et $u_{1000}$.
  
    ??? success "Réponse"
        $(u_n)_n$ est une suite arithmétique, donc $u_n = u_0+r×n$, avec $r\in\mathbb R$, on sait que :

        $$\begin{cases}
        u_{10} = u_0+r×10 = -5\\
        u_{100} = u_0+r×100 = 40
        \end{cases}$$

        D'où $r×(100-10) = 40 - (-5)$, et $90r=45$, et enfin $r=\frac12$.

        Ensuite, on déduit $u_0+\frac12×10 = -5$, d'où $u_0 = -5 - 5 = -10$.

        De sorte que pour tout $n\in\mathbb N$, on a $u_n=-10 + \dfrac{n}2$.

        On déduit

        * $u_{1} = -10 + \frac12 = -9,\!5$
        * $u_{1000} = -10 + \frac{1000}2 = 490$
        
2. $(v_n)_n$ est une suite géométrique telle que $v_0=100$ et $v_2=121$. Calculer $v_3$ et $v_4$. _(Donner toutes les solutions !)_
  
    ??? success "Réponse"
        $(v_n)_n$ est une suite géométrique, donc $v_n = v_0×q^n$, avec $q\in\mathbb R$, on sait que :

        $$v_2 = 100×q^2 = 121$$

        On déduit $q^2=1,\!21$, puis qu'il y a **deux solutions** : $q=\pm\sqrt{1,\!21}=\pm1,\!1$.

        On obtient $v_3 = \pm133,\!1$ (**deux** solutions), et $v_4=146,\!41$ (une seule solution).

## Sigma - simple

Écrire sans points de suspension et calculer :

* $A = 1+2+3+4+\cdots+2021$
  
    ??? success "Réponse"
        $A = \displaystyle\sum_{n=1}^{2021} n = \frac{2021×(2021+1)}2=2\,043\,231$

* $B = 42+142+242+342+\cdots+10042$
  
    ??? success "Réponse"
        $B = (42 + 0) + (42 + 100) + (42 + 200) + \cdots + (42 + 10\,000)$

        $B = (42 + 0\times100) + (42 + 1\times100) + (42 + 2\times100) + \cdots + (42 + 100\times100)$

        $B = \displaystyle\sum_{n=0}^{100} 42+100n$
        
        $B = \displaystyle\sum_{n=0}^{100} 42+100\sum_{n=0}^{100}n$

        $B = 42×101 + 100×\dfrac{100×(100+1)}2$

        $B = 509\,242$

        Variante : il y a $101$ termes d'une suite arithmétique, donc la somme est

        $B = 101 \times \dfrac{42 + 10\,042}2 = 509\,242$

* $C = 1+5+25+125+625+\cdots+5^{12}$
  
    ??? success "Réponse"
        $C = 5^0+5^1+5^2+5^3+5^4+\cdots+5^{12}$

        $C = \displaystyle\sum_{n=0}^{12} 5^n$

        $C = \dfrac{5^{12+1}-1}{5-1}$

        $C = 305\,175\,781$

* $D = 1+3+9+27+\cdots+3\,486\,784\,401$
  
    ??? success "Réponse"
        $D = 3^0+3^1+3^2+3^3+\dots+3^{20}$

        $D = \displaystyle\sum_{n=0}^{20} 3^n$

        $D = \dfrac{3^{20+1}-1}{3-1}$

        $D = 5\,230\,176\,601$

        ---

        Variante, si on ne veut pas chercher l'indice $20$.

        On pose $n$ l'entier tel que $3^n = 3\,486\,784\,401$

        $D = 3^0+3^1+3^2+3^3+\dots+3^n$

        $D = \displaystyle\sum_{i=0}^{n} 3^i$

        $D = \dfrac{3^{n+1}-1}{3-1}$

        $D = \dfrac{3×3^n-1}{3-1}$

        $D = \dfrac{3×3\,486\,784\,401-1}{3-1}$

        $D = 5\,230\,176\,601$


## Placement à taux fixe - Python

On place $1\,000$ € sur un compte qui rapporte avec un taux annuel de $0,\;\!\!75$%.

1. Quel est le coefficient multiplicateur associé à une hausse de $0,\;\!\!75$% ?
  
    ??? success "Réponse"
        Le coefficient est $×\left(1+\dfrac{0,\!75}{100}\right) = 1,\!0075$.

2. Recopier et compléter l'algorithme ci-dessous qui détermine le nombre d'années nécessaires pour doubler le capital.
  
    ```python
    def  durée_pour_doubler(capital_initial, taux):
            capital = ???
            année = 0
            while capital < capital_initial * ???:
                    capital = (1 + taux / 100) * ???
                    année = année + 1
            return année
    ```

    ```python
    >>> # utilisations
    >>> durée_pour_doubler(1000, 0.75)
    ???
    ```
    
    ??? success "Réponse"
        ```python
        def  durée_pour_doubler(capital_initial, taux):
                capital = capital_initial  # pas nécessairement 1000...
                année = 0
                while capital < capital_initial * 2:  # on souhaite doubler !!
                        capital = (1 + taux / 100) * capital  # capital est modifié !
                        année = année + 1
                return année
        ```

        ```python
        >>> # utilisations
        >>> durée_pour_doubler(1000, 0.75)
        93
        ```

        Il y a plusieurs méthodes pour trouver le $93$. Le plus simple est de lancer le script.

3. La durée pour doubler un capital dépend-elle du capital initial, du taux ou des deux ?
  
    ??? success "Réponse"
        On a $c_n = c_0×q^n$, le capital double l'année $n$ signifie $c_n \approx 2×c_0$, d'où $c_0×q^n \approx 2×c_0$, et $q^n \approx 2$. Cette équation **ne dépend pas** de $c_0$, le capital initial, mais de $q$ qui est égal à $1+\frac{t}{100}$. Ainsi la durée pour doubler un capital dépend du taux, mais pas du capital.

## Suite arithmético-géométrique

La suite $(a_n)_n$ est définie par $a_0 = 1$ et $a_{n+1} = 5a_n + 1$, pour $n\in \mathbb N$.

1. Résoudre l'équation $l = 5×l + 1$.
  
    ??? success "Réponse"
        $-4l=1$, d'où $l=\frac{-1}4$.

2. On considère la suite $(b_n)_n$ définie par $b_n = a_n - \dfrac{-1}4$. Montrer que $(b_n)_n$ est une suite géométrique.
  
    ??? success "Réponse"
        $b_{n+1} = a_{n+1} - \dfrac{-1}4$

        $b_{n+1} = 5a_{n} + 1 - \dfrac{-1}4$

        D'autre part $a_n = b_n + \dfrac{-1}4$, d'où

        $b_{n+1} = 5\left(b_n + \dfrac{-1}4\right) + 1 - \dfrac{-1}4$

        $b_{n+1} = 5b_n + \dfrac{-5}4 + \dfrac44 + \dfrac{1}4$

        $b_{n+1} = 5b_n$ ; ainsi $(b_n)$ est géométrique de raison $5$.

3. En déduire une expression $a_n$ en fonction de $n$.
  
    ??? success "Réponse"
        On a $b_0 = a_0 - \dfrac{-1}4 = \dfrac54$.

        $a_n = b_n + \dfrac{-1}4$, et $b_n=b_0×5^n$, d'où

        $a_n = \dfrac54×5^n + \dfrac{-1}4$

4. Donner plusieurs méthodes possibles pour calculer $a_{42}$. _Les calculs ne sont pas demandés ; juste des méthodes expliquées._
  
    ??? success "Réponse"

        * On peut utiliser la formule précédente ; mais il faut l'avoir trouvée ! On obtient la réponse en deux opérations.
        * On peut calculer de proche en proche avec la définition initiale ; cela pourrait être long dans certains cas.

5. Donner plusieurs méthodes possibles pour calculer la somme des termes $a_n$ pour $n<43$.
  
    ??? success "Réponse"

        * On peut calculer la somme de proche en proche en accumulant le résultat.
        * $a_n = 5^n + \dfrac{-1}4$, donc on peut découper la somme des $a_n$ en deux sommes, il y aura une somme de termes en progression géométrique (on a une formule pour ça), et une somme de termes constants (on a aussi une formule)... On pourra obtenir une formule directe également !

## Défis

1. (Facile) Trouver $a$, $b$, $c$ tels que $\displaystyle\sum_{i=0}^{i=n}i = an^2+bn+c$.
  
    ??? success "Réponse"
        C'est le cours.

        $S_1(n) = \displaystyle\sum_{i=0}^{i=n}i = \frac{n(n+1)}2 = \frac{n^2+n}2= \frac12n^2+\frac12n+0$

        Ainsi, $a=\frac12$, $b=\frac12$, $c=0$

2. (Moyen) Trouver $b$, $c$, $d$ tels que $S_2(n) = \displaystyle\sum_{i=0}^{i=n}i^2 = \frac13n^3+bn^2+cn+d$.
  
    ??? success "Réponse"

        * On a $S_2(0)=0$, on déduit $d=0$.
        * On écrit un système de deux équations à deux inconnues $b$ et $c$, on prenant (par exemple) $n=1$, puis $n=2$.
        * $\begin{cases}
        \dfrac13×1^3 + b×1^2 + c×1 = 0^2+1^2\\
        \dfrac13×2^3 + b×2^2 + c×2 = 0^2+1^2+2^2\\
        \end{cases}$, on a traduit le problème avec $n=1$, puis $n=2$.
        * $\begin{cases}
        \dfrac13 + b + c = 1\\
        \dfrac83 + 4b + 2c = 5\\
        \end{cases}$, on a réduit, on va tout multiplie par $3$.
        * $\begin{cases}
        1 + 3b + 3c = 3\\
        8 + 12b + 6c = 15\\
        \end{cases}$
        * $\begin{cases}
        3b + 3c = 2\\
        12b + 6c = 7\\
        \end{cases}$, on a mis sous forme classique, et va multiplier la première ligne par $-2$.
        * $\begin{cases}
        -6b - 6c = -4\\
        12b + 6c = 7\\
        \end{cases}$, on ajoute les deux lignes.
        * $\begin{cases}
        6b  = 3\\
        2×3 + 6c = 7\\
        \end{cases}$, dans la deuxième ligne, on a remplacé $12b$ par $2×6b = 2×3$.
        * $\begin{cases}
        b  = \dfrac12\\
        c = \dfrac16\\
        \end{cases}$, on a trouvé $b$ et $c$.
        * Ainsi $S_2(n)=\dfrac13n^3 + \dfrac12n^2 + \dfrac16n$, ou plus simplement
        * $S_2(n)=\dfrac{2n^3 + 3n^2 + n}6$
        * Vérification
            - $\dfrac{2×5^3 + 3×5^2 + 5}6 = \dfrac{250+75+5}6 = 55$
            - Or $S_2(5)=0^2+1^2+2^2+3^2+4^2+5^2 = 0+1+4+9+16+25 = 55$

3. (Dur) Trouver une formule pour $S_3(n) = \displaystyle\sum_{i=0}^{i=n}i^3$.
  
    ??? done "Réponse"
        Ce sera un excellent exercice ! Cherchez !!! Ce n'est pas si dur, et vous progresserez.

        Indice : C'est encore un polynôme, qui commence par $\frac14n^4$, et finit aussi par $+0$. Il vous reste trois termes à trouver !

        Magie : Montrer ensuite que $S_3(n) = \left(S_1(n)\right)^2$, pour tout $n\in\mathbb N$.


## Sigma

Écrire sans suspensions les sommes suivantes et
détailler leur calcul.

1. $A = 7×5^{10} + 7×5^{11} + 7×5^{12} + \cdots + 7×5^{20}$
  
    ??? success "Réponse"

        * $\displaystyle A = \sum_{n=10}^{20} 7×5^{n}$
        * $\displaystyle A = 7×\sum_{n=10}^{20} 5^{n}$
        * $\displaystyle A = 7×\left(\sum_{n=0}^{20} 5^{n} - \sum_{n=0}^{9} 5^{n}\right)$
        * $A = 7×\left(\dfrac{5^{20+1}-1}{5-1}-\dfrac{5^{9+1}-1}{5-1}\right)$
        * $A = 7×\left(\dfrac{5^{21}-1-5^{10}+1}{5-1}\right)$
        * $A = 7×\left(\dfrac{5^{11+10}-5^{10}}{4}\right)$
        * $A = 7×5^{10}×\left(\dfrac{5^{11}-1}{4}\right)$
        * $A = 834\,465\,009\,765\,625$

        Variante

        * $\displaystyle A = 7×\sum_{n=10}^{20} 5^{n}$
        * $\displaystyle A = 7×\sum_{i=0}^{10} 5^{10+i}$, avec le changement de variable $n = 10+i$
        * $\displaystyle A = 7×\sum_{i=0}^{10} 5^{10}×5^i$
        * $\displaystyle A = 7×5^{10}×\sum_{i=0}^{10} 5^i$
        * $A = 7×5^{10}×\left(\dfrac{5^{11}-1}{4}\right)$
        * $A = 834\,465\,009\,765\,625$
        
  
2. $B = 999 + 1999 + 2999 + 3999 + \cdots + 42999$
  
    ??? success "Réponse"

        * $B = (1000 - 1) + (2000-1) + (3000-1) + (4000 - 1) + \cdots + (43000 - 1)$
        * $B = (1×1000 - 1) + (2×1000 - 1) + (3×1000 - 1) + (4×1000 - 1) + \cdots + (43×1000 - 1)$
        * $B = \displaystyle\sum_{n=1}^{43}(n×1000 - 1)$
        * $B = \displaystyle\left(\sum_{n=1}^{43}n\right)×1000 - \sum_{n=1}^{43}1$
        * $B = \displaystyle\left(\dfrac{43×(43+1)}2\right)×1000 - 43$
        * $B = 43×22×1000 - 43$
        * $B = 945\,957$
  
3. $C = 7 + 21 + 63 + 189 + \cdots + 100\,442\,349$
  
    ??? success "Réponse"
        
        * $C = 7×3^0 + 7×3^1 + 7×3^2 + 7×3^3 + \cdots +7×3^{15}$
        * $\displaystyle C = \sum_{n=0}^{15} 7×3^n$
        * $\displaystyle C = 7×\sum_{n=0}^{15} 3^n$
        * $C = 7×\dfrac{3^{15+1}-1}{3-1}$
        * $C = 7×\dfrac{3^{16}-1}2$
        * $C = 150\,663\,520$
  
## Suite arithmético-géométrique - général

On considère la suite[^1] $(u_n)$ définie par la donnée de $u_0$ supposé connu,
 et la relation $u_{n+1} = a×u_n + b$ avec $a$ et $b$ connus, avec $a\neq1$.

[^1]: :material-wikipedia: [Suite arithmético-géométrique](https://fr.wikipedia.org/wiki/Suite_arithm%C3%A9tico-g%C3%A9om%C3%A9trique)

1. Résoudre l'équation $l = a×l + b$ dont l'inconnue est $l$.
  
    ??? success "Réponse"
        
        * $l -al = b$
        * $(1-a)l = b$, **attention**, avec $l-a\neq 1$.
        * $l = \dfrac{b}{1-a}$
  
2. On considère la suite $v_n = u_n - l$.
  
    1. Exprimer $u_n$ en fonction de $v_n$.
      
        ??? success "Réponse"
            $u_n = v_n + l$
      
    2. Exprimer $v_{n+1}$ en fonction de $u_n$.
      
        ??? success "Réponse"
            
            * $v_{n+1} = u_{n+1} - l$
            * $v_{n+1} = a×u_n + b - l$
  
    3. En déduire que $(v_n)$ est une suite géométrique de raison $a$.
      
        ??? success "Réponse"
            * $v_{n+1} = a×(v_n+l) + b - l$
            * $v_{n+1} = a×v_n + al + b - l$
            * $v_{n+1} = a×v_n$, d'après la définition de $l$.
            * Ainsi $(v_n)$ est une suite géométrique de raison $a$.
  
3. Exprimer $v_n$ en fonction de $n$, $v_0$ et $a$.
  
    ??? success "Réponse"

        * $(v_n)$ est géométrique de raison $a$, donc
        * Pour tout $n \in \mathbb N$, $v_n = v_0×a^n$.
  
4. Exprimer $v_0$ en fonction de $u_0$, $a$ et $b$.
  
    ??? success "Réponse"

        * $v_0 = u_0 - l$
        * $v_0 = u_0 - \dfrac{b}{1-a}$
  
5. En déduire une expression de $u_n$ en fonction de $n$, $u_0$ et $a$.
  
    ??? success "Réponse"

        * $u_n = v_n + \dfrac{b}{1-a}$
        * $u_n = \left(u_0 - \dfrac{b}{1-a}\right)×a^n + \dfrac{b}{1-a}$
  
