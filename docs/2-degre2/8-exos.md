# Exercices corrigés

## Somme et produit

Déterminer, si possible :

1. Deux nombres dont la somme est $10$ et le produit est $-299$.
  
    ??? success "Réponse"

        * On suppose que $x_1+x_2 = s = 10$ et $x_1×x_2 = p = -299$.
        * $x_1$ et $x_2$ sont solutions de $(x-x_1)(x-x_2) = 0$, donc aussi de
        * $x^2 - x×x_1 - x×x_2 + (-x_1)×(-x_2) = 0$
        * $x^2 -(x_1+x_2)x + x_1x_2 = 0$
        * $x^2 -sx + p = 0$
        * $x^2 -10x -299 = 0$ ; équation dont $x_1$ et $x_2$ sont solutions.
        * Ce trinôme du second degré (avec $a=1$, $b=-10$, $c=-299$) possède un discriminant égal à $\Delta = (-10)^2 - 4×1×(-299) = 100 + 1196 = 1296$ ; il est positif, ainsi on peut factoriser le trinôme.
        * Avec $\sqrt\Delta = \sqrt{1296} = 36$, on a
        * $1×\left(x-\dfrac{-(-10) + 36}{2×1}\right)\left(x-\dfrac{-(-10) - 36}{2×1}\right) = 0$
        * $(x-\dfrac{46}2)(x-\dfrac{-26}2) = 0$
        * $(x-23)(x-(-13)) = 0$, dont les solutions sont
        * $23$ et $-13$, deux nombres dont la somme est $10$, et le produit $-299$.
        
2. Deux nombres dont la somme est $4$ et le produit est $2$.
  
    ??? success "Réponse"

        * Avec le même raisonnement, on a
        * $x^2 -sx + p = 0$
        * $x^2 -4x +2 = 0$ ; équation dont $x_1$ et $x_2$ sont solutions.
        * $\Delta = (-4)^2 - 4×1×2 = 16 - 8 = 8$, qui est positif.
        * $\left(x - \dfrac{-(-4) - \sqrt8}{2×1}\right)\left(x - \dfrac{-(-4) + \sqrt8}{2×1}\right) = 0$
        * $\left(x - \dfrac{4 - 2\sqrt2}{2}\right)\left(x - \dfrac{4 + 2\sqrt2}{2}\right) = 0$
        * $\left(x - (2 - \sqrt2)\right)\left(x - (2 + \sqrt2)\right) = 0$, dont les solutions sont
        * $2 - \sqrt2$ et $2 + \sqrt2$, les nombres cherchés.
        * _Vérification_ :
            * La somme vaut $4$, clairement.
            * Le produit vaut $2^2 - \sqrt2^2=4 - 2 = 2$.
  
3. Deux nombres dont la somme est $5$ et le produit est $7$.
  
    ??? success "Réponse"

        * Avec le même raisonnement, on a
        * $x^2 -sx + p = 0$
        * $x^2 -5x +7 = 0$ ; équation dont $x_1$ et $x_2$ sont solutions.
        * $\Delta = (-5)^2 - 4×1×7 = 25 - 28 = -3$, qui est strictement négatif.
        * **Il n'y a aucune solution !**
  

## Degré trois

Résoudre l'équation $5x^3 -13x^2 -6x = 0$.

??? success "Réponse"

    * On factorise le membre de gauche par $x$, c'est un produit nul.
    * $x(5x^2 -13x -6) = 0$ a pour solution
        * $x = 0$, mais aussi
        * celles de $(5x^2 -13x -6) = 0$.
        * Pour ce deuxième cas, c'est un trinôme du second degré avec $a=5$, $b=-13$, $c=-6$, donc $\Delta = (-13)^2 - 4×5×(-6) = 169 + 120 = 289$, qui est positif avec $\sqrt{\Delta} = 17$.
        * Les solutions sont $\dfrac{-(-13) - 17}{2×5}$ et $\dfrac{-(-13) + 17}{2×5}$ ; à savoir $\dfrac{-4}{10}$ et $3$.
    * L'ensemble des solutions de l'équations est

    $$\mathscr S = \left\{\dfrac{-4}{10}, 0, 3\right\}$$

## Degré quatre

Résoudre l'équation $x^4 -16x^2 +63 = 0$.

??? success "Réponse"

    * On pose $X = x^2$, l'équation devient
    * $X^2 - 16X + 63 = 0$, qui est un trinôme du second degré en $X$,
    * avec $a=1$, $b=-16$, $c=63$, on a $\Delta = (-16)^2 - 4×1×63 = 256 - 252 = 4$, qui est positif, avec $\sqrt{\Delta} = 2$.
    * Les solutions sont $X$ est égal à $\dfrac{-(-16) - 2}{2×1}$ ou $\dfrac{-(-16) + 2}{2×1}$.
    * Ainsi $X = 7$, ou bien $X = 9$.
    * Dans le cas $X = 7$, on a
        * $x^2 = 7$ qui possède deux solutions :
        * $x = -\sqrt7$, ou bien $x = \sqrt7$.
    * Dans le cas $X = 9$, on a
        * $x^2 = 9$ qui possède deux solutions :
        * $x = -3$, ou bien $x = 3$.
    * Conclusion, l'ensemble des solutions est

    $$\mathscr S = \left\{-3, -\sqrt7, \sqrt7, 3\right\}$$

## Extrémum

_Indice_ : on pourra d'abord donner une forme canonique au trinôme du second degré...

Donner l'extrémum sur $\mathbb R$ et son antécédent des fonctions suivantes :

1. $f(x) = 8(x+3)^2 - 13$
  
    ??? success "Réponse"
        Ici, $a=8$ ; la parabole est orientée vers le haut, avec un minimum réalisé quand $x+3=0$.

        $f$ possède un **minimum** égal à $f(-3)=-13$.
  
2. $g(x) = -5x^2 + 7x + 8$
  
    ??? success "Réponse"
        
        * $g(x) = -5\left(x^2 - 2\dfrac7{10}x\right) + 8$
        * $g(x) = -5\left(x^2 - 2\dfrac7{10}x + \left(\dfrac7{10}\right)^2 - \left(\dfrac7{10}\right)^2 \right) + 8$
        * $g(x) = -5\left(x - \dfrac7{10} \right)^2 +5×\left(\dfrac7{10}\right)^2 + 8$
        * $g$ possède un **maximum** égal à $g\left(\dfrac7{10}\right) = 5×\left(\dfrac7{10}\right)^2 + 8 = \dfrac{1045}{100}$, que l'on pourrait simplifier par 5 en $\dfrac{209}{20}$.
        
  
3. $h(x) = (x+3)(4x-7) - 5$
  
    ??? success "Réponse"
        
        * $h(x) = 4x^2 -7x +12x -21 - 5$
        * $h(x) = 4x^2 +5x -26$
        * $h(x) = (2x)^2 +2×2x×\dfrac54 +\left(\dfrac54\right)^2 -\left(\dfrac54\right)^2 - 26$
        * $h(x) = \left(2x+\dfrac54\right)^2 -\left(\dfrac54\right)^2 - 26$
        * $h$ possède un **minimum** quand $\left(2x+\dfrac54\right)=0$, c'est à dire quand $x = \dfrac{-5}8$, qui est égal à $h\left(\dfrac{-5}8\right) = -\left(\dfrac54\right)^2 - 26 = \dfrac{-441}{16}$
        
  

## Python

1. Écrire une fonction `discriminant(a, b, c)` qui renvoie le discriminant du trinôme $ax^2+bx+c$.
  
    ??? success "Réponse"

        ```python
        def discriminant(a, b, c):
            "renvoie le discriminant du trinôme ax² + bx + c."
            return b*b -4*a*c
        ```
    
        Remarques : 
        
        * `b*b` ou `b**2`, le premier est plus simple.
        * Ensuite `-4*a*c`, les multiplications doivent être explicites ; `4ac` est **faux**.
  
2. Recopier et compléter la fonction `solutions(a, b, c)` qui renvoie  
 les solutions de l'équation $ax^2+bx+c = 0$.
  
    ```python
    ??? import sqrt

    def solutions(a, b, c):
        "Renvoie les solutions de ax² + bx + c = 0"
        delta = ???
        if delta > 0:
            racine_delta = ???
            x_1 = ???
            x_2 = ???
            return (x_1, x_2)  # deux solutions
        elif ???
            x_0 = ???
            return (x_0, x_0)  # solution double
        else:
            return None        # pas de solution
    ```

??? success "Réponse"

    ```python
    from math import sqrt

    def solutions(a, b, c):
        "Renvoie les solutions de ax² + bx + c = 0"
        delta = discriminant(a, b, c)
        if delta > 0:
            racine_delta = sqrt(delta)
            x_1 = (-b - racine_delta) / (2*a)
            x_2 = (-b + racine_delta) / (2*a)
            return (x_1, x_2)  # deux solutions
        elif delta == 0:
            x_0 = -b / (2*a)
            return (x_0, x_0)  # solution double
        else:
            return None        # pas de solution
    ```
    
    Remarques :
    
    * `/ (2*a)` ; les parenthèses sont indispensables !
    * `#!py3 elif delta == 0:` ; il faut doubler le `=`, et ne pas oublier `:` à la fin.


## Défi 1

On considère l'expression $f(x) = 2x^3 -7x^2 +2x +3$.

1. Vérifier que $1$ est solution de $f(x) = 0$.
  
    ??? success "Réponse"
        $f(1) = 2×1^3-7×1^2 +2×1 +3 = 2 - 7 +2 + 3 = 0$. Oui, $1$ est solution de l'équation.
  
2. En déduire une factorisation de $f(x)$ sous la forme $(x-1)(ax^2+bx+c)$.
  
    ??? success "Réponse"

        * Développons $(x-1)(ax^2+bx+c)$.
        * $ax^3+bx^2+cx  -ax^2-bx-c$
        * $ax^3 +(b-a)x^2 + (c-b)x - c$, qui est censé être égal à $2x^3 -7x^2 +2x +3$, on déduit
        * $\begin{cases}
           a = 2\\
           b-a = -7\\
           c-b = 2\\
           -c = 3
           \end{cases}$
        * d'où $a=2$, puis $b=-5$ et $c=-3$.
        * Il vient $f(x) = (x-1)(2x^2-5x-3)$
  
3. Résoudre l'équation $f(x) = 0$.
  
    ??? success "Réponse"
        
        * Ou bien $x-1 = 0$, et alors $x=1$.
        * Ou bien $2x^2-5x-3 = 0$ qui est un trinôme du second degré,
            * avec $a=2$, $b=-5$ et $c=-3$, on a $\Delta = 25 + 24 = 49$, qui est positif, avec $\sqrt\Delta = 7$.
            * dont les solutions sont $\dfrac{-(-5) - 7}{2×2}$ et $\dfrac{-(-5) + 7}{2×2}$, soit $\dfrac{-1}2$ et $3$.
        * Conclusion :
        
        $$\mathscr S = \left\{\dfrac{-1}2, 1, 3\right\}$$

## Défi 2

Résoudre $x -2\sqrt{x} -3 = 0$.

??? success "Réponse"

    * On sait déjà que $x\geqslant0$, afin que $\sqrt x$ soit défini.
    * On pose $X = \sqrt x$, l'équation devient :
    * $X^2 -2X -3 = 0$, qui est un trinôme du second degré en $X$.
    * On trouve facilement $X = -1$ ou $X = 3$.
    * Dans le cas où $X = -1$, on a
        * $\sqrt x = -1$, qui n'a aucune solution en $x$ réel.
    * Dans le cas où $X = 3$, on a
        * $\sqrt x = 3$, qui possède une unique solution réelle :
        * $x = 9$.
    * Conclusion : l'unique solution réelle de l'équation est $9$.

