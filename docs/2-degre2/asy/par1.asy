import graph;

size(5cm, 0);

real xmin = -3, xmax = 3;
real ymin = -1, ymax = 9.5;

// Définition de la fonction
real f(real x) {return x^2 - 4;}

// Tracé de la courbe
path Cf=graph(f, xmin, xmax, n=400);
draw(Cf, linewidth(2bp));

xaxis(Label("$x$", align=N), xmax=3.4, Arrow);
yaxis(dashed+1bp);

label("$a>0$", (0.5, 4), E);
label("$\Delta>0$", (0.5, 3), E);

label("$\frac{-b}{2a}$", (0, 0), SW);


shipout(bbox(2mm));
