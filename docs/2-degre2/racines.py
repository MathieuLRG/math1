from math import sqrt

def discriminant(a, b, c):
    """Renvoie le discriminant
    du trinôme ax² +bx + c
    """
    delta = b*b - 4*a*c
    return delta

def racines(a, b, c):
    """Renvoie les racines
    du trinôme ax² + bx + c
    """
    delta = discriminant(a, b, c)
    if delta < 0:
        # il n'y a pas de racines
        return []  # une liste vide
    elif delta == 0:
        return [-b/(2*a)]  # une liste avec une valeur
    else:
        racine_delta = sqrt(delta)
        return [
            (-b - racine_delta)/(2*a),
            (-b + racine_delta)/(2*a),
        ]  # une liste de deux valeurs

