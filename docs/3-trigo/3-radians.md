# Les radians

Il existe différentes unités d'angle :

=== "Le radian"
    ![https://fr.wikipedia.org/wiki/Radian](./images/Circle_radians.gif){ width="300" align="left" }

    Très utilisé dans les situations scientifiques ;

    Exemple : un angle droit, c'est $\frac{\pi}2$ radians.


=== "Le degré"
    ![https://pixabay.com/fr/vectors/tour-de-pise-pise-la-tour-rep%c3%a8re-144966/](./images/pisa-tower-144966.svg){ width="100" align="left" }

    Très utilisé dans les situations simples ;
    
    Exemple : la tour de Pise a une inclinaison d'environ $4°$.


=== "Le grade"
    ![https://fr.wikipedia.org/wiki/Grade_(angle)](./images/450px-Boussole_en_grades_table_conversion.jpg){ width="300" align="left" }

    En France, le [grade](https://fr.wikipedia.org/wiki/Grade_(angle)) est l'unité légale de mesure d'angle pour l'ensemble des travaux topographiques (arpentage, génie civil) et géodésiques (IGN) réalisés en France. En dehors de ces importants domaines, il est peu ou pas utilisé.

    Le quart du méridien terrestre fait, par définition, cent grades en mesure angulaire.

    Pendant longtemps, jusqu'à ce que l'on mesure avec précision la longueur du méridien, il y avait donc un rapport de conversion exact entre 60 × 90 = 5 400 milles et 100 × 100 = 10 000 km, alors que la longueur de l'un et l'autre n'était pas connue avec précision. 

=== "Le nombre de tours"
    ![https://pixabay.com/fr/illustrations/isol%c3%a9-transparent-planifier-moteur-1500372/](./images/isolated-1500372_960_720.png){ .autolight width="300"  align="left" }

    Utilisé, pour des machines à rotation rapide ;
    
    Exemple : un moteur qui tourne à 2500 [tours par minute](https://fr.wikipedia.org/wiki/Tour_par_minute).

    (En anglais : _revolution per minute_ ; rpm)

=== "Le mil angulaire "
    ![](./images/Schweizer_Artillerie_Promille.svg){ .autolight width="300"  align="left" }

    Le [mil angulaire](https://fr.wikipedia.org/wiki/Mil_angulaire) (ou millième angulaire, ou pour mille d'artillerie) est une unité de mesure des angles en usage dans les domaines militaire et nautique, utilisée surtout pour les instruments d'orientation et de pointage.

    Le mil se base sur l'angle qu'intercepte un objet de 1 m à une distance de 1 000 m ; soit, par l'approximation de Gauss, 1 milliradian. Un viseur ou des jumelles avec un réticule gradué en mils permet d'évaluer rapidement, sans télémètre, la distance d'un objet de hauteur connue, comme, en navigation côtière, celle d'un amer.

    Les bataillons d'artillerie de nombreux pays utilisent le mil comme unité d'angle, bien qu'il en existe **trois définitions différentes**. Exemple, le mil est défini comme étant 1/6400 d'une révolution (360°). L'unité approche la valeur d'un milliradian à 1,8 % près. Le nombre rond, 6 400 choisi par convention permet de construire un rapporteur de fortune en pliant une feuille de papier (circulaire) six fois consécutivement (car 64 = 2^6). La taille apparente, à peu près identique, du Soleil et de la Lune, est de 9 ₥.


!!! tip "À retenir : la même mesure d'angle"
    === "$1$ tour"
        - $360$ degrés
        - $2\pi$ radians
        - $400$ grades

    === "$\frac14$ tour"
        - $90$ degrés
        - $\frac{\pi}2$ radians
        - $100$ grades

    La conversion d'unité se fait par proportionnalité.

!!! note "Sens de rotation"
    Une rotation est caractérisée par un centre et un angle orienté (positif ou négatif).

    Par convention,

    - le sens trigonométrique est le sens anti-horaire, associé à un angle positif.
    - un angle négatif est associé à une rotation dans le sens horaire.

## Pourquoi un tour est-il égal à $2\pi$ radians ?

Il y a de très nombreuses raisons. Voyons-en certaines.

### La longueur d'un arc de cercle

La longueur $l$ d'un arc de cercle est proportionnelle au rayon $r$ du cercle et à la mesure $\alpha$ de l'angle de l'arc.

Ainsi $l = k×\alpha×r$ où $k$ est un coefficient qui dépend de l'unité de l'angle choisie. Pour trouver la valeur de $k$, on considère un cercle complet ; on doit avoir le périmètre du cercle $l = 2\pi×r$.

- Avec les radians, on déduit $k=1$, et donc $l = \alpha×r$ ; c'est simple.
- Avec les degrés, on déduit $k=\dfrac{\pi}{180}$, et donc $l = \dfrac{\pi×\alpha×r}{180}$ ; c'est plus lourd.

### L'aire d'un secteur angulaire

De la même manière, l'aire d'un secteur angulaire d'angle $\alpha$ et de rayon $r$ est :

- $\mathscr A = \dfrac{\alpha×r^2}2$, si $\alpha$ est en radian ; c'est simple !
- $\mathscr A = \dfrac{\pi×\alpha×r^2}{360}$, si $\alpha$ est en degré ; c'est plus lourd !

### Approximation des fonctions trigonométriques

Nous le reverrons plus tard, mais, pour $x$ un angle très proche de $0$ :

=== "Avec $x$ en radian, proche de zéro"

    - $\sin(x) \approx x$
    - $\cos(x) \approx 1-\dfrac{x^2}2$

    C'est simple !

=== "Avec $x$ en degrés, proche de zéro"

    - $\sin(x) \approx \dfrac{\pi×x}{180}$
    - $\cos(x) \approx 1 - \frac12×\left(\dfrac{\pi×x}{180}\right)^2$
    
    C'est plus lourd !

:warning: Et c'est encore plus flagrant avec une approximation plus précise.

!!! info "Radian : une excellente unité d'angle"
    Le radian constitue une excellente unité d'angle dans le domaine scientifique. Il ne faut pas en avoir peur ; au contraire !

    ![](./images/arc1.svg){ .autolight }
    ![](./images/arc2.svg){ .autolight }

    - Avec $\alpha$ exprimé en radian :
        - La longueur d'un arc de cercle d'angle $\alpha$ et de rayon $r$ est $l = \alpha r$
        - L'aire d'un secteur angulaire d'angle $\alpha$ et de rayon $r$ est : $\mathscr A = \dfrac{\alpha r^2}2$
    - Les approximations au voisinage de $0$ des fonctions trigonométriques (en radian) sont **bien** plus simples.


## Conversion d'unités

On a le tableau de proportionnalité

| Degré|Radian  |
|-----:|:-------|
|  $0°$ | $0$    |
|$360°$ | $2\pi$ |
|$180°$ | $\pi$  |
| $90°$ | $\dfrac{\pi}2$ |
| $60°$ | $\dfrac{\pi}3$ |
| $45°$ | $\dfrac{\pi}4$ |
| $30°$ | $\dfrac{\pi}6$ |

On rappelle qu'il existe des fonctions Python de conversion

```pycon
>>> from math import pi, degrees, radians
>>> radians(180)
3.141592653589793
>>> degrees(pi)
180.0
```



## Mesure principale d'un angle

Jusqu'à présent, on n'a évoqué que les angles aigus ; il y en a d'autres.

La notion d'angle, en général, n'a pas encore été rigoureusement définie dans ce cours, cependant on peut s'accorder sur quelques points :

- un angle peut être associé à une rotation ;
- cette rotation peut être de plusieurs tours, complets ou non ;
- cette rotation peut avoir eu lieu dans un sens ou dans l'autre...

Connaissant l'état initial et final d'une rotation, on peut donner une mesure de l'angle... à un ou plusieurs tours près. On dira **modulo** un tour ou modulo 360°.

En radian, on dira « modulo $2\pi$ ».

La mesure principale d'un angle est, ou bien :

- un nombre réel entre $0$ (inclus) et $1$ tour (exclu)
- un nombre réel entre $0$ (inclus) et $360°$ (exclu)
- un nombre réel entre $0$ (inclus) et $2\pi$ radians (exclu)

??? warning "Parfois, l'intervalle est centré"
    - un nombre réel entre $-\frac12$ (inclus) et $+\frac12$ tour (exclu)
    - un nombre réel entre $-180°$ (inclus) et $+180°$ (exclu)
    - un nombre réel entre $-\pi$ (inclus) et $+\pi$ radians (exclu)

=== "De $0$ à $2\pi$"
    ![](./asy/tour1.svg){.autolight}

=== "De $-\pi$ à $+\pi$"
    ![](./asy/tour2.svg){.autolight}


!!! example "Exemples en radian"

    - La mesure principale de l'angle $10\pi$ est $0$. Cela correspond à exactement $5$ tours complets.
    - La mesure principale de de l'angle $\dfrac{17\pi}2$ est obtenue en écrivant $\dfrac{17\pi}2 = \dfrac{8×2\pi}2 + \dfrac{\pi}2$, dont la première partie correspond à 4 tours. La réponse est donc $\dfrac{\pi}2$.
    - La mesure principale de l'angle $\dfrac{31\pi}4$ est obtenue en écrivant $\dfrac{31\pi}4 = \dfrac{24\pi}4 + \dfrac{7\pi}4= \dfrac{3×4×2\pi}4 + \dfrac{7\pi}4$, dont la première partie correspond à 3 tours. La réponse est donc $\dfrac{7\pi}4$.

    Jouez avec cette appliquette !

    <iframe scrolling="no" title="mesure principale" src="https://www.geogebra.org/material/iframe/id/wepjatrz/width/735/height/413/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false" width="735px" height="413px" style="border:0px;"> </iframe>

    - Surtout avec $n\in[3, 4, 6, 8]$
    - Cherchez à anticiper l'affichage mobile en faisant varier $m$.
    - Essayez de comprendre la différence avec la mesure principale approchée qui est donnée au centre.

### Figure bilan

![](./asy/bilan.svg){.autolight}

:warning: Il faut savoir compléter cette figure si elle comporte des informations manquantes. On utilise les informations connues avec les angles aigus et les symétries de la figure.

## Enroulement de la droite sur le cercle

<iframe scrolling="no" title="Enroulement" src="https://www.geogebra.org/material/iframe/id/njwjsbpx/width/577/height/424/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false" width="577px" height="424px" style="border:0px;"> </iframe>

!!! note "Point image"
    À chaque nombre réel $a$, représenté par le point $P$ sur la droite d'équation $y=1$, on associe un point $M$ sur le cercle trigonométrique que l'on appelle **point image**.

On rappelle que la longueur d'un arc de cercle de rayon $r$ et d'angle $\alpha$ en radian est $l = r\alpha$, or pour un cercle de rayon $1$, la formule devient extrêmement simple : $l = \alpha$.

On peut alors donner une nouvelle définition d'un angle et de la mesure d'un angle. Cette définition reste grossière, on pourra consulter cette [page Wikipédia sur l'angle](https://fr.wikipedia.org/wiki/Angle) pour comprendre différentes subtilités.

- Un angle représente la rotation de centre $O$ qui envoie le point $A$ en $M$.
- Cet angle est associé à une « longueur » d'un arc de cercle (éventuellement plusieurs tours, éventuellement négatif, d'où les guillemets à longueur).
- Cet « arc de cercle » est associé à un point sur la droite qui est enroulée autour du cercle. Ce point possède une ordonnée, un nombre réel.

On retrouve qu'un tour complet est égal à $2\pi$ radians, la circonférence d'un cercle de rayon $1$.

!!! note "Plusieurs angles sont associés à la même rotation"
    Si $\alpha$ est un angle associé à une rotation, alors $\alpha+2\pi$ est un autre angle, mais la rotation de même centre associée à cet angle aura le même effet.

    Deux nombres réels $x$ et $x'$ de la droite numérique ont le même point image sur le cercle si et seulement si $x = x' + 2k\pi$ avec $k\in\mathbb Z$.

    > On dit alors que les angles sont égaux à quelques tours près ($2\pi$) ; un nombre nul, positif ou négatif de tours. On dit aussi qu'ils sont congrus, modulo $2\pi$.

!!! example "Exemples"
    - $\pi$, $-\pi$ et $3\pi$ sont congrus, modulo $2\pi$. Leur point image commun est $(-1;0)$.
    - $\dfrac{3\pi}2$ et $\dfrac{-\pi}2$ sont congrus, modulo $2\pi$. Leur point image commun est $(0;-1)$.


## Exercices

### Pyromaths

Utilisez <https://enligne.pyromaths.org/>, onglet Seconde, section Trigonométrie.

Créez plusieurs exercices, vous obtiendrez un fichier `pdf` avec le corrigé à part.

Vous pouvez ainsi réviser et vous autoévaluer jusqu'à ce que le concept soit acquis de manière solide.

### Choisir la bonne valeur

!!! question "Quadrant NO"
    Sachant que $x\in\left[\dfrac{\pi}2;\pi\right]$ et que $\sin(x) = 0,\!3$, donner la valeur exacte de $\cos(x)$.

!!! success "Réponse"
    On sait que $x$ est dans le quadrant au nord-ouest, avec $\cos(x)\leqslant 0$ et $\sin(x)\geqslant 0$.

    On a aussi

    - $\cos^2(x) + \sin^2(x) = 1$, d'où
    - $\cos^2(x) + 0,\!3^2 = 1$
    - $\cos^2(x) = 1- 0,\!09 = 0,\!91$, avec $\cos(x)\leqslant 0$ :warning:
    - $\boxed{\cos(x) = -\sqrt{0,\!91}}$
    