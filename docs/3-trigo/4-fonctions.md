# Fonctions sin, cos, tan

Nous ne faisons ici qu'une présentation de ces fonctions. Leur étude sera faite après le chapitre sur la dérivation.

## Définitions

<iframe scrolling="no" title="Cercle trigonométrique" src="https://www.geogebra.org/material/iframe/id/ysry6vzz/width/660/height/660/border/888888/sfsb/true/smb/false/stb/true/stbh/false/ai/true/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false" width="660px" height="660px" style="border:0px;"> </iframe>

Pour un angle $a$ en radian (quelconque, donc n'importe quel nombre réel), on associe le point $P$ d'ordonnée $a$ sur la droite que l'on enroule sur le cercle trigonométrique. Cela donne un point $M$ image d'une rotation de $A$ de centre $O$ et d'angle $a$. On définit alors

- $\sin(a)$ : **l'ordonnée** du point $M$
- $\cos(a)$ : **l'abscisse** du point $M$
- $\tan(a) = \dfrac{\sin(a)}{\cos(a)}$ si $\cos(a)\neq0$, sinon $\tan(a)$ n'est pas défini.

!!! info "Cohérence"
    Cette nouvelle définition est cohérente avec la définition donnée pour les angles aigus ; elle **coïncide** !

    On a choisit la variable $a$ juste au-dessus, pour éviter les confusion avec la fonction `x` de GeoGebra qui renvoie l'abscisse d'un point. Mais on peut utiliser la variable $x$...

    Cette définition prolonge donc à tout nombre réel $x$ la notion de $\sin(x)$, $\cos(x)$ et $\tan(x)$.

    :warning: Pour $\tan(x)$, il faut que $\cos(x)\neq0$, sinon $\tan(x)$ n'est pas défini. Nous verrons ensuite à quelles valeurs de $x$ cela correspond. Nous donnerons aussi en exercice une interprétation géométrique de $\tan(x)$ qui n'est pas seulement $\dfrac{\sin(x)}{\cos(x)}$.

## Lignes trigonométriques

Recopier et compléter le tableau suivant :

|Angle (radian)|sinus|cosinus|tangente|
|:-------------|----:|------:|-------:|
|$0$| | | |
|$\dfrac{\pi}6$| | | |
|$\dfrac{\pi}4$| | | |
|$\dfrac{\pi}3$| | | |
|$\dfrac{\pi}2$| | | |
|$\dfrac{2\pi}3$| | | |
|$\dfrac{3\pi}4$| | | |
|$\dfrac{5\pi}6$| | | |
|$\pi$| | | |
|$\dfrac{5\pi}4$| | | |
|$\dfrac{7\pi}4$| | | |
|$2\pi$| | | |

> On pourra s'aider du tableau vu avec les angles aigus, de l'appliquette page précédente (avec $n$ et $m$ variable) et des symétries de la figure.

:warning: Il **faut** savoir compléter ce genre de tableau.

## Représentation graphique

### Cosinus

<iframe scrolling="no" title="Fonction cos" src="https://www.geogebra.org/material/iframe/id/nkw2papb/width/821/height/425/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/true/asb/false/sri/false/rc/false/ld/false/sdz/true/ctl/false" width="821px" height="425px" style="border:0px;"> </iframe>

!!! example "Expériences"
    Il y a un champ de saisie en bas, à côté du gros :octicons-plus-16:

    - Saisir `cos(x+a)`, faire varier `a`. Émettre une conjecture, puis effacer cette courbe.
    - Saisir `cos(x-a)`, faire varier `a`. Émettre une conjecture, puis effacer cette courbe.
    - Saisir `sin(x+a)`, faire varier `a`. Émettre une conjecture, puis effacer cette courbe.
    - Saisir `sin(x-a)`, faire varier `a`. Émettre une conjecture, puis effacer cette courbe.

    Notre objectif va être de préciser et démontrer ces conjectures.

### Sinus

Faire le même genre d'expériences avec `sin(x)`.

## Propriétés

Pour tout réel $x$, un angle en radian, on a :

- $-1 \leqslant \cos(x) \leqslant +1$
- $-1 \leqslant \sin(x) \leqslant +1$
- $\cos^2(x) + \sin^2(x) = 1$

### Une figure de référence

!!! info "Motivation :rotating_light: "
    L'idée est d'être capable de retrouver facilement les formules afin
    
    - :octicons-light-bulb-16: de ne pas avoir à les apprendre par cœur
    - :warning: et d'être certain de ne faire aucune erreur de signe.


Il faut savoir refaire rapidement, **à la main**, le schéma qui correspond à l'appliquette en haut de la page : **le cercle trigonométrique**.

![](./asy/c-trig.svg){.autolight}


- Tracer un cercle de rayon $1$.
- Tracer ensuite les axes.
- Placer un point $M$, sur le cercle,
    - associé à un angle $x$ d'environ $30°$ ; c'est plus simple pour raisonner.
    - :warning: ne pas choisir environ $45°$.
- Placer $\cos(x)$ et $\sin(x)$ sur les axes.

!!! tip "Encadrement utile avec $x$ un angle aigu en radian"

    Si $x$ est un **angle aigu en radian** ( $0 < x < \frac\pi 2$), alors

    $$\sin(x) < x < \tan(x)$$

    !!! questions "Preuve"

        ![](./asy/tan.svg){.autolight align="left"}

        On considère la figure ci-contre, avec un arc de cercle de rayon $1$

        1. Donner l'aire du triangle $OIM$.
        2. Donner l'aire du secteur angulaire $\angle O\overset{\Large\frown}{IM}$.
        3. Donner l'aire du triangle $OIN$.
        4. En déduire l'encadrement demandé.

    ??? success "Réponses"
        1. Le point $M$ a pour coordonnées $(\cos(x), \sin(x))$, on en déduit que la hauteur $MH$ mesure $\sin(x)$, associée à la base $OI$ de mesure $1$, on obtient une aire de $\dfrac{1×\sin(x)}2$ pour le triangle $OIM$.
        2. D'après la formule vue plus haut, avec un rayon $1$, l'aire du secteur angulaire est simplement $\dfrac{x×1^2}2$
        3. Dans la configuration de Thalès, avec $MH/\!/NI$, on déduit $\dfrac{NI}{MH} = \dfrac{OI}{OH}$, d'où $\dfrac{NI}{\sin(x)} = \dfrac{1}{\cos(x)}$, donc $NI = \tan(x)$. Ainsi l'aire du triangle $OIN$ est $\dfrac{1×\tan(x)}2$
        4. Le triangle $OIM$ est inclus dans le secteur angulaire, qui lui-même est inclus dans le triangle $OIN$, on déduit

        $$\dfrac{\sin(x)}2 < \dfrac{x}2 < \dfrac{\tan(x)}2$$

        Et ainsi, pour finir, pour tout angle aigu $0 < x < \frac\pi2$, on a :

        $$\sin(x) < x < \tan(x)$$



!!! example "Activité"
    Pour **toutes** les situations suivantes :

    - Tracer un cercle trigonométrique.
    - Placer le point $M'$ associé à la situation.
    - Indiquer l'angle associé au point $M'$.
    - Placer le cosinus et le sinus associé.
    - Déduire, avec des considérations géométriques, **une formule de trigonométrie**.

    Situations :

    1. Le point $M'$ est l'image de $M$ par une rotation de centre $O$ et d'angle $+2\pi$ (un tour complet).
    2. Le point $M'$ est l'image de $M$ par une rotation de centre $O$ et d'angle $+\pi$ (un demi-tour).
    3. Le point $M'$ est l'image de $M$ par une rotation de centre $O$ et d'angle $+\dfrac{\pi}2$ (un quart de tour).
    4. Le point $M'$ est le symétrique de $M$ par rapport au point $O$.
    5. Le point $M'$ est le symétrique de $M$ par rapport à l'axe $(Ox)$.
    6. Le point $M'$ est le symétrique de $M$ par rapport à l'axe $(Oy)$.
    7. Le point $M'$ est le symétrique de $M$ par rapport à la première bissectrice (la droite d'équation $y=x$).

???+ success "4. Symétrie centrale"
    - Le point $M$ associé à l'angle $x$ a pour coordonnées $\left(\cos(x), \sin(x)\right)$.
    - Le point $M'$ symétrique de $M$ par rapport à $O$ est associé à un angle de $x+\pi$.
    - Le point $M'$ a donc pour coordonnées $\left(\cos(x+\pi), \sin(x+\pi)\right)$
    - Pour des raisons de symétrie le point $M'$ a pour coordonnées $\left(-\cos(x), -\sin(x)\right)$

    ![](./asy/c-trig-1.svg){.autolight}

    On en déduit que pour tout $x\in\mathbb R$

    $$\sin(x+\pi) = -\sin(x)$$

    $$\cos(x+\pi) = -\cos(x)$$
    
    $$\tan(x+\pi) = \frac{\sin(x+\pi)}{\cos(x+\pi)} = \frac{-\sin(x)}{-\cos(x)}= \frac{\sin(x)}{\cos(x)} = \tan(x)\quad\text{si c'est défini}$$

:warning: À vous de faire les autres situations et d'établir trois formules dans chaque cas.


??? note "Bilan à savoir retrouver"
    Formules valables pour tout $x\in\mathbb R$, sauf pour $\tan$ où il y a des problèmes de définitions en certains points. Rappel : $\tan(x)$ n'est pas défini quand $\cos(x)=0$. Nous y reviendrons.

    1. Rotation de $2\pi$ ; le point image de $x$ et celui de $x+2\pi$ sont confondus.
      
        $$\begin{cases}
        \sin(x+2\pi) & = &\sin(x)\\
        \cos(x+2\pi) & = &\cos(x)\\
        \tan(x+2\pi) & = &\tan(x)\\
        \end{cases}$$
    2. Rotation de $\pi$ ; le point image de $x$ et celui de $x+\pi$ sont diamétralement opposés.
      
        $$\begin{cases}
        \sin(x+\pi) & = &-\sin(x)\\
        \cos(x+\pi) & = &-\cos(x)\\
        \tan(x+\pi) & = &\tan(x)\\
        \end{cases}$$
    3. Rotation de $\pi/2$ ; attention aux signes
      
        $$\begin{cases}
        \sin\left(x+\frac{\pi}2\right) & = &\cos(x)\\
        \cos\left(x+\frac{\pi}2\right) & = &-\sin(x)\\
        \tan\left(x+\frac{\pi}2\right) & = &\frac{-1}{\tan(x)}\\
        \end{cases}$$
    4. Symétrie par rapport au point $O$, c'est une rotation de $\pi$ ; déjà vu.
    5. Symétrie par rapport à l'axe $(Ox)$
      
        $$\begin{cases}
        \sin(-x) & = &-\sin(x)\\
        \cos(-x) & = &\cos(x)\\
        \tan(-x) & = &-\tan(x)\\
        \end{cases}$$
    6. Symétrie par rapport à l'axe $(Oy)$
      
        $$\begin{cases}
        \sin(\pi-x) & = &\sin(x)\\
        \cos(\pi-x) & = &-\cos(x)\\
        \tan(\pi-x) & = &-\tan(x)\\
        \end{cases}$$
    7. Symétrie par rapport à la première bissectrice
      
        $$\begin{cases}
        \sin\left(\frac{\pi}2 - x\right) & = &\cos(x)\\
        \cos\left(\frac{\pi}2 - x\right) & = &\sin(x)\\
        \tan\left(\frac{\pi}2 - x\right) & = &\frac{1}{\tan(x)}\\
        \end{cases}$$

    Pour le dernier cas, on retrouve une formule du collège : pour deux angles complémentaires, le sinus de l'un est le cosinus de l'autre. Et, en effet, $\x$ et $\pi/2 - x$ sont bien complémentaires.

## Utilisation pratique

!!! question "Exercice simple"
    - Déterminer $\cos\left(\dfrac{7\pi}6\right)$.
    - Déterminer $\sin\left(\dfrac{-19\pi}4\right)$.


### Calcul numérique :boom: :boom:

Surtout pour les élèves qui sont aussi en NSI, mais il n'y **aucune connaissance** de Python particulière à avoir.

- On suppose que l'on est en train de construire soi-même des fonctions Python `cos`, `sin` et `tan`. Le module `math` est supposé inaccessible.
- On suppose que les fonctions `_cos_`, `_sin_` sont disponibles et renvoient le cosinus et le sinus d'un angle, mais uniquement pour un angle $-\dfrac{\pi}4 \leqslant x \leqslant \dfrac{\pi}4$.

**Objectif** : écrire de nouvelles fonctions `cos` et `sin` qui renvoient le cosinus et le sinus d'un angle quelconque.

On pourra compléter le code ci-dessous.

{{ IDE('exo', SANS='math') }}
