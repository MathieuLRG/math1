# Addition de deux signaux de même fréquence

Cette activité est une activité simple de découverte. La fin (partie démonstration) est totalement facultative et illustre le travail effectué en études supérieures.

## Activité GeoGebra

<iframe scrolling="no" title="Addition, même fréquence" src="https://www.geogebra.org/material/iframe/id/nhb6u5rv/width/788/height/539/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false" width="788px" height="539px" style="border:0px;"> </iframe>

### Découverte du signal

En sciences physiques, on retrouve la forme sinusoïdale dans plusieurs expériences :

- le courant électrique alternatif,
- une onde sonore d'une seule fréquence,
- la lumière monochromatique émise par un laser,
- une vibration non amortie,
- et beaucoup d'autres.

Dans l'appliquette ci-dessus, on a représenté la fonction $f$ telle que $f(x)=A\cos(\omega x + \varphi)$

- $A$ est l'amplitude du signal
- $\omega$ est la pulsation du signal
- $\varphi$ est le déphasage du signal

!!! example "Jouez avec les curseurs"
    - La pulsation $\omega$, on prononce oméga.
        - Une pulsation $\omega$ élevée correspond suivant la situation à
            - un son aigu,
            - une lumière visible plus proche du violet.
        - Une pulsation $\omega$ faible correspond suivant la situation à
            - un son grave,
            - une lumière visible plus proche du rouge.
    - Le déphasage, ou juste la phase, $\varphi$, on prononce phi.
        - cela indique le décalage du phénomène ondulatoire sur le repère.
    - L'amplitude, $A$, désigne l'intensité du phénomène.
        - Le curseur $A$ se trouve dans le menu déroulant en bas de l'appliquette,

    Jouez avec les curseurs !

!!! example "Jouez avec deux fonctions"
    1. Cochez pour rendre visibles
        - la fonction $g$, ainsi que
        - le déphasage associé $\psi$ (on prononce psi)
    2. Les fonctions $f$ et $g$ partagent ici
        - la même amplitude $A=B=1$
        - la même pulsation $\omega$
    
    !!! question "Questions"
        1. Est-il possible que les courbes soient superposées ? À quelle condition ?
        2. Est-il possible que les courbes soient en opposition ? À quelle condition ?
        3. Que pensez-vous qu'il se passe si on ajoute les deux signaux dans chaque situation ?
    
    ??? success "Réponse"
        1. Les courbes sont superposées quand la  différence de déphase est congrue (égale) à $0$ modulo $2\pi$.
        2. Les courbes sont en opposition quand la  différence de déphase est congrue (égale) à $\pi$ modulo $2\pi$.
        3. Si on ajoute deux signaux
            - superposés, la somme sera d'intensité double ;
            - en opposition, la somme sera nulle.

!!! info "Casque à réduction active de bruit"
    Pour information, un casque à réduction active de bruit détecte un signal à absorber et crée un signal en opposition de phase, de manière à tenter d'annuler le premier.

!!! info "Déphasage provoqué"
    Dans une entreprise avec des machines électriques puissantes, le démarrage d'une machine peut entrainer un déphasage sur tout le circuit électrique de l'entreprise. Il faut savoir l'anticiper et éviter des dégâts possibles.

!!! example "Fabriquer la somme de deux signaux de même pulsation"
    1. Cochez les curseurs $A$ et $B$, les amplitudes de $f$ et $g$.
    2. Cochez la fonction $h$ qui est la somme de $f$ et $g$.
    3. Jouez avec les curseurs !
    3. Observez le phénomène important : **la somme de deux signaux sinusoïdaux de même fréquente (ou pulsation) est lui-même un signal sinusoïdal de même fréquence**.


!!! danger "Démonstration simplifiée :boom: :boom: :boom:"
    :warning: Cette démonstration est hors programme, même simplifiée, mais elle utilise judicieusement plusieurs fois la formule vue à la page précédente :

    $$\cos(a+b) = \cos(a)\cos(b) - \sin(a)\sin(b)$$

    Pour comprendre la démonstration du phénomène, on simplifie avec une pulsation égale à $1$.

    - $f_1(x) = A_1×\cos(x+b_1)$
    - $f_2(x) = A_2×\cos(x+b_2)$
    - $f(x) = f_1(x) + f_2(x)$

    On a

    - $f_1(x) = A_1(\cos(x)\cos(b_1) - \sin(x)\sin(b_1))$
    - $f_2(x) = A_2(\cos(x)\cos(b_2) - \sin(x)\sin(b_2))$

    Après avoir développé et factorisé les termes en $\cos(x)$ et $\sin(x)$, on a

    - $f(x) = (A_1\cos(b_1) + A_2\cos(b_2))\cos(x) - (A_1\sin(b_1) + A_2\sin(b_2))\cos(x)$

    On note $C = A_1\cos(b_1) + A_2\cos(b_2)$ et $S = A_1\sin(b_1) + A_2\sin(b_2)$, de sorte que

    - $f(x) = C×\cos(x) - S×\cos(x)$

    :warning: On suppose ici que $C\neq0$.

    On pose $\varphi = \arctan\left(\dfrac S C\right)$, de sorte que

    - $\varphi \in \left]\dfrac{-\pi}2 \,;\, \dfrac{-\pi}2\right[$
    - $\tan(\varphi) = \dfrac S C$
    - $C = \dfrac C {\cos(\varphi)}×\cos(\varphi)$
    - $S = \dfrac C {\cos(\varphi)}×\sin(\varphi)$
    - $\cos(\varphi) > 0$

    On a alors

    - $f(x) = \dfrac C {\cos(\varphi)} (\cos(\varphi)×\cos(x) - \sin(\varphi)×\cos(x))$

    Et finalement

    - $f(x) = \dfrac C {\cos(\varphi)} \cos(x + \varphi)$

    Ce qui prouve que le signal obtenu est sinusoïdal.

    !!! note "Remarque"

        - $C^2 = \dfrac {C^2} {\cos^2(\varphi)}×\cos^2(\varphi)$
        - $S^2 = \dfrac {C^2} {\cos^2(\varphi)}×\sin^2(\varphi)$

        De sorte que $C^2 + S^2 = \dfrac {C^2} {\cos^2(\varphi)}$ d'où

        - $f(x) = \sqrt{C^2+S^2} \cos(x + \varphi)$

    :warning: Dans le cas où $C=0$, on a

    - $f(x) = - S×\cos(x)$
    - $f(x) = S×\cos(x+\pi)$

    Ainsi en posant $\varphi = \pi$, on retrouve

    - $f(x) = \sqrt{C^2+S^2} \cos(x + \varphi)$


    !!! info "Pour ceux qui veulent en savoir plus à ce sujet"

        - Cours de physique à l'IUT : [Électricité, Chapitre 4, La somme des signaux alternatifs sinusoïdaux de même fréquence](https://public.iutenligne.net/electronique/piou_fruitet_fortun/baselecpro/acquisition/pdf/DL-001051-04-04.01.00.pdf) par Michel Piou.
        - Cours de physique à l'ENS Lyon : [Somme de signaux sinusoïdaux de même fréquence mais de phase différente, en langage python](https://culturesciencesphysique.ens-lyon.fr/ressource/somme-sinus-python.xml) par Delphine Chareyron, en partenariat avec ÉduScol.


!!! info "Sciences étonnantes, sur YouTube"
    Quelques vidéos de David Louapre disponibles sur YouTube, chaudement recommandées.

    - En additionnant des fonctions sinusoïdales, on peut expliquer les variations du paléoclimat
        - :fontawesome-brands-youtube: [Les cycles de Milankovitch et les changements climatiques](https://www.youtube.com/watch?v=MXcY8Cf6hsI)
        - :fontawesome-solid-blog: [blog](https://scienceetonnante.com/2016/07/22/les-cycles-de-milankovitch-et-les-changements-climatiques/)

    - L'expérience des fentes d'Young en mécanique quantique
        - :fontawesome-brands-youtube: [La plus belle expérience de toute l'histoire de la physique !](https://www.youtube.com/watch?v=zPolTp0ddRg)
        - :fontawesome-solid-blog: [blog](https://scienceetonnante.com/2018/04/13/lexperience-des-fentes-dyoung-en-mecanique-quantique/)
