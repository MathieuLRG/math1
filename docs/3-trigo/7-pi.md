# Approximations de π

> Cette page s'inspire en partie d'un document d'édu**scol** : [Activité 10 - La méthode d'Archimède](https://media.eduscol.education.fr/ftp_eduscol/2019/Ressources/Mathematiques/RA19_Lycee_G_1_MATH_Algorithmique_et_Programmation_activite_10.html)


!!! warning "Sommaire, Page très longue"
    1. Quelques informations culturelles
    2. Méthode des polygones, expériences avec GeoGebra, NumWorks et Python
    3. Accélération de la convergence : doubler le nombre de côtés ; nouvelles formules de récurrence
    4. (Python) Algorithme de Héron pour l'extraction de racine carrée
    5. (Python) Algorithme d'Archimède pour calculer 1000 décimales, ou plus, de $\pi$ 


$$\pi = 3,\!14159265358979323846264338327950288419716939937510\cdots$$


## Histoire sur le calcul de π

![](./images/History_of_pi_computation.png)

La suite de ce paragraphe, sur l'histoire du calcul de $\pi$, est tiré essentiellement de cette [page Wikipédia](https://fr.wikipedia.org/wiki/Approximation_de_%CF%80).

!!! cite "Environ 1600 ans avant notre ère"
    - Les babyloniens utilisaient $3$ à la place de $\pi$, cela suffisait pour leur construction architecturale, mais savait que c'était une approximation, ils en connaissait une meilleure : $3+\frac 1 8=\dfrac{25}8$.
    - Les égyptiens donnent $\dfrac{256}{81}$ en utilisant un octogone régulier approchant un cercle. La précision était d'environ $0,\!5\%$

!!! cite "Environ 300 ans avant notre ère"
    La philosophie pythagoricienne prétend que le nombre entier et ses rapports (c'est-à-dire les fractions) expliquent le Monde. Vers -500, [la découverte de l'incommensurabilité](https://fr.wikipedia.org/wiki/Hippase_de_M%C3%A9taponte) (par exemple, $\sqrt 2$ est irrationnel) jeta le trouble dans la confrérie et ouvrit une profonde crise philosophique. Les fractions restent très utilisées !
    
    En Grèce, vers -300, Archimède a prouvé que $\dfrac{223}{71} < \pi < \dfrac{22}7$.

    Il a utilisé un polygone à $3×2^5 = 96$ côtés ; un triangle équilatéral, puis découpage de l'angle de $60°$ en deux, 5 fois de suite. Il a aussi utilisé des approximations pour l'extraction des racines carrées.

    > Nous allons utiliser cette technique : découper un angle en deux, plusieurs fois, mais avec Python !

!!! cite "Entre 200 et 500 de notre ère"
    En Chine, Liu Hui puis Zu Chongzhi sont connus pour avoir travaillé sur $\pi$, avec une précision de 7 sept chiffres après la virgule. Le résultat $\pi\approx \dfrac{355}{113}$ restera pendant près de mille ans la fraction la plus précise.

    Cette approximation est connue sous le nom de [Milü](https://en.wikipedia.org/wiki/Mil%C3%BC), c'est la meilleure approximation de $\pi$ par une fraction ayant un dénominateur à 4 chiffres ou moins.


!!! cite "Autour du XIV<sup>e</sup> siècle de notre ère"
    - En Inde, Madhava de Sangamagrama calcule, avec une toute autre méthode (une série infinie), une approximation de $\pi$ et obtient 11 chiffres après la virgule.
    - Plus tard, en Perse, en 1424, Al-Kashi, obtient 17 chiffres avec un polygone à $3×2^{28}$ côtés.

!!! cite "Du XVI<sup>e</sup> au XIX<sup>e</sup> siècle"
    - Dans la seconde moitié du XVI<sup>e</sup> siècle, le mathématicien français François Viète a découvert un produit infini qui converge vers π, connu sous le nom de formule de Viète.
    - Le mathématicien germano-néerlandais Ludolph van Ceulen (vers 1600) a calculé les 35 premières décimales de π à l'aide d'un $2^{62}$-gone. Il était si fier de ce résultat qu'il l'a fait inscrire sur sa pierre tombale.
    - En 1789, le mathématicien slovène Jurij Vega a calculé les 140 premières décimales de π, cependant les 14 dernières étaient incorrectes ; ce résultat a néanmoins été le record du monde pendant 52 ans, jusqu'en 1841, lorsque William Rutherford a calculé 208 décimales dont les 152 premières étaient correctes.
    - Le mathématicien amateur anglais William Shanks a passé plus de 20 ans à calculer les 707 premières décimales de π. Lorsqu'il eut terminé sa tâche en 1873, les 527 premières décimales étaient correctes. Il calculait de nouvelles décimales toute la matinée et passait l'après-midi à vérifier le travail de la matinée. Il s'agissait de l'approximation la plus précise de π jusqu'à l'avènement de l'ordinateur, un siècle plus tard.

!!! cite "Au XX<sup>e</sup> siècle"
    - En 1910, le mathématicien indien Srinivasa Ramanujan a trouvé plusieurs séries infinies de π à convergence rapide. Il n'y avait pas d'ordinateur à cette époque.
    - En 1944, D. F. Ferguson, à l'aide d'une calculatrice mécanique, a constaté que William Shanks avait commis une erreur à la 528e décimale et que toutes les décimales suivantes étaient incorrectes.
    - Ensuite **tous** les calculs de $\pi$ se font avec calculatrice ou ordinateur. Très vite, $100\,000$ décimales sont calculées, puis des milliards.

!!! cite "Au XXI<sup>e</sup> siècle"
    Parmi les exploits réalisés, citons

    - Le 31 décembre 2009, en France, Fabrice Bellard a utilisé un ordinateur personnel pour calculer un peu moins de 3 mille milliards de chiffres, au bout de 131 jours.
    - Le 9 janvier 2022[^actuel], Google avec un super calculateur, en la personne d'Emma Haruka Iwao, a publié 100 mille milliards de chiffres, au bout de 157 jours de calculs.
    [^actuel]: Information sur le record actuel (fin 2022) du calcul de $\pi$ ; [Even more pi in the sky: Calculating 100 trillion digits of pi on Google Cloud](https://cloud.google.com/blog/products/compute/calculating-100-trillion-digits-of-pi-on-google-cloud?hl=en)

### Mots croisés

<iframe 
    title="Mots croisés autour de pi"
    width="700"
    height="1100"
    src="../mots-croises-pi.html">
</iframe>


## Approximation polygonale d'un cercle

<iframe scrolling="no" title="polygones" src="https://www.geogebra.org/material/iframe/id/vhff5gtv/width/665/height/766/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false" width="665px" height="766px" style="border:0px;"> </iframe>

Archimède, dans _De la mesure du cercle_, a créé le premier algorithme pour le calcul de π, basé sur l'idée que le périmètre de n'importe quel polygone inscrit dans un cercle est inférieur à la circonférence du cercle qui, à son tour, est inférieur au périmètre de tout polygone circonscrit à ce cercle. Il a commencé par des hexagones réguliers inscrits et circonscrits, dont les périmètres sont facilement déterminés. Il montre alors comment calculer les périmètres des polygones réguliers ayant deux fois plus de côtés qui sont inscrits et circonscrits autour du même cercle.

Il s'agit d'une procédure récursive qui serait décrite aujourd'hui comme suit :

Soit $p_n$ et $P_n$ les périmètres de polygones réguliers à $n$ côtés, inscrits et circonscrits autour du même cercle, respectivement. Alors, 

$$P_{2n} = \dfrac{2p_nP_n}{p_n+P_n},\qquad p_{2n}=\sqrt{p_nP_{2n}}$$

- la difficulté de la méthode, ici, est l'obtention de bonnes approximations pour les racines carrées dans les calculs impliqués.
- la trigonométrie est utilisée pour l'initialisation des variables et la démonstration de ces formules.
- Nous prouverons plus tard ces formules ainsi que l'encadrement.

> Essayons, avec les outils modernes comme Python de reproduire la méthode. Nous n'utiliserons à la fin aucun module externe ! Notre objectif est d'afficher rapidement 1000 décimales, ou plus, avec une méthode entièrement expliquée. 

### Obtenir une approximation de π avec des flottants

Dans un premier temps, on va utiliser les nombres flottants et le module `math`. Nous aurons une précision limitée, mais nous pourrons donner une approximation de $\pi$ avec 15 chiffres.

#### Comprendre la méthode

!!! question "Justifications"
    Dans la figure ci-dessous (cas $n=6$ puis $n=7$), donner les coordonnées des points $D$ et $F$ en fonction de $n$. Le cercle est de rayon $1$.

    ![](./asy/polygones-6.svg){.autolight}
    ![](./asy/polygones-7.svg){.autolight}

    - Pour $D$, c'est le cours.
    - Pour $F$, il y a un calcul à mener.
    - Pour $C$ et $E$ c'est simplement de la symétrie.

    ??? success "Réponses"
        L'angle formé par l'axe $(Ox)$ et $(OD)$ est **la moitié** de $\dfrac{2\pi}n$, soit $\dfrac{\pi}n$, ainsi

        D'après le cours, les coordonnées de $D$ sont $\left(\cos\left(\dfrac{\pi}n\right), \sin\left(\dfrac{\pi}n\right)\right)$

        Pour $F$, ses coordonnées sont $(1, y)$ où $y$ est un nombre positif à déterminer. Il y a une configuration de Thalès avec $(CD)/\!/(EF)$, où l'on peut déduire

        - $\dfrac y1 = \dfrac{\sin\left(\dfrac{\pi}n\right)}{\cos\left(\dfrac{\pi}n\right)}$, et donc
        - $y = \tan\left(\dfrac{\pi}n\right)$

        Ainsi $F$ a pour coordonnées $\left(1, \tan\left(\dfrac{\pi}n\right)\right)$


#### Construction de l'appliquette GeoGebra

!!! info "Protocole de construction"
    Pour ceux qui souhaitent recréer l'appliquette GeoGebra présentée plus haut, à partir d'une feuille vide.

    Entrer chaque ligne dans la ligne de saisie :

    ```{title="À saisir dans GeoGebra"}
    O = (0, 0)
    Cercle(O, 1)
    n = Curseur(3, 24, 1, 1, 150, false, true, true, false)
    D = (cos(pi/n), sin(pi/n))
    C = (x(D), -y(D))
    F = (1, tan(pi/n))
    E = (x(F), -y(F))
    Polygone(C, D, n)
    Polygone(E, F, n)
    ```

    ??? note "Explications détaillées"
        1. On construit le point $O$, origine du repère.
        2. On construit le cercle trigonométrique de centre $O$ et de rayon $1$.
        3. On construit un curseur `n`
            - `3, 24, 1, 1, ` : qui va de 3 à 24 par pas de 1, avec une vitesse de 1
            - `150, `qui mesure 150 pixels
            - `false, true, true, false` : qui n'est pas un angle, qui est horizontal, qui est animé, qui n'est pas aléatoire
        4. On construit les points $D$, $C$, $F$ et $E$ grâce à leurs coordonnées.
        5. On construit le polygone régulier à $n$ côtés **intérieur** en donnant deux points.
        6. On construit le polygone régulier à $n$ côtés **extérieur** en donnant deux points.

!!! example "Encadrement de $\pi$"
    Faire varier le curseur et en déduire que $2\pi$, la circonférence du cercle trigonométrique est encadré par les périmètres des deux polygones. En déduire la formule, pour $n>2$

    $$n\sin\left(\frac\pi n\right) < \pi < n\tan\left(\frac\pi n\right)$$


    !!! info "Preuve que le polygone inscrit a un périmètre inférieur au cercle"
        C'est simple, la corde est de longueur inférieur à chaque arc.
        
        En faisant la somme des $n$ côtés du polygone **intérieur** et des $n$ arcs, on obtient que le polygone intérieur a un périmètre inférieur à la circonférence du cercle.

        $$n×2×\sin\left(\frac\pi n\right) < 2×\pi$$

        En simplifiant par $2$, on obtient la première moitié de l'encadrement.

    !!! danger "Preuve que le polygone circonscrit a un périmètre supérieur au cercle"
        Cela semble évident, mais ça ne l'est pas du tout.[^preuve]
        [^preuve]: [Une discussion](https://math.stackexchange.com/questions/881668/algebraic-proof-of-tan-xx) sur une preuve simple de $\tan(x)>x$ pour $x$ un angle aigu.

        :warning: Il ne faut pas raisonner sur les longueurs, mais sur les aires, et on rappelle qu'on a ainsi prouver l'encadrement pour $x$ un angle aigu en radian, $\sin(x) < x < \tan(x)$. (Voir [ici](../4-fonctions/#une-figure-de-reference) pour le détail)

        En faisant la somme des $n$ côtés du polygone **extérieur** et des $n$ arcs, on obtient que le polygone extérieur a un périmètre supérieur à la circonférence du cercle.

        $$2×\pi < n×2×\tan\left(\frac\pi n\right)$$

        En simplifiant par $2$, on obtient la deuxième moitié de l'encadrement.

        Un autre raisonnement **intuitif** permet d'obtenir ce résultat, mais on ne peut pas travailler ainsi au lycée : il s'agit d'imaginer :
        
        1. que le polygone intérieur est souple, qu'on le gonfle, qu'il se déforme jusqu'à devenir un cercle. Pendant la déformation, les longueurs augmentent, ainsi le polygone intérieur a un périmètre inférieur à la circonférence du cercle. C'est presque la même démonstration que précédemment.
        2. que le cercle est souple, qu'on le gonfle, qu'il se déforme jusqu'à devenir le polygone extérieur. Pendant la déformation, les longueurs augmentent, ainsi le cercle a une circonférence inférieure au périmètre du polygone extérieur. Ce n'est pas du tout la même démonstration que précédemment.
    
    !!! tip "Preuve directe"
        Pour $n>2$ et $x=\dfrac{\pi}n$ qui est un angle aigu en radian, on a $\sin\left(\dfrac{\pi}n\right) < \dfrac{\pi}n < \tan\left(\dfrac{\pi}n\right)$.

        En multipliant par $n$, on obtient directement l'encadrement demandé.

#### Avec NumWorks

!!! example "Avec la calculatrice NumWorks"
    Tracer les représentations graphiques des trois suites

    1. $u_n = n\sin\left(\dfrac\pi n\right)$
    2. $v_n = \pi\quad$ ; une suite constante
    3. $w_n = n\tan\left(\dfrac\pi n\right)$

    Commentez vos observations.

    ??? success "Réponse"
        ![](./images/3_suites.png){ .bordure }
        ![](./images/graph_uvw.png){ .bordure }

        On constate que 
        
        1. $(u_n)$ semble croissante
        1. $(w_n)$ semble décroissante
        1. Ces suites semblent converger vers $\pi$
        1. $(u_n)$ semble deux fois plus proche de $(v_n)$ que $(w_n)$ ne l'est.

        Une bonne approximation de $\pi$ sera alors $\dfrac{2u_n + w_n}3$

#### Avec Python

!!! example "Avec Python"
    Écrire une fonction Python `calcul_pi_v1` de paramètre `n` qui renvoie $p_1(n) = \dfrac{n\left(2\sin\left(\dfrac\pi n\right) + \tan\left(\dfrac\pi n\right)\right)}3$

    Calculer la précision relative $\dfrac{\pi - p_1(n)}\pi$ pour $n$ parmi $[10, 100, 1000, 10000, 100000]$

    {{ IDE('pi1' )}}

    ??? success "Réponse"

        ```python
        from math import sin, tan, pi

        def calcul_pi_v1(n):
            return n * (2*sin(pi/n) + tan(pi/n)) / 3
        
        for n in [10, 1000, 10000, 100000]:
            p1 = calcul_pi_v1(n)
            print(n, (pi - p1)/pi)
        ```

        ```output
        10 -0.000504933811538179
        100 -4.8721720145915495e-08
        1000 -4.870630760200308e-12
        10000 -4.240739575284689e-16
        100000 1.4135798584282297e-16
        ```

!!! info "Méthode correcte, mais limitée"
    - On peut calculer $\pi$ avec cette méthode et obtenir 15 chiffres pour $n$ de l'ordre de 10000. Mais on n'aura pas plus de chiffres, la limitation vient du conteneur de type flottant ; la précision de la calculatrice.
    - Le **problème principal** est qu'on utilise $\pi$ pour calculer $\pi$... Il faut trouver comment s'en affranchir, sinon, autant utiliser `#!py print(pi)`
    - L'idée sera de calculer pour $n=3×2^k$ les valeurs de $u_n$ et $w_n$ que l'on calculera par récurrence sur $k$. On part de l'hexagone avec $n=6$ et on double le nombre de côtés, jusqu'à ce que $3×2^k$ soit de l'ordre de 10000.

    !!! question "Cas de l'hexagone"
        Calculer la valeur exacte de $u_6$ et $w_6$.

        Donner alors un premier encadrement de $\pi$

    ??? success "Réponse"
        - $u_6 = 6×\sin(\pi/6) = 3$
        - $w_6 = 6×\tan(\pi/6) = 2\sqrt3$

        On déduit

        $$3 < \pi < 2\sqrt3$$

        $$3 < \pi < 3,\!5$$

    Notre objectif est de doubler le nombre de côtés du polygone.

    La question suivante est alors : comment calculer $u_{2n}$ et $w_{2n}$ en fonction de $u_{n}$ et $w_{n}$ ?

    La question revient à calculer $\sin(a)$ et $\tan(a)$ en fonction de $\sin(2a)$ et $\tan(2a)$. C'est l'objet de notre nouveau paragraphe.

## Doublement du nombre de côtés

On rappelle les formules de trigonométrie vues à la page sur la duplication :

- $\sin(2a) = \dfrac{2\tan(a)}{1+\tan^2(a)}$
- $\cos(2a) = \dfrac{1-\tan^2(a)}{1+\tan^2(a)}$
- $\tan(2a) = \dfrac{2\tan(a)}{1-\tan^2(a)}$

!!! example "Exemple simple"
    Avec $a=\dfrac{\pi}{12}$, on pose $t = \tan\left(\dfrac{\pi}{12}\right)$.
    
    1. Justifier que $0<t<1$. (On pourra se contenter d'un schéma.)
    2. Choisir la bonne formule (parmi les trois précédentes) pour établir que $t^2-4t+1=0$.
    3. Prouver que $t = 2 - \sqrt 3$
    4. **Comment** pourriez-vous trouver une valeur exacte de $\sin\left(\dfrac{\pi}{12}\right)$ ? (_calcul non demandé, juste une explication_)

    ??? success "Réponse"
        1. Un bon argument est que la fonction $\tan$ est croissante sur $\left]\dfrac{-\pi}{2}\;;\;\dfrac{\pi}{2}\right[$, ainsi avec $0 < \dfrac{\pi}{12} < \dfrac{\pi}{4}$ on a : $\tan(0) < \tan \left(\dfrac{\pi}{12}\right) < \tan \left(\dfrac{\pi}{4}\right)$ d'où $0 < \tan \left(\dfrac{\pi}{12}\right) < 1$.
        2. On sait que $\sin \left(\dfrac{\pi}{6}\right) = \dfrac12$, que $\cos \left(\dfrac{\pi}{6}\right) = \dfrac{\sqrt3}2$ et que $\tan \left(\dfrac{\pi}{6}\right) = \dfrac{\sqrt3}3$. Le plus simple est de choisir la formule de $\sin(2a)$. (Mais toute autre serait valable.)
            - $\sin \left(\dfrac{2\pi}{12}\right) = \dfrac{2\tan\left(\dfrac{\pi}{12}\right)}{1+\tan^2\left(\dfrac{\pi}{12}\right)}$
            - $\dfrac12 = \dfrac{2t}{1+t^2}$
            - $t^2-4t+1=0$
        3. On a un trinôme du second degré en $t$, dont le discriminant est $\Delta = (-4)^2 - 4×1×1 = 16-4=12$. Les deux solutions sont $\dfrac{4+\sqrt{12}}2$ et $\dfrac{4-\sqrt{12}}2$. Or, on sait que $0<t<1$, on en déduit $t = \dfrac{4-\sqrt{12}}2$ que l'on peut simplifier en $t = 2-\sqrt 3$.
        4. Il y a plusieurs possibilités... En voici une :
            - Avec $\tan(2a)=\cdots$, et une résolution similaire à ce qui précède, on détermine $\tan \left(\dfrac{\pi}{24}\right)$ ;
            - On déduit $\sin \left(\dfrac{\pi}{12}\right)$ avec la formule de duplication $\sin(2a)=\cdots$.


!!! danger ":boom: :boom: :boom: Une méthode générale :boom: :boom: :boom:"
    On considère ici **un angle aigu** $a$ donc pour lequel $\sin(a) > 0$ et $\tan(a)$ existe.

    1. Additionner les formules, membre à membre, pour $\sin$ et $\tan$ et réduire au même dénominateur.
    2. Faire de même en faisant le produit, membre à membre, de ces deux formules.
    3. Faire le quotient, membre à membre, des deux dernières formules ainsi trouvées. On doit pouvoir obtenir $\tan(a)$ en fonction de $\sin(2a)$ et $\tan(2a)$.
    4. En multipliant, membre à membre, la définition $\tan(a)=\dfrac{\sin(a)}{\cos(a)}$ par la formule $\sin(2a)=2×\sin(a)×\cos(a)$, trouver une formule pour $\sin(a)$ en fonction de $\tan(a)$ et $\sin(2a)$.

    ??? success "Réponse 1"
        On additionne :

        - $\sin(2a) = \dfrac{2\tan(a)}{1+\tan^2(a)}$
        - $\tan(2a) = \dfrac{2\tan(a)}{1-\tan^2(a)}$

        On obtient :

        - $\sin(2a)+\tan(2a) = \dfrac{2\tan(a)}{1+\tan^2(a)} + \dfrac{2\tan(a)}{1-\tan^2(a)}$ ; on va factoriser par $2\tan(a)$ et réduire
        - $\sin(2a)+\tan(2a) = 2\tan(a)×\left(\dfrac{1-\tan^2(a)}{(1+\tan^2(a))×(1-\tan^2(a))} + \dfrac{1+\tan^2(a)}{(1+\tan^2(a))×(1-\tan^2(a))}\right)$, on déduit
        - $\sin(2a)+\tan(2a) = 2\tan(a)×\dfrac{1-\tan(a) + 1 +\tan(a)}{1-\tan^4(a)}$
        - $\sin(2a)+\tan(2a) = \dfrac{4\tan(a)}{1-\tan^4(a)}$

    ??? success "Réponse 2"
        On fait le produit :

        - $\sin(2a) = \dfrac{2\tan(a)}{1+\tan^2(a)}$
        - $\tan(2a) = \dfrac{2\tan(a)}{1-\tan^2(a)}$

        On obtient facilement :

        - $\sin(2a)\tan(2a) = \dfrac{4\tan^2(a)}{1-\tan^4(a)}$
    
    ??? success "Réponse 3"
        On obtient le quotient

        $\tan(a) = \dfrac{\sin(2a)\tan(2a)}{\sin(2a)+\tan(2a)}$

    ??? success "Réponse 4"
        - $\tan(a) × \sin(2a) = \tan(a) × \sin(2a)$
        - $\dfrac{\sin(a)}{\cos(a)}×2\sin(a)\cos(a) = \tan(a) × \sin(2a)$
        - $2\sin^2(a) = \tan(a) × \sin(2a)$, avec $\sin(a)>0$
        - $\sin(a) = \sqrt{\dfrac{\tan(a) × \sin(2a)}2}$

!!! info "Conclusion"
    On pose $a_k = u_{6×2^k}$ et $b_k = w_{6×2^k}$, on peut alors établir une relation de récurrence

    - $b_{k+1} = \dfrac{2×a_k×b_k}{a_k+b_k}$
    - $a_{k+1} = \sqrt{a_k×b_{k+1}}$

    On a les conditions initiales

    - $a_0 = 3$ et $b_0 = 2\sqrt3$

    ??? danger "Preuve"
        - $a_0 = 6×\sin{(\pi/6)} = 6×0.5 = 3$
        - $b_0 = 6×\tan{(\pi/6)} = 6×\dfrac{\sqrt3}3 = 2\sqrt3$
        - $a_{k} = u_{6×2^k} = 6×2^k ×\sin\left(\dfrac{\pi}{6×2^k}\right)$
        - $b_{k} = w_{6×2^k} = 6×2^k ×\tan\left(\dfrac{\pi}{6×2^k}\right)$
        - $b_{k+1} = w_{6×2^k×2} = 6×2^k×2 ×\tan\left(\dfrac{\pi}{2×6×2^k}\right)$
        - $\dfrac{2×a_k×b_k}{a_k+b_k} = \dfrac{2×6×2^k ×\sin\left(\dfrac{\pi}{6×2^k}\right)×6×2^k ×\tan\left(\dfrac{\pi}{6×2^k}\right)}{6×2^k ×\sin\left(\dfrac{\pi}{6×2^k}\right)+6×2^k ×\tan\left(\dfrac{\pi}{6×2^k}\right)}$
        - $\dfrac{2×a_k×b_k}{a_k+b_k} = \dfrac{2×6×2^k ×\sin\left(2×\dfrac{\pi}{2×6×2^k}\right)×\tan\left(2×\dfrac{\pi}{2×6×2^k}\right)}{\sin\left(2×\dfrac{\pi}{2×6×2^k}\right)+\tan\left(2×\dfrac{\pi}{2×6×2^k}\right)}$
        - $\dfrac{2×a_k×b_k}{a_k+b_k} = 2×6×2^k ×\tan\left(\dfrac{\pi}{2×6×2^k}\right) = b_{k+1}$

        Et pour l'autre,

        - $a_{k+1} = 2×6×2^k ×\sin\left(\dfrac{\pi}{2×6×2^k}\right)$

        - $a_{k+1} = 2×6×2^k×\sqrt{\dfrac{\tan\left(\dfrac{\pi}{2×6×2^k}\right) × \sin\left(2×\dfrac{\pi}{2×6×2^k}\right)}2}$

        - $a_{k+1} = \sqrt{\dfrac{b_{k+1} × 2 a_k}2}$

        - $a_{k+1} = \sqrt{b_{k+1} × a_k}$

!!! note "Exercice"
    Calculer $a_1$ et $b_1$ et donner un nouvel encadrement pour $\pi$.

    ??? success "Réponse"
        On calcule d'abord, avec $a_0 = 3$ et $b_0 = 2\sqrt3$,

        - $b_1 = \dfrac{2×a_0×b_0}{a_0+b_0}$
        - $b_1 = \dfrac{2×3×2\sqrt 3}{3+2\sqrt 3}$
        - $b_1 = \dfrac{(2×3×2\sqrt 3)(3-2\sqrt3)}{(3+2\sqrt 3)(3-2\sqrt3)}$
        - $b_1 = \dfrac{36\sqrt 3 - 72}{9-12}$
        - $b_1 = 12(2-\sqrt3)$

        Puis

        - $a_1 = \sqrt{a_0×b_1}$
        - $a_1 = \sqrt{3×12(2-\sqrt3)}$
        - $a_1 = 6\sqrt{2-\sqrt3}$

        On en déduit
        
        $$6\sqrt{2-\sqrt3} < \pi < 12(2-\sqrt3)$$

        $$3,\!10 < \pi < 3,\!22$$

!!! example "Construction avec Python"
    
    On commencera à résoudre le puzzle, puis le code à compléter.

    === "Façon puzzle"

        <iframe src="https://www.codepuzzle.io/IPNYX6" width="100%" height="600" frameborder="0"></iframe>

    === "Code à compléter"
        Compléter la fonction Python qui renvoie $(n_k, a_k, b_k)$ en fonction de $k$.

        {{ IDE('pi2') }}

    On pourra ensuite lancer le code, puis vérifier dans le terminal

    ```pycon
    >>> n, a, b = calcul_pi_v2(10)
    >>> n
    6144
    >>> p2 = (2*a + b) / 3
    >>> p2
    3.1415926535898038
    >>> from math import pi
    >>> (pi - p2) / pi
    -3.392591660227751e-15
    ```

    Ceci montre qu'avec **seulement 10 tours de boucles**, on a pu calculer une approximation de $\pi$ avec une précision de 15 chiffres environ, la limite des flottants.

    On n'a utilisé ni $\sin$, ni $\tan$, ni $\pi$ pour le calcul.

    :warning: **Mais**, on a utilisé la fonction `sqrt` du module `math`.

    Il est possible de faire ces calculs à la main, un peu moins précis, dès l'antiquité, sans calculatrice, c'est humainement possible. Il faut juste apprendre à extraire une racine carrée sans calculatrice !

## Obtenir une racine carrée

!!! warning "Triche"
    ```pycon
    >>> from math import sqrt, pi
    >>> sqrt(2)
    1.4142135623730951
    >>> pi
    3.141592653589793
    >>>
    ```

    Avec le module `math`, on a un accès direct à $\pi$ avec 15 chiffres après la virgules, de même on a un accès facile aux racines carrées, mais avec une précision limitée. 

    On va présenter la méthode de Héron d'Alexandrie pour le calcul de racine carrée avec des entiers aussi grands que l'on veut.


!!! question "Questions"
    1. Si un rectangle a pour aire $42~\text{m}^2$ et un côté qui mesure $6~\text{m}$, combien mesure l'autre côté ?
    2. Si un rectangle a pour aire $\mathscr A$ et un côté qui mesure $c$, combien mesure l'autre côté ?
    3. Si un **carré** a pour aire $\mathscr A$, combien mesure son côté ?

    ??? success "Réponses"
        1. L'autre côté mesure $42/6 = 7~\text{m}$
        2. L'autre côté mesure $\mathscr A/c$
        3. Le côté mesure $\sqrt{\mathscr A}$

!!! question "Questions"
    - On part d'un rectangle d'aire $\mathscr A$ dont un côté mesure $c$.
    - On fabrique un nouveau rectangle de même aire, dont un côté est la **moyenne** des deux côtés du **rectangle précédent**, avec pour objectif de le faire se rapprocher d'un carré.

    1. Donner une formule en fonction de $\mathscr A$ et $c$ qui donne le nouveau côté suivant ce principe.
    2. Appliquer ce principe à un rectangle qui a pour aire $42$ et un côté qui mesure $6$,
        - encore une fois avec le résultat obtenu,
        - et encore une fois avec le nouveau résultat obtenu.
    3. Que donne $\sqrt{42}$ sur calculatrice ?

    ??? success "Réponse"
        - Le nouveau côté sera $\dfrac{c + \mathscr A/c}2$
        - Appliqué une première fois avec $\mathscr A = 42$ et $c=6$, on obtient un côté $\dfrac{6 + 42/6}2 = 6.5$
        - Appliqué une seconde fois avec $\mathscr A = 42$ et $c=6.5$, on obtient un côté $\dfrac{6.5 + 42/6.5}2 \approx 6.480769$
        - Appliqué une troisième fois avec $\mathscr A = 42$ et $c=6.480769$, on obtient un côté $\dfrac{6.480740698 + 42/6.480740698}2 \approx 6.480740698$

        On constate que cette réponse permet d'obtenir rapidement une approximation de $\sqrt{42}$. Vérifiez !

!!! question "Question"
    Trouvez rapidement une approximation de $\sqrt{2023}$. On peut partir de n'importe quel nombre (entre $1$ et $2023$) pour le départ de la méthode, mais soyez malins !
    
    ??? success "Réponse"
        Oui, on peut partir de n'importe quel nombre, mais soyons malins.

        $16×100<2023<25×100$, donc $4×10 < \sqrt{2023} < 5×10$, on prendra $c_0 = 45$ comme point de départ.

        - $c_0 = 45$
        - $c_1 = \dfrac{45 + 2023/45}2 \approx 44.977777778$
        - $c_2 = \dfrac{44.977777778 + 2023/44.977777778}2 \approx 44.977772288$
        - $c_3 = \dfrac{44.977772288 + 2023/44.977772288}2 \approx 44.977772288$

        La méthode propose dès $c_2$ une très bonne approximation de $\sqrt{2023}$.

        :warning: Si on prend un moins bon départ, ou si on cherche une plus grande précision, il faudra faire plus que 2 tours !

!!! question "Questions"
    On souhaite appliquer cette méthode pour une racine carrée approximative d'un grand entier, par exemple $17^{83}$, dont on souhaite une valeur approchée entière de la racine carrée.

    1. Donner un point de départ malin, sachant que $17^{83} = 17 × 17^{82}$
    2. Utiliser Python **et uniquement des opérations sur les entiers** pour trouver une valeur entière proche de $\sqrt{17^{83}}$.
        - On utilisera `#!py a ** b` pour calculer $a^b$.
        - On utilisera `#!py a // b` pour calculer le quotient **entier** de `a` par `b`.
        - :warning: On n'utilisera pas `#!py a / b` ; il s'agit d'un quotient **flottant**
        - On utilisera une variable `c` pour stocker le résultat,
            - on pourra utiliser l'historique pour répéter facilement une action
            - il suffit de taper sur ++up+enter++

    {{ terminal() }}

    ??? success "Réponses"
        1. $17^{83} = 17 × 17^{82}$ donc un point de départ malin sera $4×17^{41}$ dont le carré est $16 × 17^{82}$
        2. Voici un exemple

        ```pycon
        >>> A = 17**83
        >>> A
        1340480418895698023067411176743717312981007012239556134605753941864386337047277655624320393544131928113
        >>> c = 4*17**41
        >>> c
        1123222431021074408192698456408147713970417460697668
        >>> c = (c + A // c) // 2
        >>> c
        1158323131990482983448720283170902330031993006344470
        >>> c = (c + A // c) // 2
        >>> c
        1157791303187916186853932073674496957061363073834670
        >>> c = (c + A // c) // 2
        >>> c
        1157791181040740585844149164117883175395545290202949
        >>> c = (c + A // c) // 2
        >>> c
        1157791181040734142569769628610748063500802096846666
        >>> c = (c + A // c) // 2
        >>> c
        1157791181040734142569769628592819189361054248906582
        >>> c = (c + A // c) // 2
        >>> c
        1157791181040734142569769628592819189361054248906582
        >>>
        ```

        On constate qu'en quelques étapes, on a une réponse _stabilisée_, et juste avant la moitié des chiffres étaient corrects.

        On peut vérifier que notre résultat est bien plus précis que

        ```pycon
        >>> from math import sqrt
        >>> sqrt(17 ** 83)
        1.1577911810407341e+51
        >>>
        ```

        La fin avec `e+51` signifie une écriture scientifique et que la réponse est environ $1.1577911810407341×10^{+51}$ et nous avons obtenu un résultat bien plus précis.


!!! question "Fonction Python"
    Compléter le code ci-dessous pour avoir une fonction `racine` telle que `racine(n, depart, marge)` renvoie une racine carrée entière approximative de `n`, avec la méthode de Héron, avec un nombre de `depart` donné et un arrêt quand deux termes construits consécutifs sont plus proches que la `marge` donnée.

{{ IDE('heron') }}


!!! tip "Indices"
    1. La variable `b` doit être initialisée avec le côté de ...
    2. La variable `c` reçoit le calcul où `n` est l'aire d'un carré et `b` en est un côté.
    3. `a = b` et `b = c` servent à sauvegarder les résultats pour progresser.
    4. Cliquez sur `Lancer` pour tester votre code.
    5. Cliquez sur `Valider` pour vérifier votre code ; s'il est correct la solution officielle apparait.
    6. Au bout de 5 erreurs, vous aurez automatiquement la solution ; mais essayez de chercher vous-même !
    7. Cet exercice existe aussi en [version Puzzle](https://www.codepuzzle.io/PKX8D)


!!! question "Exercice"
    Essayez de tester cette fonction avec

    ```pycon
    >>> racine(2*10**100, 10**50, 0)
    141421356237309504880168872420969807856967187537694
    ```

    Il s'agit de la racine **entière** de $2×10^{100}$, avec un point de départ à $10^{50}$. La marge est à $0$ pour indiquer que la boucle s'arrête sur deux valeurs successives identiques.

    Expliquer pourquoi $\sqrt2\approx 1.41421356237309504880168872420969807856967187537694$

    ??? success "Réponse"
        Un bon nombre de départ pour le calcul de $\sqrt{2×10^{100}}$ est $10^{50}$.

        Le résultat sera une très bonne approximation entière de $\sqrt2 ×10^{50}$

        Pour avoir une approximation décimale de $\sqrt 2$, il suffit de diviser par $10^{50}$, ce qui revient à décaler la virgule... On a ici une précision de 50 chiffres.

    Calculer une approximation décimale de $\sqrt 3$ avec 100 chiffres après la virgule.

## Obtenir une approximation de π avec une fraction de grands entiers

Si on veut obtenir une approximation de $\pi$ avec bien plus de chiffres (des milliers), il existe plusieurs techniques, mais on ne peut plus stocker l'approximation dans un flottant qui est limité à une quinzaine de chiffres.

L'idée que nous allons utiliser ici est de prendre un cercle trigonométrique avec un rayon, non pas de 1 unité, mais de $10^d$ pour $d$ le nombre de chiffres de travail, un peu plus que le nombre de chiffres désirés.

Par exemple, on pourra obtenir l'approximation suivante :

$$\pi \approx \frac{314159265358979323846264338327950288419716939937510}
                   {100000000000000000000000000000000000000000000000000}$$



- On n'utilise plus les `float`, uniquement des entiers aussi grands que nécessaires
- On n'utilise plus la fonction `sqrt` ni rien du module `math`, ni aucun autre module
- On n'utilise plus les divisions à résultat flottant, mais la division entière `//`
- On poursuit dans la boucle tant que la précision n'est pas atteinte.
- On utilise notre fonction maison `racine` et on donne des indications judicieuses pour démarrer la boucle de calcul. Par exemple :
    - Un entier proche de $K\sqrt3$ est la racine carrée entière de $3K^2$ pour lequel un bon départ sera $K$.
    - Un entier proche de $\sqrt{ab}$ sera $b$ que l'on vient de calculer, en effet, très vite $a$ et $b$ sont des nombres qui se rapprochent.

{{ IDE('pi3') }}

**Sans aucun module**, avec une méthode entièrement expliquée, on obtient 1000 décimales de $\pi$ en une fraction de seconde. Avec un peu de patience, on peut obtenir 10000 chiffres corrects. Pour des méthodes plus performantes, il faudra attendre le cours de maths expertes en terminale ou des cours du supérieur.

On constatera que dans ces 1000 premiers chiffres, il y a

- trois 0 consécutifs, (pourquoi pas),
- six 9 consécutifs, c'était très peu probable, mais le hasard inclut des surprises, **il n'y a aucune régularité**
- une répartition assez homogène des différentes combinaisons de 2 chiffres. Il est intéressant de faire des statistiques.

Aucune preuve n'existe à ce jour, mais on pense que $\pi$ est un nombre univers, il contiendrait toutes les séquences possibles de chiffres quelque part dans son écriture. Ainsi on pourrait retrouver en théorie tous les numéros de téléphone de ses amis, mais aussi des livres entiers numérisés, ainsi que leurs variantes qui comportent des fautes, ainsi que ce document numérisé. Tout, _potentiellement_, est écrit dans les décimales de $\pi$, même votre livre ou film préféré numérisé...

!!! example "10000 chiffres de pi"
    
    ![](./images/pi_10000.png){ .bordure }

    [Plus d'images de ce genre](https://evanstainsby.com/the-pi-pixel-project/)

!!! info "Fantastique ?"
    Si on regarde dans les 44 milliards premières décimales de $\pi$, on retrouve les 14 premiers chiffres. Plus précisément `31415926535897` apparait à partir de la $43\,420\,162\,171\,515$e décimale.

    [Michael J Swart](https://twitter.com/MJSwart/status/1537098675045924872) pense que c'est fantastique. Rendez-vous en fin d'année avec le chapitre probabilité pour déterminer si c'est plausible ou fantastique.

