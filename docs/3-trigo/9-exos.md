# Exercices corrigés

Ces exercices sont corrigés en classe avec beaucoup plus de détails à l'oral.
Vous devez être capable de pouvoir les refaire, les grandes lignes vous sont données ici, pour vous aider à les refaire.

## Ex 62 p 197

Pour chaque question, il est conseillé de faire un schéma qui illustre chaque angle avec son point image.

??? note "Question 1"

    - $\cos\left(\dfrac{-\pi}3\right) = \cos\left(\dfrac{\pi}3\right) = \dfrac12$
    - $\sin\left(\dfrac{-7\pi}4\right) = \sin\left(\dfrac{-7\pi}4 + 2\pi\right) = \sin\left(\dfrac{\pi}4\right) = \dfrac{\sqrt2}2$

    Ainsi $\cos\left(\dfrac{-\pi}3\right) - \sin\left(\dfrac{-7\pi}4\right) = \dfrac12 - \dfrac{\sqrt2}2 = \dfrac{1-\sqrt2}2$

??? note "Question 2"

    - $\cos\left(\dfrac{5\pi}3\right) = \cos\left(\dfrac{5\pi}3 - 2\pi\right) = \cos\left(\dfrac{-\pi}3\right) = \cos\left(\dfrac{\pi}3\right) = \dfrac{1}2$
    - $\sin(2\pi) = \sin(0) = 0$
    - $\cos\left(\dfrac{-\pi}6\right) = \cos\left(\dfrac{\pi}6\right) = \dfrac{\sqrt3}2$

    Ainsi $\cos\left(\dfrac{5\pi}3\right) - \sin(2\pi) + \cos\left(\dfrac{-\pi}6\right) = \dfrac{1}2 - 0 + \dfrac{\sqrt3}2 = \dfrac{1+\sqrt3}2$

??? note "Question 3"

    - $\cos(-2018\pi) = \cos(-1009×2\pi) = \cos(0) = 1$
    - $\cos\left(\dfrac{-\pi}4\right) = \cos\left(\dfrac{\pi}4 \right) = \dfrac{\sqrt2}2$
    - $\sin\left(\dfrac{3\pi}2\right) =\sin\left(\dfrac{\pi}2 + \pi\right)=-\sin\left(\dfrac{\pi}2 \right) = -1$
    - $\sin\left(\dfrac{\pi}4\right) = \dfrac{\sqrt2}2$

    Ainsi $\cos(-2018\pi) - \cos\left(\dfrac{-\pi}4\right) + \sin\left(\dfrac{3\pi}2\right) - \sin\left(\dfrac{\pi}4\right) = 1 - \dfrac{\sqrt2}2 -1 - \dfrac{\sqrt2}2 = -\sqrt 2$

??? note "Question 4"

    - $\cos\left(\dfrac{\pi}6\right) = \dfrac{\sqrt3}2$
    - $\sin\left(\dfrac{\pi}3\right) = \dfrac{\sqrt3}2$
    - $\sin\left(\dfrac{\pi}2\right) = 1$
    - $\sin\left(\dfrac{4\pi}3\right) = \sin\left(\dfrac{\pi}3+\pi\right) = -\sin\left(\dfrac{\pi}3\right) = - \dfrac{\sqrt3}2$

    Ainsi $\cos\left(\dfrac{\pi}6\right)  + \sin\left(\dfrac{\pi}3\right) - \sin\left(\dfrac{\pi}2\right) + \sin\left(\dfrac{4\pi}3\right) = \dfrac{\sqrt3}2 + \dfrac{\sqrt3}2 - 1 - \dfrac{\sqrt3}2 =\dfrac{\sqrt3}2 + (-1)= \dfrac{\sqrt3 - 2}{2}$

## Ex 63 p 197

??? note "Question 1"

    Pour tout $x\in\mathbb R$, on a $\cos^2\left(x\right) + \sin^2\left(x\right) = 1$


    Ainsi $\cos^2\left(\dfrac{-\pi}{13}\right) + \sin^2\left(\dfrac{-\pi}{13}\right) = 1$

??? note "Question 2"

    - $\cos\left(\dfrac{-\pi}{6}\right) = \cos\left(\dfrac{\pi}{6}\right) = \dfrac{\sqrt 3}2$
    - $\sin\left(\dfrac{-\pi}{6}\right) = -\sin\left(\dfrac{\pi}{6}\right) = \dfrac{-1}2$


    Ainsi $\cos^2\left(\dfrac{-\pi}{6}\right) - \sin^2\left(\dfrac{-\pi}{6}\right) = \dfrac 3 4 - \dfrac 1 4 = \dfrac 1 2$

??? note "Question 3"

    - $\sin\left(\dfrac{-5\pi}{6}\right) =-\sin\left(\dfrac{5\pi}{6}\right) =-\sin\left(\pi-\dfrac{\pi}{6}\right) =-\sin\left(\dfrac{\pi}{6}\right) =\dfrac{-1}2$

    - $\cos\left(\dfrac{2\pi}{3}\right) = \cos\left(\pi-\dfrac{\pi}{3}\right) = -\cos\left(\dfrac{\pi}{3}\right) = \dfrac{-1}2$
    - $\cos\left(-\pi\right) = -1$

    Ainsi $\sin\left(\dfrac{-5\pi}{6}\right) × \cos\left(\dfrac{2\pi}{3}\right) - \cos\left(-\pi\right) = \dfrac{-1}2×\dfrac{-1}2-(-1)=\dfrac 1 4 + 1 = \dfrac 5 4$

??? note "Question 4"

    - $\sin\left(\dfrac{\pi}4\right) = \dfrac{\sqrt2}2$
    - $\cos\left(\dfrac{\pi}3\right) = \dfrac{1}2$

    Ainsi $\dfrac{\sin\left(\dfrac{\pi}4\right)}{\cos^2\left(\dfrac{\pi}3\right)} = \dfrac{\dfrac{\sqrt2}2}{\left(\dfrac{1}2\right)^2} = 2\sqrt2$

## Ex 73 p 198

On donne $\sin\left(\dfrac{7\pi}{12}\right)=\dfrac{\sqrt 2 + \sqrt 6}4$

Avec $\cos^2(x) + \sin^2(x) = 1$ pour tout $x\in\mathbb R$, on déduit

- $\cos^2\left(\dfrac{7\pi}{12}\right) + \sin^2\left(\dfrac{7\pi}{12}\right)=1$
- $\cos^2\left(\dfrac{7\pi}{12}\right) + \left(\dfrac{\sqrt 2 + \sqrt 6}4\right)^2=1$
- $\cos^2\left(\dfrac{7\pi}{12}\right) + \dfrac{2 + 2\sqrt{2×6} + 6}{16}=1$
- $\cos^2\left(\dfrac{7\pi}{12}\right) + \dfrac{8 + 2\sqrt{4×3}}{16}=1$
- $\cos^2\left(\dfrac{7\pi}{12}\right) + \dfrac{8 + 4\sqrt{3}}{16}=1$
- $\cos^2\left(\dfrac{7\pi}{12}\right) + \dfrac{2 + \sqrt{3}}{4}=1$
- $\cos^2\left(\dfrac{7\pi}{12}\right) = \dfrac{4- (2 + \sqrt{3})}{4}$
- $\cos^2\left(\dfrac{7\pi}{12}\right) = \dfrac{2 - \sqrt{3}}{4}$

Or $\dfrac{\pi}{2}<\dfrac{7\pi}{12} < \pi$, donc on déduit $\cos\left(\dfrac{7\pi}{12}\right) < 0$ et donc

- $\cos\left(\dfrac{7\pi}{12}\right) = -\sqrt{\dfrac{2 - \sqrt{3}}{4}}$

### Variante

On constate que 

- $A = \left(\dfrac{\sqrt 2 + \sqrt 6}4\right)^2 + \left(\dfrac{\sqrt 2 - \sqrt 6}4\right)^2$
- $A = \dfrac{2 + 2×\sqrt{2×6}+ 6}{16} + \dfrac{2 -2×\sqrt{2×6}+6}{16}$
- $A = \dfrac{16}{16}$
- $A=1$

On en déduit que

$$\cos\left(\dfrac{7\pi}{12}\right) = \dfrac{\sqrt 2 - \sqrt 6}4$$

On trouve donc une autre façon d'écrire la réponse qui est bien négative.


## Ex 82 p 200

On rappelle que $\cos(\alpha+\beta) = \cos(\alpha)\cos(\beta)-\sin(\alpha)\sin(\beta)$

Avec $x=\alpha=\beta$, on tire $\cos(2x) = \cos^2(x)-\sin^2(x)$

Or avec $1 = \cos^2(x)+\sin^2(x)$, en ajoutant ces deux lignes, on tire

$1+\cos(2x) = 2\cos^2(x)$ d'où on déduit

$$\cos^2(x) = \frac{1+\cos(2x)}2$$

On sait que $\cos\left(\dfrac{\pi}4\right)=\dfrac{\sqrt 2}2$.

Avec $x=\dfrac{\pi}8$, on a $2x=\dfrac{\pi}4$ on déduit que

- $\cos^2\left(\dfrac{\pi}8\right) = \dfrac{1+\cos\left(\dfrac{\pi}4\right)}2$
- $\cos^2\left(\dfrac{\pi}8\right) = \dfrac{2+\sqrt 2}4$

Et avec $\cos\left(\dfrac{\pi}8\right) > 0$, on déduit

$$\cos\left(\dfrac{\pi}8\right) = \dfrac{\sqrt{2+\sqrt 2}}2$$


- $\sin^2\left(\dfrac{\pi}8\right) = \dfrac{4-(2+\sqrt 2)}4$
- $\sin^2\left(\dfrac{\pi}8\right) = \dfrac{2-\sqrt 2}4$

Et avec $\sin\left(\dfrac{\pi}8\right) > 0$, on déduit

$$\sin\left(\dfrac{\pi}8\right) = \dfrac{\sqrt{2-\sqrt 2}}2$$

!!! warning "Questions pour vérifier"
    - Comment expliquer que $\cos\left(\dfrac{\pi}8\right) > 0$ ?
    - Comment expliquer que $\sin\left(\dfrac{\pi}8\right) > 0$ ?
    - Savez-vous refaire les calculs sans aide et sans vous tromper ?
