size(10cm,0);
defaultpen(fontsize(14pt)+black); // Stylo par défaut

import geometry;

pair O=(0,0), pI=(1,0), pJ=(0,1);
dot("$O$", O, SW);
draw(unitcircle);

real angle1=30;
pair pM=dir(angle1);
draw(-pJ--pJ);
draw(-pI--pI);
draw(O--pM);
draw("$x$",arc(pI,O,pM,0.35),blue,Arrow);
pair pcos1=(Cos(angle1),0), psin1=(0,Sin(angle1));
draw(pM--pcos1^^pM--psin1,blue+dashed);
perpendicular(pcos1,NW,blue);
perpendicular(psin1,SE,blue);

dot("$M(\cos(x),\sin(x))$", pM, dir(O--pM));
dot("$\cos(x)$", pcos1, S+W/2);
dot("$\sin(x)$", psin1, W);

dot("$0$", pI, E, blue);
dot("$\frac{\pi}2$", pJ, N, blue);
dot("$\pi$", -pI, W, blue);
dot("$\frac{3\pi}2$", -pJ, S, blue);

draw("+",arc(dir(40),O,dir(80),1.2),0.75bp+blue, Arrow);
