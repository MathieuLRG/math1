import geometry;
defaultpen(fontsize(14pt)+black); // Stylo par défaut

size(5cm);

triangle t=triangleabc(10, 10, 10, angle=-120);
show(
     La="",Lb="2",Lc="2",
     t,1bp+black);

pair H = (t.B + t.C)/2;

dot("$H$", H, S);
draw(t.A--H);
label("1", (H+t.C)/2, S);
label("1", (H+t.B)/2, S);

perpendicularmark(line(t.A,H),line(H,t.C), quarter=2);
