import geometry;
defaultpen(fontsize(14pt)+black); // Stylo par défaut
size(6cm);
real qt = asin(1);
triangle t=triangleAbc(90, 13, 13/tan(22*qt/90));
show(La="?", Lb="", Lc="$13$ cm", t, linewidth(1bp));
markangle("$22^\circ$", 2, radius=1.5cm, t.C, t.B, t.A);

perpendicularmark(line(t.A,t.B),line(t.A,t.C), quarter=1);
