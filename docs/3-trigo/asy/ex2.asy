import geometry;
defaultpen(fontsize(14pt)+black); // Stylo par défaut

size(7cm);

triangle t2=triangleAbc(90, 13, 21, angle=10);
show(LA="$R$",LB="$S$",LC="$T$",
     La="",Lb="$13\,\textrm{km}$",Lc="$21\,\textrm{km}$",
     t2,1bp+black);
markangle("?",1,radius=1.5cm,
          t2.C,t2.B,t2.A);        
          
perpendicularmark(line(t2.A,t2.B),line(t2.A,t2.C), quarter=1);
