size(10cm,0);
defaultpen(fontsize(14pt)+black); // Stylo par défaut

import geometry;

pair O=(0,0), pI=(1,0), pJ=(0,1);
dot("$O$", O, SW);
draw(unitcircle);

real a=150, b=110;

pair pA=dir(a), pB=dir(b), pE=dir(a-b);
draw(-1.1*pJ--1.1*pJ, Arrow);
draw(-1.1*pI--1.1*pI, Arrow);

draw(O--pE);
draw("$a-b$",arc(pI,O,pE,0.45),blue,Arrow);
draw(O--pB);
draw("$b$",arc(pI,O,pB,0.2),blue,Arrow);
draw(O--pA);
draw("$a$",arc(pJ,O,pA,0.35),blue,Arrow);
draw("",arc(pI,O,pJ,0.35),blue);

draw("+",arc(dir(40),O,dir(80),1.35),0.75bp+blue, Arrow);

draw(pA--pB);
draw(pE--pI);

dot("$A$", pA, dir(O--pA));
dot("$B$", pB, dir(O--pB));
dot("$E$", pE, dir(pE));
dot("$D$", pI, NE);

label("$x$", (1,0), SE);
label("$y$", (0,1), NE);
