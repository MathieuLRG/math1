size(10cm,0);
defaultpen(fontsize(14pt)+black); // Stylo par défaut

import geometry;

pair O=(0,0), pI=(1,0);
dot("$O(0, 0)$", O, SW);
dot("$I(1, 0)$", pI, SE);

real angle=27;
pair pM=dir(angle), pN=(1, Tan(angle));
draw(O--pI--pN--O);
draw(pI--pM);
draw("$x$",arc(pI,O,pM,0.35),blue,Arrow);

pair pcos1=(Cos(angle),0);
draw(pM--pcos1, blue+dashed);
perpendicular(pcos1,NW,blue);
perpendicular(pI,NW,blue);

dot("$M$", pM, dir(pI--pM));
dot("$N$", pN, E);

draw("", arc(dir(0),O,dir(angle),1), 0.75bp+blue);
