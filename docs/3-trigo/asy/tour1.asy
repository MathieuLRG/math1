size(7.5cm,0);
import base_pi; 
defaultpen(fontsize(16pt)+black); // Stylo par défaut

draw(unitcircle, 2bp+black);

int k=24;
real a=360/k;
for (int i=0; i<k; ++i)
  {
    if (i==0 || (pgcd(i,2)!=1 || pgcd(i,3)==3)) {
    pair pC=dir((i)*a);
    label(texfrac(2*(i),k,factor="\pi"),pC,align=pC);
    draw((0,0)--pC);
    }
  }