#--- HDR ---#
def _cos_(x):
    assert -pi/4 <= x <= pi/4, "Interdit en dehors de l'intervalle"
    s = 0
    xn = 1
    f = 1
    for i in range(1, 17):
        if i % 2 == 1:
            s += xn / f
        xn *= x
        f *= i
    return s

def _sin_(x):
    assert -pi/4 <= x <= pi/4, "Interdit en dehors de l'intervalle"
    s = 0
    xn = 1
    f = 1
    for i in range(1, 17):
        if i % 2 == 0:
            s += xn / f
        xn *= x
        f *= i
    return s
#--- HDR ---#
pi = 3.141592653589793

def cos(x):
    x = x % (2*pi)
    if x > pi:
        x = x - 2*pi

    if x < 0:
        x = -x

    if x > pi/2:
        signe = -1
        x = pi - x
    else:
        signe = +1
    
    if x > pi/4:
        x = pi/2 - x
        return signe * _sin_(x)
    else:
        return signe * _cos_(x)

def sin(x):
    x = x % (2*pi)
    if x > pi:
        x = x - 2*pi

    if x < 0:
        x = -x
        signe = -1
    else:
        signe = +1

    if x > pi/2:
        x = pi - x
    
    if x > pi/4:
        x = pi/2 - x
        return signe * _cos_(x)
    else:
        return signe * _sin_(x)


# Tests

assert cos(0) == 1
assert sin(0) == 0
assert cos(pi/2) == 0
assert sin(pi/2) == 1
assert cos(pi) == -1
assert sin(pi) == 0
assert cos(2*pi) == 1
assert sin(2*pi) == 0
