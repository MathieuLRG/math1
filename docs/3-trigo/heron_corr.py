def racine(n, depart, marge):
    " Algorithme de Héron d'Alexandrie "
    a = 0
    b = depart
    while abs(b - a) > marge:
        c = (b + n//b) // 2
        a = b
        b = c
    return b
