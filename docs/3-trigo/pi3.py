def racine(n, depart, marge):
    " Algorithme de Héron d'Alexandrie "
    a = 0
    b = depart
    while abs(b - a) > marge:
        c = (b + n//b) // 2
        a = b
        b = c
    return b

def calcule_pi(nb_chiffres):
    " Algorithme d'Archimède "
    extra = 6
    coeff = 10 ** (nb_chiffres + 2 * extra)
    marge = 10 ** extra

    a = 3 * coeff
    b = 2 * racine(3 * coeff * coeff, coeff, marge)

    while abs(b - a) > marge:
        b = (2 * a * b) // (a + b)
        a = racine(b * a, b, marge)
    return (2*a + b) // 3

def affiche_pi(nb_chiffres):
    " On découpe la grande chaine de caractères "
    d = 50  # nb_chiffres par ligne
    pi = str(calcule_pi(nb_chiffres))[1:]
    print("pi est environ égal à")
    print("3." + pi[:d])
    for i in range(d, len(pi) - d, d):
        print(" ", pi[i:i+d])

affiche_pi(nb_chiffres=1000)
