# Introduction

==À reprendre==

## Contexte

Dans ce chapitre, on travaille avec des fonctions usuelles, et celles que l'on construit facilement avec. Des fonctions numériques à une seule variable réelle.

Dans les études supérieures, on rencontre

- des fonctions à plusieurs variables, et on dérive par rapport à l'une et/ou l'autre...
- des _monstres_ : des fonctions curieuses, continues, mais nulle part dérivables...

Dans ces conditions, on donne des exemples de fonctions simples que l'on envisage :

1. $f(x) = (2x-1)(x+3)$ sur $\mathbb R$
1. $f(x) = x^2(\sqrt x + 1)$ sur $[0\,;\,+\infty[$
1. $f(x) = \dfrac{x^3+1}{x^2-1}$ sur $\mathbb R\setminus \{-1\,;\,+1\}$
1. $f(x) = \dfrac{x^2+x+1}{\sqrt x}$ sur $]0\,;\,+\infty[$
1. $f(x) = \dfrac{\sqrt x}{x-1}$ sur $]1\,;\,+\infty[$

Une telle fonction possède une courbe représentative.

Pour **presque chaque point** de la courbe, on peut tracer une tangente à la courbe.

Lorsque c'est le cas pour une fonction $f$ au point d'abscisse $x_0$, on peut écrire en notant $f'(x_0)$ le coefficient directeur de cette tangente.

$$f(x) \approx f(x_0) + f'(x_0)×(x - x_0)  \text{ pour } x\approx x_0$$


## Le nombre dérivé

Pour déterminer le nombre dérivé $f(x_0)$, on peut utiliser la notion de limite, quand elle existe.

<iframe scrolling="no" title="nombre dérivé" src="https://www.geogebra.org/material/iframe/id/cq7mqjjj/width/800/height/600/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/true/asb/false/sri/false/rc/false/ld/false/sdz/true/ctl/false" width="800px" height="600px" style="border:0px;"> </iframe>

!!! exemple "Example"
    Jouez avec cette appliquette GeoGebra.

    1. Déplacer le point $A$,
    2. Faire varier le curseur $h$, pour que le point $B$ soit plus ou moins proche de $A$,
    3. Regarder l'équation de la tangente ; le coefficient directeur $m$ est affiché en direct.

    Vous pouvez changer la fonction $f$ à loisir.

    ??? info "Comment est construite cette appliquette"
        1. Une fonction $f$ est crée seulement en donnant une expression littérale en $x$.
        2. Un point $A$ est créé sur la courbe représentative.
        3. Un curseur est créé, un nombre réel de $0.01$ à $1$, par pas de $0.05$, il est initialisé à $h = 0.5$.
        4. Un point $B$ est créé :
            - son abscisse est $A_x + h$ et son ordonnée sera l'image par $f$,
            - avec GeoGebra : `(x(A) + h, f(x(A) + h))`
        5. La droite $(AB)$ est créée, c'est une **approximation** de la tangente à $f$ au point $A$.
        6. Le nombre réel $m$ peut se déterminer avec $g(1) - g(0)$.

        Tout est dynamique !

## L'équation de la tangente

Si une fonction $f$ est dérivable en $x_0$, alors la courbe représentative de $f$, que l'on note $\mathcal C_f$ possède une tangente d'équation :

$$T_{x_0} : y = f(x_0) + f'(x_0)×(x - x_0)$$

!!! example "Exemple"
    Si $f$ est dérivable en $5.1$, avec

    - $f(5.1) = -3.2$
    - $f'(5.1) = 7.8$

    alors $\mathcal C_f$ admet une tangente au point $A$ d'abscisse $(5.1\,;\,-3.2)$, d'équation

    $$T_A : y = -3.2 + 7.8×(x - 5.1)$$

    On vérifie sans mal que cette tangente passe par $A$ et possède un coefficient directeur égal à $7.8$.


!!! tip "La réciproque est vraie"
    Si une fonction $f$ possède une courbe représentative $\mathcal C_f$ avec une tangente au point d'abscisse $x_0$, dont l'équation est :

    $$T_{x_0} : y = f(x_0) + m×(x - x_0)$$

    Alors $f$ est dérivable en $x_0$ et on a $f'(x_0) = m$.

## Dériver une fonction

On procède par analyse de la fonction :

- Est-ce une fonction usuelle ?
    - Si oui, on connait sa dérivée. (Le tableau de résumé est à apprendre)
    - Si non, quelle est la **dernière** opération en terme priorité pour la construire ? On la décompose.

