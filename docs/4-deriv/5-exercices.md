# Exercices

==Il y a aussi des exercices corrigés à la page des formules.==

## Exercices élémentaires

### 28 p 119 (produit ou inverse de fonctions usuelles)

Dériver les fonctions suivantes telles que

??? example "1. $f(x) = \dfrac1x (x^3-1)$ sur $\mathbb R^*$"
    On pose

    $$\begin{align*}
    u(x) &= \dfrac1x & u'(x) &= \dfrac{-1}{x^2}\\
    v(x) &= x^3 -1   & v'(x) &= 3x^2 + 0\\
    \end{align*}$$

    On a :

    $$\begin{align*}
    f'(x) &= (u(x)×v(x))'\\
    f'(x) &= u'(x)×v(x) + u(x)×v'(x)\\
    f'(x) &= \dfrac{-1}{x^2}×(x^3-1)  + \dfrac{1}{x}×(3x^2)\\
    f'(x) &= \dfrac{-1}{x^2}×(x^3-1)  + \dfrac{1}{x^2}×(3x^3)\\
    f'(x) &= \dfrac{-x^3 + 1 + 3x^3}{x^2}\\
    f'(x) &= \dfrac{2x^3 + 1}{x^2}\\
    \end{align*}$$

??? example "2. $f(x) = x^2(\sqrt x + 1)$ sur $[0\,;\,+\infty[$"
    On pose

    $$\begin{align*}
    u(x) &= x^2          & u'(x) &= 2x\\
    v(x) &= \sqrt x + 1  & v'(x) &= \dfrac1{2×\sqrt x} + 0\\
    \end{align*}$$

    On a :

    $$\begin{align*}
    f'(x) &= (u(x)×v(x))'\\
    f'(x) &= u'(x)×v(x) + u(x)×v'(x)\\
    f'(x) &= 2x × (\sqrt x + 1) + x^2 × \dfrac1{2×\sqrt x}\\
    f'(x) &= 2x × (\sqrt x + 1) + \dfrac12 ×x×\sqrt x\\
    f'(x) &= \dfrac52 x \sqrt x + 2x\\
    \end{align*}$$


??? example "3. $f(x) = \dfrac1{x^2+1}$ sur $\mathbb R$"
    On pose $v(x) = x^2 + 1$, on a $v'(x) = 2x + 0$

    On déduit :

    $$\begin{align*}
    f'(x) &= \left(\dfrac1{v(x)}\right)'\\
    f'(x) &= \dfrac{-v'(x)}{v^2(x)}\\
    f'(x) &= \dfrac{-2x}{(x^2+1)^2}\\
    \end{align*}$$

??? example "4. $f(x) = \dfrac1{\sqrt x}$ sur $]0\,;\,+\infty[$"
    On pose $v(x) = \sqrt x$, on a $v'(x) = \dfrac1{2\sqrt x}$

    On déduit :

    $$\begin{align*}
    f'(x) &= \left(\dfrac1{v(x)}\right)'\\
    f'(x) &= \dfrac{-v'(x)}{v^2(x)}\\
    f'(x) &= \dfrac{-\dfrac1{2\sqrt x}}{(\sqrt x)^2} \text{  , avec $x$ positif}\\
    f'(x) &= \dfrac1{-2\sqrt x}×\dfrac{1}{x}\\
    f'(x) &= \dfrac1{-2x\sqrt x}\\
    \end{align*}$$

### 29 p 119 (produit ou quotient de fonctions usuelles)

??? example "1. $f(x) = \dfrac{x+1}{x-2}$ sur $\mathbb R\setminus \{2\}$"
    On pose

    $$\begin{align*}
    u(x) &= x+1  & u'(x) &= 1\\
    v(x) &= x-2  & v'(x) &= 1\\
    \end{align*}$$

    On a :

    $$\begin{align*}
    f'(x) &= \left(\dfrac{u(x)}{v(x)}\right)'\\
    f'(x) &= \dfrac{u'(x)×v(x) - u(x)×v'(x)}{v^2(x)}\\
    f'(x) &= \dfrac{1×(x-2) - (x+1)×1}{(x-2)^2}\\
    f'(x) &= \dfrac{-3}{(x-2)^2}\\
    \end{align*}$$

??? example "2. $f(x) = \dfrac{x^3+1}{x^2-1}$ sur $\mathbb R\setminus \{-1\,;\,+1\}$"
    On pose

    $$\begin{align*}
    u(x) &= x^3+1  & u'(x) &= 3x^2\\
    v(x) &= x^2-1  & v'(x) &= 2x\\
    \end{align*}$$

    On a :

    $$\begin{align*}
    f'(x) &= \left(\dfrac{u(x)}{v(x)}\right)'\\
    f'(x) &= \dfrac{u'(x)×v(x) - u(x)×v'(x)}{v^2(x)}\\
    f'(x) &= \dfrac{(3x^2)×(x^2-1) - (x^3+1)×(2x)}{(x^2-1)^2}\\
    f'(x) &= \dfrac{3x^4 - 3x^2 - 2x^4 -2x}{(x^2-1)^2}\\
    f'(x) &= \dfrac{x^4 - 3x^2 -2x}{(x^2-1)^2}\\
    f'(x) &= \dfrac{x(x^3 - 3x -2)}{(x^2-1)^2}\\
    \end{align*}$$

    $-1$ est une racine évidente de $x^3-3x-2$, on peut alors le factoriser par $(x+1)$, on a alors un polynôme du second degré. Ce qui permet ensuite d'étudier le signe de $f'(x)$, ce qui permet ensuite d'étudier le sens de variation de $f$.

    Rappel : on aime les expressions factorisées, on ne les développe que lorsqu'on est obligé !

??? example "3. $f(x) = \dfrac{\sqrt x}{x-1}$ sur $]1\,;\,+\infty[$"
    On pose

    $$\begin{align*}
    u(x) &= \sqrt x  & u'(x) &= \dfrac1{2\sqrt x}\\
    v(x) &= x - 1    & v'(x) &= 1\\
    \end{align*}$$

    On a :

    $$\begin{align*}
    f'(x) &= \left(\dfrac{u(x)}{v(x)}\right)'\\
    f'(x) &= \dfrac{u'(x)×v(x) - u(x)×v'(x)}{v^2(x)}\\
    f'(x) &= \dfrac{\dfrac1{2\sqrt x}×(x-1) - \sqrt x×1}{(x-1)^2}\\
    f'(x) &= \dfrac{\dfrac{x-1 - 2\sqrt x×\sqrt x}{2\sqrt x}}{(x-1)^2}\\
    f'(x) &= \dfrac{-(x+1)}{(x-1)^2×2\sqrt x}\\
    \end{align*}$$

??? example "4. $f(x) = \dfrac{x^2+x+1}{\sqrt x}$ sur $]0\,;\,+\infty[$"

    :warning: il y avait une erreur d'énoncé. $0$ est exclu.

    On pose

    $$\begin{align*}
    u(x) &= x^2+x+1   & u'(x) &= 2x+1\\
    v(x) &= \sqrt x   & v'(x) &= \dfrac1{2\sqrt x}\\
    \end{align*}$$

    On a :

    $$\begin{align*}
    f'(x) &= \left(\dfrac{u(x)}{v(x)}\right)'\\
    f'(x) &= \dfrac{u'(x)×v(x) - u(x)×v'(x)}{v^2(x)}\\
    f'(x) &= \dfrac{(2x+1)×\sqrt x - (x^2+x+1)×\dfrac1{2\sqrt x}}{(\sqrt x)^2}\\
    f'(x) &= \dfrac{(2x+1)×2x - (x^2+x+1)}{2x\sqrt x}\\
    f'(x) &= \dfrac{x^2+x-1}{2x\sqrt x}\\
    \end{align*}$$

## Entrainement

### 80 p 126 (Deux méthodes pour dériver)

Pour chaque fonction : calculer sa dérivée directement, puis recommencer en développant d'abord l'expression.

??? example "1. $f(x) = (2x+3)(1-4x)$ sur $\mathbb R$"
    On a d'une part

    $$\begin{align*}
    f'(x) &= 2(1-4x) + (2x+3)×(-4)\\
    f'(x) &= 2-8x + -8x-12\\
    f'(x) &= -16x -10\\
    \end{align*}$$

    D'autre part

    $$\begin{align*}
    f(x) &= (2x+3)(1-4x)\\
    f(x) &= 2x + 3 -8x^2 -12x\\
    f(x) &= -8x^2 -10x + 3\\
    \end{align*}$$

    On déduit $f'(x) = -16x -10$ ; évidemment, on retrouve le même résultat.

    Les deux techniques sont possibles.

??? example "2. $g(x) = (x^2-1)(x^3+x)$ sur $\mathbb R$"
    On a d'une part

    $$\begin{align*}
    g'(x) &= 2x(x^3+x) + (x^2-1)×(3x^2 + 1)\\
    g'(x) &= (2x^4+2x^2) + (3x^4-3x^2 + x^2-1)\\
    g'(x) &= 5x^4-1\\
    \end{align*}$$

    D'autre part

    $$\begin{align*}
    g(x) &= (x^2-1)(x^3+x)\\
    g(x) &= x^5+x^3 + -x^3-x\\
    g(x) &= x^5-x\\
    \end{align*}$$

    On déduit $g'(x) = 5x^4 -1$ ; évidemment, on retrouve le même résultat.

    Les deux techniques sont possibles.

### Ex 82 p 126

??? example "1. Calculer la dérivée de $f(x) = \dfrac{4}{2x-3}$ sur $\mathbb R \setminus\left\{\frac32\right\}$"

    Il suffit de mettre le $4$ en facteur et d'utiliser l'inverse de $2x-3$.

    On pose $v(x) = 2x-3$, donc $v'(x) = 2$ et on a

    $$\begin{align*}
    f'(x) &= \left(4×\dfrac{1}{v(x)}\right)'\\
    f'(x) &= 4×\dfrac{-v'(x)}{v^2(x)}\\
    f'(x) &= 4×\dfrac{-2}{(2x-3)^2}\\
    f'(x) &= \dfrac{-8}{(2x-3)^2}\\
    \end{align*}$$


??? example "2. Calculer la dérivée de $g(x) = \dfrac{2}{1-4x}$ sur $\mathbb R \setminus\left\{\frac14\right\}$"

    Il suffit de mettre le $2$ en facteur et d'utiliser l'inverse de $1-4x$.

    On pose $v(x) = 1-4x$, donc $v'(x) = -4$ et on a

    $$\begin{align*}
    f'(x) &= \left(2×\dfrac{1}{v(x)}\right)'\\
    f'(x) &= 2×\dfrac{-v'(x)}{v^2(x)}\\
    f'(x) &= 2×\dfrac{+4}{(1-4x)^2}\\
    f'(x) &= \dfrac{8}{(1-4x)^2}\\
    \end{align*}$$



### Ex 83 p 126

??? example "1. Calculer la dérivée de $f(x) = \dfrac{-2}{x^2+x+1}$ sur $\mathbb R$"

    Justifions d'abord que la fonction est bien définie et dérivable sur $\mathbb R$.

    $f$ est un quotient de fonctions dérivables sur $\mathbb R$,
    $f$ est donc dérivable sur $\mathbb R$ privé des points où le dénominateur s'annule.
    
    Le dénominateur est un polynôme du second degré dont le discriminant est $1^1-4×1×1=-3 < 0$,
    de sorte que le dénominateur ne s'annule pas ! Il n'y a donc aucun point à enlever.

    Pour dériver $f$, il suffit de mettre le $-2$ en facteur et d'utiliser l'inverse de $x^2+x+1$.

    On pose $v(x) = x^2+x+1$, donc $v'(x) = 2x+1$

    $$\begin{align*}
    f'(x) &= \left(-2×\dfrac{1}{v(x)}\right)'\\
    f'(x) &= -2×\dfrac{-v'(x)}{v^2(x)}\\
    f'(x) &= -2×\dfrac{-(2x+1)}{(x^2+x+1)^2}\\
    \end{align*}$$




??? example "2. Calculer la dérivée de $g(x) = \dfrac{3}{x^4+1}$ sur $\mathbb R$"

    Justifions d'abord que la fonction est bien définie et dérivable sur $\mathbb R$.

    $g$ est un quotient de fonctions dérivables sur $\mathbb R$,
    $g$ est donc dérivable sur $\mathbb R$ privé des points où le dénominateur s'annule.
    
    Le dénominateur est un polynôme de degré 4, avec $x^4\geqslant 0$, on a $x^4+1>0$,
    de sorte que le dénominateur ne s'annule pas !
    Il n'y a donc aucun point à enlever.

    Pour dériver $g$, il suffit de mettre le $3$ en facteur et d'utiliser l'inverse de $x^4+1$.

    On pose $v(x) = x^4+1$, donc $v'(x) = 4x^3$

    $$\begin{align*}
    g'(x) &= \left(3×\dfrac{1}{v(x)}\right)'\\
    g'(x) &= 3×\dfrac{-v'(x)}{v^2(x)}\\
    g'(x) &= 3×\dfrac{-(4x^3)}{(x^4+1)^2}\\
    g'(x) &= \dfrac{-12x^3}{(x^4+1)^2}\\
    \end{align*}$$




### 85 p 126

??? example "1. $f(x) = \dfrac{x-1}{x^2+x+1}$ sur $\mathbb R$"
    Le dénominateur est un polynôme du second degré qui a
     un discriminant strictement négatif, donc le dénominateur
     ne s'annule pas sur les réels, ainsi la fonction est
     dérivable sur $\mathbb R$.

    On pose

    $$\begin{align*}
    u(x) &= x-1 & u'(x) &= 1\\
    v(x) &= x^2+x+1 & v'(x) &= 2x+1\\
    \end{align*}$$

    On a :

    $$\begin{align*}
    f'(x) &= \left(\dfrac{u(x)}{v(x)}\right)'\\
    f'(x) &= \dfrac{u'(x)×v(x) - u(x)×v'(x)}{v^2(x)}\\
    f'(x) &= \dfrac{1×(x^2+x+1) - (x-1)×(2x+1)}{(x^2+x+1)^2}\\
    f'(x) &= \dfrac{(x^2+x+1)- (2x^2-2x+x-1)}{(x^2+x+1)^2}\\
    f'(x) &= \dfrac{-x^2 -3x + 2}{(x^2+x+1)^2}\\
    \end{align*}$$



??? example "2. $g(x) = \dfrac{x^2+x+1}{x^2+1}$ sur $\mathbb R$"
    Le dénominateur est un polynôme du second degré qui a
     un discriminant strictement négatif, donc le dénominateur
     ne s'annule pas sur les réels, ainsi la fonction est
     dérivable sur $\mathbb R$.
     
    On pose

    $$\begin{align*}
    u(x) &= x^2+x+1 & u'(x) &= 2x+1\\
    v(x) &= x^2+1 & v'(x) &= 2x\\
    \end{align*}$$

    On a :

    $$\begin{align*}
    g'(x) &= \left(\dfrac{u(x)}{v(x)}\right)'\\
    g'(x) &= \dfrac{u'(x)×v(x) - u(x)×v'(x)}{v^2(x)}\\
    g'(x) &= \dfrac{(2x+1)×(x^2+1) - (x^2+x+1)×(2x)}{(x^2+1)^2}\\
    g'(x) &= \dfrac{(2x^3+2x+x^2+1) - (2x^3+2x^2+2x)}{(x^2+1)^2}\\
    g'(x) &= \dfrac{1-x^2}{(x^2+1)^2}\\
    g'(x) &= \dfrac{(1+x)(1-x)}{(x^2+1)^2}\\
    \end{align*}$$


### 86 p 126

??? example "1. $f(x) = \dfrac{\sqrt x}{x+1}$ sur $\mathbb R_{+}^{*}$"
    On pose

    $$\begin{align*}
    u(x) &= \sqrt x & u'(x) &= \dfrac1{2\sqrt x}\\
    v(x) &= x+1 & v'(x) &= 1\\
    \end{align*}$$

    On a :

    $$\begin{align*}
    f'(x) &= \left(\dfrac{u(x)}{v(x)}\right)'\\
    f'(x) &= \dfrac{u'(x)×v(x) - u(x)×v'(x)}{v^2(x)}\\
    f'(x) &= \dfrac{\dfrac1{2\sqrt x}×(x+1) - \sqrt x×1}{(x+1)^2}\\
    f'(x) &= \dfrac{x+1 - 2\sqrt x × \sqrt x}{2\sqrt x(x+1)^2}\\
    f'(x) &= \dfrac{x+1 - 2x}{2\sqrt x(x+1)^2}\\
    f'(x) &= \dfrac{1 - x}{2\sqrt x(x+1)^2}\\
    \end{align*}$$


??? example "2. $g(x) = \dfrac{\sqrt x}{x^2+1}$ sur $\mathbb R_{+}^{*}$"
    On pose

    $$\begin{align*}
    u(x) &= \sqrt x & u'(x) &= \dfrac1{2\sqrt x}\\
    v(x) &= x^2+1 & v'(x) &= 2x\\
    \end{align*}$$

    On a :

    $$\begin{align*}
    g'(x) &= \left(\dfrac{u(x)}{v(x)}\right)'\\
    g'(x) &= \dfrac{u'(x)×v(x) - u(x)×v'(x)}{v^2(x)}\\
    g'(x) &= \dfrac{\dfrac1{2\sqrt x}×(x^2+1) - \sqrt x×2x}{(x^2+1)^2}\\
    g'(x) &= \dfrac{x^2+1 - 2\sqrt x × \sqrt x×2x}{2\sqrt x(x^2+1)^2}\\
    g'(x) &= \dfrac{x^2+1 - 4x^2}{2\sqrt x(x+1)^2}\\
    g'(x) &= \dfrac{1 - 3x^2}{2\sqrt x(x+1)^2}\\
    \end{align*}$$


    On pourrait factoriser le numérateur, de sorte à pouvoir étudier
     le signe de $g'(x)$ ce qui permet de conclure sur
     le sens de variation de $g$.






### 87 p 126 (Python)

Soit $f$ la fonction définie sur $\mathbb R$ par $f(x) = ax^2+bx+c$, avec $a, b, c$ qui sont réels et $a\neq 0$.

Donner une expression de $f'(x)$

??? success "Réponse"
    $f'(x) = 2ax + b$


Compléter le code Python

```python
a, b, c = 5, 7, 11  # par exemple

def f(x):
    "Renvoie ax² + bx + c"
    return a * x**2  +  b * x  +  c

def f_prime(x):
    "Renvoie f'(x)"
    return ...  # ← À compléter
```

??? success "Réponse"
    On a le code

    ```python
    def f_prime(x):
        "Renvoie f'(x)"
        return 2 * a * x  +  b
    ```

