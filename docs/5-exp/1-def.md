# Généralités

## Définition

On admet qu'il existe une fonction, dite **fonction exponentielle**, notée $\mathrm{exp}$, telle que :

- $\mathrm{exp}$ est définie et dérivable sur $\mathbb R$ ;
- $\mathrm{exp}(0) = 1$ ;
- $\mathrm{exp}' = \mathrm{exp}$ sur $\mathbb R$.

!!! example "Dérivées de fonctions exponentielles"
    D'après les formules sur la dérivation, on déduit rapidement que pour $x\in\mathbb R$ :

    1. $(k×\mathrm{exp}(x))' = k×\mathrm{exp}(x)$, pour $k$ une constante
    2. $(\mathrm{exp}(x+p))' = \mathrm{exp}(x+p)$, pour $p$ une constante
    3. $(\mathrm{exp}(mx+p))' = m×\mathrm{exp}(mx+p)$, pour $p$ une constante

## Premières propriétés



!!! info "$\mathrm{exp}$ ne s'annule pas sur $\mathbb R$"
    On pose $f(x) = \mathrm{exp}(x)×\mathrm{exp}(-x)$, on a pour $x\in\mathbb R$ :

    $$\begin{align*}
    f'(x) &= (\mathrm{exp}(x))'×\mathrm{exp}(-x) + \mathrm{exp}(x)×(\mathrm{exp}(-x))'\\
    f'(x) &= \mathrm{exp}(x)×\mathrm{exp}(-x) + \mathrm{exp}(x)×(-1)×\mathrm{exp}(-x)\\
    f'(x) &= 0\\
    \end{align*}
    $$

    On en déduit que $f$ est constante sur $\mathbb R$.

    La valeur de cette constante est $f(0) = \mathrm{exp}(0)×\mathrm{exp}(-0) = 1×1 = 1$.

    On déduit que pour $x\in\mathbb R$ :

    $$\mathrm{exp}(x)×\mathrm{exp}(-x) = 1$$

    D'où 

    $$\mathrm{exp}(-x) = \frac1{\mathrm{exp}(x)}$$

    et on déduit le fait que $\mathrm{exp}(x)$ ne s'annule pas sur $\mathbb R$.

    On peut même ajouter que $\mathrm{exp}(x)$ reste du signe de $\mathrm{exp}(0) = +1$, ainsi $\mathrm{exp}$ est strictement positive sur $\mathbb R$.


??? info "Une telle fonction $\mathrm{exp}$ est unique"
    Supposons qu'il existe une autre fonction $g$ telle que

    - $g$ est définie et dérivable sur $\mathbb R$ ;
    - $g(0) = 1$ ;
    - $g' = g$ sur $\mathbb R$.

    On définit alors $f(x) = \dfrac{g(x)}{\mathrm{exp}(x)}$ sur $\mathbb R$, également dérivable sur $\mathbb R$ avec :

    $f'(x) = \dfrac{g'(x)×\mathrm{exp}(x) - g(x)×\mathrm{exp}'(x)}{\mathrm{exp}^2(x)} = 0$

    $f$ est ainsi une fonction constante sur $\mathbb R$, égale à $f(0) = \dfrac{g(0)}{\mathrm{exp}(0)} = 1$

    On déduit enfin que $g(x) = \mathrm{exp}(x)$ sur $\mathbb R$, ce qui prouve l'unicité de $\mathrm{exp}$ d'après sa définition.

## Relation fonctionnelle

!!! quote "Théorème"
    Pour tous nombres réels $x$ et $y$, on a :

    $$\mathrm{exp}(x+y) = \mathrm{exp}(x) × \mathrm{exp}(y)$$

!!! note "Démonstration"
    On fixe un nombre réel $y$, on définit ensuite la fonction $f$ sur $\mathbb R$ avec $f(x) = \dfrac{\mathrm{exp}(x+y)}{\mathrm{exp}(x)}$.

    $f$ est dérivable sur $\mathbb R$, comme quotient de fonctions dérivables sur $\mathbb R$, dont le dénominateur ne s'annule pas sur $\mathbb R$. On a :

    - $(\mathrm{exp}(x+y))' = \mathrm{exp}(x+y)$
    - $(\mathrm{exp}(x))' = \mathrm{exp}(x)$
    
    
    D'où $f'(x) = \dfrac{\mathrm{exp}(x+y)×\mathrm{exp}(x) - \mathrm{exp}(x+y)×\mathrm{exp}(x)}{\mathrm{exp}^2(x)} = 0$

    On déduit que $f$ est constante sur $\mathbb R$, égale à $f(0) = \dfrac{\mathrm{exp}(0+y)}{\mathrm{exp}(0)} = \mathrm{exp}(y)$.

    On conclut que $\dfrac{\mathrm{exp}(x+y)}{\mathrm{exp}(x)} = \mathrm{exp}(y)$, d'où le théorème.


!!! quote "Propriété"
    On a déjà vu que pour tout $x\in \mathbb R$, on a

    $$\mathrm{exp}(-x) = \dfrac1{\mathrm{exp}(x)}$$

!!! note "Nouvelle démonstration"
    Pour $x\in \mathbb R$, on a $\mathrm{exp}(x) × \mathrm{exp}(-x) = \mathrm{exp}(x+(-x)) = \mathrm{exp}(0) = 1$

!!! quote "Propriété"
    Pour tous nombres réels $x$ et $y$, on a :

    $$\mathrm{exp}(x-y) = \dfrac{\mathrm{exp}(x)}{\mathrm{exp}(y)}$$

!!! note "Démonstration"

    $$\begin{align*}
    \mathrm{exp}(x-y) &= \mathrm{exp}(x+(-y))\\
    \mathrm{exp}(x-y) &= \mathrm{exp}(x)×\mathrm{exp}(-y)\\
    \mathrm{exp}(x-y) &= \mathrm{exp}(x)×\dfrac1{\mathrm{exp}(y)}\\
    \mathrm{exp}(x-y) &= \dfrac{\mathrm{exp}(x)}{\mathrm{exp}(y)}\\
    \end{align*}
    $$

!!! quote "Propriétés"
    Pour $x\in\mathbb R$ et $n\in\mathbb Z$, on a

    $$\begin{align*}
    \mathrm{exp}(0×x) &= 1 = [\mathrm{exp}(x)]^0\\
    \mathrm{exp}(1×x) &= [\mathrm{exp}(x)]^1\\
    \mathrm{exp}(x+x) &= \mathrm{exp}(x)×\mathrm{exp}(x) = [\mathrm{exp}(x)]^2\\
    \mathrm{exp}(x+x+x) &= \mathrm{exp}(x)×\mathrm{exp}(x+x) = \mathrm{exp}(x)×[\mathrm{exp}(x)]^2= [\mathrm{exp}(x)]^3\\
    \cdots &= \cdots\\
    \mathrm{exp}(nx) &= [\mathrm{exp}(x)]^n\\
    \mathrm{exp}((n+1)x) &= \mathrm{exp}(x + nx) = \mathrm{exp}(x)×\mathrm{exp}(nx) = \mathrm{exp}(x)×[\mathrm{exp}(x)]^n = [\mathrm{exp}(x)]^{n+1}\\
    \end{align*}
    $$

!!! note "Démonstration"
    Ce résultat est admis ; il utilise le principe de récurrence :

    - On prouve le résultat pour des cas de base,
    - on démontre ensuite que la validité du théorème pour un certain $n$ entraine la validité pour $n+1$. - Ainsi, de proche en proche, la validité est établie pour tout $n\in\mathbb N$.

## Le nombre e

!!! info "Définition"
    On pose $\mathrm e = \mathrm{exp}(1)$

    $\mathrm e$ est un nombre réel, et on donnera une valeur approchée bientôt, en la justifiant.
    
!!! quote "Propriété"
    D'après la propriété précédente, on a pour tout $n\in\mathbb Z$ :

    $$\mathrm{exp}(n) = \mathrm{exp}(n×1) = [\mathrm{exp}(1)]^n = \mathrm e^n$$

!!! info "Nouvelle notation"
    Par extension, on note pour tout $x\in\mathbb R$

    $$\mathrm{exp}(x) = \mathrm e^x$$

    On retrouve avec cette notation les formules similaires sur les puissances, pour tous $x$ et $y$ de $\mathbb R$ et $n\in\mathbb Z$ :

    - $\mathrm e^{-x} = \dfrac1{\mathrm e^x}$
    - $\mathrm e^{x+y} = \mathrm e^x × \mathrm e^y$
    - $\mathrm e^{x-y} = \dfrac{\mathrm e^x}{\mathrm e^y}$
    - $\mathrm e^{nx} = \left(\mathrm e^x\right)^n$


!!! question "Utilisation : réduction"
    Réduire avec une seule exponentielle les expressions suivantes

    ??? success "$A = \mathrm e^5 × \mathrm e^8 × \left(\mathrm e^2\right)^3$"

        $$\begin{align*}
        A &= \mathrm e^5 × \mathrm e^8 × \left(\mathrm e^2\right)^3\\
        A &= \mathrm e^{5+8+2×3}\\
        A &= \mathrm e^{19}\\
        \end{align*}
        $$

    
    ??? success "$B = \mathrm e^5 × (\mathrm e^5)^{-1} × \mathrm e^{10}$"

        $$\begin{align*}
        A &= \mathrm e^5 × (\mathrm e^5)^{-1} × \mathrm e^{10}\\
        A &= \mathrm e^{5-5+10}\\
        A &= \mathrm e^{10}\\
        \end{align*}
        $$


Faire les exercices 27 à 34 p 171

---

## Repères historiques et contemporains

!!! inline end abstract "Leonhard Euler"
    ![](./Leonhard_Euler.jpg)

    1707 Bâle (Suisse) - 1783 Saint-Pétersbourg (Russie)


Le nombre $\mathrm e$ est défini à la fin du XVIIe siècle, dans une correspondance entre Leibniz et Christian Huygens.

- Euler démontre en 1737 que $\mathrm e$ est irrationnel et en donne une première approximation avec 23 décimales.

- En 1873, Charles Hermite montre que le nombre $\mathrm e$ est même transcendant, c'est-à-dire qu'il n'est racine d'aucun polynôme non nul à coefficients entiers.

- Le célèbre informaticien Donald Knuth a numéroté les différentes versions de son logiciel Metafont d'après le décimales de $\mathrm e$ : `2`, `2.7`, `2.71`, `2.718` etc.

- En 2004, pour son introduction en bourse, Google a levé $\mathrm e$ milliards de dollars (au dollar près). Et pour une campagne de recrutement, une première énigme à résoudre était de trouver le premier nombre premier à dix chiffres dans les décimales de $\mathrm e$. La réponse étant $7~427~466~391$, qui commence à la 99e décimale.



## Compléments

!!! warning "Attention"
    Cette section est **partiellement hors programme**, la lecture est réservée aux élèves curieux.

### Méthode d'Euler

On utilise la formule de l'approximation d'une fonction dérivable par sa fonction tangente, au voisinage du point considéré. Si $f$ est dérivable en $a$, pour $x$ proche de $a$, on a $f(x) \approx f(a) + f'(a)×(x-a)$.

On part de $\mathrm e^{0+\frac1n} \approx \mathrm e^0 + 1×(\frac1n - 0) = 1 + \frac1n$

On déduit $\mathrm e^{\frac1n+\frac1n} \approx 1+\frac1n + (1+\frac1n)×(\frac2n - \frac1n) = (1 + \frac1n)^2$

On peut poursuivre en montrant que $\mathrm e^{\frac i n} \approx (1 + \frac1n)^i$ pour tout $i$ de $0$ à $n$.

On montre enfin que

$$\mathrm e = \lim_{n→+\infty}\left(1+\frac 1 n\right)^n$$

Avec un script Python, on peut obtenir une valeur approchée

```python
for i in range(60):
    n = 2**i
    e = (1 + 1 / n) ** n
    print(n, e)
```


```output
1 2.0
2 2.25
4 2.44140625
8 2.565784513950348
16 2.6379284973666
32 2.676990129378183
64 2.697344952565099
128 2.7077390196880207
256 2.7129916242534344
512 2.7156320001689913
1024 2.7169557294664357
2048 2.7176184823368796
4096 2.7179500811896657
8192 2.718115936265797
16384 2.7181988777219708
32768 2.718240351930294
65536 2.7182610899046034
131072 2.7182714591093062
262144 2.718276643766046
524288 2.718279236108013
1048576 2.718280532282396
2097152 2.7182811803704374
4194304 2.7182815044146706
8388608 2.71828166643684
16777216 2.7182817474479384
33554432 2.7182817879534906
67108864 2.718281808206268
134217728 2.7182818183326565
268435456 2.718281823395851
536870912 2.718281825927448
1073741824 2.7182818271932465
2147483648 2.718281827826146
4294967296 2.7182818281425956
8589934592 2.7182818283008205
17179869184 2.718281828379933
34359738368 2.718281828419489
68719476736 2.718281828439267
137438953472 2.718281828449156
274877906944 2.7182818284541006
549755813888 2.718281828456573
1099511627776 2.718281828457809
2199023255552 2.7182818284584274
4398046511104 2.718281828458736
8796093022208 2.7182818284588905
17592186044416 2.718281828458968
35184372088832 2.7182818284590065
70368744177664 2.718281828459026
140737488355328 2.7182818284590358
281474976710656 2.71828182845904
562949953421312 2.718281828459043
1125899906842624 2.718281828459044
2251799813685248 2.7182818284590446
4503599627370496 2.718281828459045
9007199254740992 1.0
18014398509481984 1.0
36028797018963968 1.0
72057594037927936 1.0
144115188075855872 1.0
288230376151711744 1.0
576460752303423488 1.0
```

On constate que pour $n$ trop grand, le calcul de $1+\frac1n$ conduit au résultat approché $1.0$, ensuite, mis à la puissance $n$, le résultat final est $1.0$ qui est faux. Juste avant, on obtient une bonne précision :

$$\mathrm e\approx 2.718281828459\cdots$$

!!! info "$\mathrm e$ est irrationnel"
    Vous avez déjà rencontré des nombres irrationnels

    - $\sqrt 2$ dont la preuve de l'irrationalité est accessible au collège
    - $\pi$ dont la preuve d'irrationalité est complexe...

    La preuve d'irrationalité de $\mathrm e$ est accessible en classe de Terminale. Pour les impatients, [voici une preuve détaillée](https://ens-fr.gitlab.io/mathinfo/5-nombres/2-e_irrationnel/).


### Autre formule pour e

Pour $x$ proche de $0$, on a $\mathrm e^x \approx 1 + x$

On pose 

- $P_0(x) = 1$
- $P_1(x) = 1 + x$, on a $P_1'(x) = P_0(x)$ et $P_1(0) = 1$.

Or, on sait que $(\mathrm e^x)' = \mathrm e^x$, avec $\mathrm e^0 = 1$. Cherchons donc un polynôme $P_2(x)$ de degré 2 tel que $P_2'(x) = P_1(x)$ et $P_2(0) = 1$. On trouve

$$P_2(x) = 1 + x + \frac {x^2}2$$

Sur le même principe, on trouve

$$P_3(x) = 1 + x + \frac {x^2}2 + \frac {x^3}6$$

$$P_4(x) = 1 + x + \frac {x^2}2 + \frac {x^3}6+ \frac {x^4}{24}$$

De manière générale

$$P_n(x) = 1 + x + \frac {x^2}2 + \frac {x^3}6+ \frac {x^4}{24} + \cdots + \frac {x^n}{1×2×3×4×\cdots×n}$$

Ce qui conduit à une nouvelle formule pour $\mathrm e$, en remplaçant $x$ par $1$ :

$$\mathrm e = \lim_{n→+\infty} \left(1 + 1 + \frac 1 2 + \frac 1 6+ \frac 1{24} + \cdots + \frac 1{1×2×3×4×\cdots×n}\right)$$

Avec un script Python, on obtient très rapidement une valeur très précise

```python
somme = 2
denominateur = 1
for n in range(2, 20):
    denominateur = denominateur * n
    somme = somme + 1 / denominateur
    print(n, somme)
```

```output
2 2.5
3 2.6666666666666665
4 2.708333333333333
5 2.7166666666666663
6 2.7180555555555554
7 2.7182539682539684
8 2.71827876984127
9 2.7182815255731922
10 2.7182818011463845
11 2.718281826198493
12 2.7182818282861687
13 2.7182818284467594
14 2.71828182845823
15 2.718281828458995
16 2.718281828459043
17 2.7182818284590455
18 2.7182818284590455
19 2.7182818284590455
```

On constate qu'on obtient très rapidement une excellente approximation de $\mathrm e$.


### Ailleurs en mathématiques

$$\mathrm e = 1 + \frac 1 1 + \frac 1 {1×2} + \frac 1 {1×2×3} + \frac 1 {1×2×3×4} + \cdots = \sum_{n=0}^{+\infty}\frac1{n!}$$

- On le trouve parfois définit comme $\mathrm e = \lim_{n→+\infty}\left(1+\dfrac1n\right)^n$

- On le trouve dans l'identité d'Euler $\mathrm e^{\mathrm i\pi} + 1 = 0$

- On le retrouve dans la formule de Stirling $n!\sim \sqrt{2\pi n}\left(\dfrac n e\right)^n$
