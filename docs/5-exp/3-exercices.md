# Exercices résolus

## Applications directes

### 27 : Simplifier les expressions

??? question "$A = \frac{\mathrm e^6}{\mathrm e^2}$"
    $$
    \begin{align*}
    A &= \frac{\mathrm e^6}{\mathrm e^2}\\
    A &= \mathrm e^{6-2}\\
    &\boxed{A = \mathrm e^4}\\
    \end{align*}
    $$


??? question "$B = \frac{(\mathrm e^3)^2}{\mathrm e^{-2}}$"
    $$
    \begin{align*}
    B &= \frac{(\mathrm e^3)^2}{\mathrm e^{-2}}\\
    B &= \mathrm e^{3×2-(-2)}\\
    &\boxed{B = \mathrm e^{8}}\\
    \end{align*}
    $$


??? question "$C = \frac{\mathrm e^{-2}×(\mathrm e^{3})^2}{\mathrm e^{2}}$"
    $$
    \begin{align*}
    C &= \frac{\mathrm e^{-2}×(\mathrm e^{3})^2}{\mathrm e^{2}}\\
    C &= \mathrm e^{-2+3×2-2}\\
    C &= \mathrm e^{-2+6-2}\\
    &\boxed{C = \mathrm e^{2}}\\
    \end{align*}
    $$


### 28 : Compléter les pointillés

??? question "$\mathrm e^{\cdots} × \mathrm e^{7} × \mathrm e^{-2} = \mathrm e^{3}$"
    $\mathrm e^{x} × \mathrm e^{7} × \mathrm e^{-2} = \mathrm e^{3}$ est successivement équivalent à

    - $\mathrm e^{x+7-2} = \mathrm e^{3}$
    - $x+7-2 = 3$
    - $x = 3+2-7$
    - $\boxed{x = -2}$

??? question "$(\mathrm e^3)^4×\mathrm e^{\cdots} = \mathrm e^{3} × \mathrm e^{-1}$"
    $(\mathrm e^3)^4×\mathrm e^x = \mathrm e^{3} × \mathrm e^{-1}$ est successivement équivalent à

    - $\mathrm e^{3×4+x} = \mathrm e^{3+(-1)}$
    - $12+x = 2$
    - $x = 2-12$
    - $\boxed{x = -10}$


??? question "$\frac{\mathrm e^{\cdots}}{\mathrm e^{3}} = \mathrm e^{-1}$"
    $\frac{\mathrm e^{x}}{\mathrm e^{3}} = \mathrm e^{-1}$ est successivement équivalent à

    - $\mathrm e^{x-3} = \mathrm e^{-1}$
    - $x-3 = -1$
    - $x = -1+3$
    - $\boxed{x = 2}$


??? question "$\frac{\mathrm e}{\mathrm e^{\cdots}} = \frac{\mathrm e^2}{\mathrm e^5}$"
    $\frac{\mathrm e^1}{\mathrm e^{x}} = \frac{\mathrm e^2}{\mathrm e^5}$ est successivement équivalent à

    - $\mathrm e^{1-x} = \mathrm e^{2-5}$
    - $-x = -3-1$
    - $x = -(-4)$
    - $\boxed{x = 4}$


### 29, 30 : Simplifier les expressions

> $x$ est un nombre réel.

??? question "$A = \mathrm e^{-2x+1} × \mathrm e^{x+3}$"
    $$
    \begin{align*}
    A &= \mathrm e^{-2x+1} × \mathrm e^{x+3}\\
    A &= \mathrm e^{(-2x+1)+(x+3)}\\
    A &= \mathrm e^{-2x+1+x+3}\\
    &\boxed{A = \mathrm e^{-x+4}}\\
    \end{align*}
    $$

??? question "$B = \mathrm e^{x+4} × (\mathrm e^{x})^2 × \mathrm e^{-2x}$"
    $$
    \begin{align*}
    B &= \mathrm e^{x+4} × (\mathrm e^{x})^2 × \mathrm e^{-2x}\\
    B &= \mathrm e^{(x+4) + 2x + (-2x)}\\
    &\boxed{B = \mathrm e^{x+4}}\\
    \end{align*}
    $$

??? question "$C = \mathrm e^{x} × \mathrm e$"
    $$
    \begin{align*}
    C &= \mathrm e^{x} × \mathrm e\\
    C &= \mathrm e^{x} × \mathrm e^1\\
    &\boxed{C = \mathrm e^{x+1}}\\
    \end{align*}
    $$

??? question "$D = \mathrm e^{x} × x\mathrm e^x$"
    $$
    \begin{align*}
    D &= \mathrm e^{x} × x\mathrm e^x\\
    D &= x\mathrm e^{x+x}\\
    &\boxed{D = x\mathrm e^{2x}}\\
    \end{align*}
    $$


??? question "$E = \frac{\mathrm e^{x} × (\mathrm e^{x})^2}{\mathrm e^{2x}}$"
    $$
    \begin{align*}
    E &= \frac{\mathrm e^{x} × (\mathrm e^{x})^2}{\mathrm e^{2x}}\\
    E &= \frac{\mathrm e^{x} × \mathrm e^{2x}}{\mathrm e^{2x}}\\
    &\boxed{E = \mathrm e^{x}}\\
    \end{align*}
    $$


??? question "$F = \frac{\mathrm e^{x+4}}{\mathrm e^{4x}}$"
    $$
    \begin{align*}
    F &= \frac{\mathrm e^{x+4}}{\mathrm e^{4x}}\\
    F &= \mathrm e^{(x+4)-4x}\\
    &\boxed{F = \mathrm e^{-3x+4}}\\
    \end{align*}
    $$


??? question "$G = \frac1{\mathrm e^{3-2x}}$"
    $$
    \begin{align*}
    G &= \frac1{\mathrm e^{3-2x}}\\
    G &= \mathrm e^{-(3-2x)}\\
    &\boxed{G = \mathrm e^{-3+2x}}\\
    \end{align*}
    $$



### 31, 32 : Déterminer le signe des fonctions

Pour $x\in\mathbb R$

??? question "**31.1** $f(x) = 3\mathrm e^x$"
    Pour $x\in\mathbb R$, $\mathrm e^x>0$, donc $f(x)>0$.


??? question "**31.2** $g(x) = 2\mathrm e^{-5x}$"
    Pour $x\in\mathbb R$, $\mathrm e^{-5x}>0$, donc $g(x)>0$.

??? question "**31.3** $h(x) = -\sqrt{2}\mathrm e^{-3x}$"
    Pour $x\in\mathbb R$, $\mathrm e^{-3x}>0$, donc $h(x)<0$.



??? question "**32.1.** $f(x) = \frac{1+\mathrm e^{4x}}{x^2+2}$"
    Pour $x\in\mathbb R$,
    
    - $1+\mathrm e^{4x}>1$
    - $x^2+2 \geqslant 2$,
    - donc $f(x)>0$.

??? question "**32.2.** $g(x) = \frac{-9}{-2-\mathrm e^{-8x}}$"
    Pour $x\in\mathbb R$,
    
    - $-9<0$
    - $-2-\mathrm e^{-8x}<-2<0$,
    - donc $g(x)>0$.

### 33, 34 : Dériver les fonctions

??? question "**33.1.** $f(x) = 3\mathrm e^{x} -5x^2 +2$"
    $f'(x) = 3\mathrm e^{x} -5×2x +0$
    
    $\boxed{f'(x) = 3\mathrm e^{x} -10x}$


??? question "**33.2.** $f(x) = x-4\mathrm e^{x} +1$"
    $f'(x) = 1-4\mathrm e^{x} +0$

    $\boxed{f'(x) = 1-4\mathrm e^{x}}$

??? question "**33.3.** $f(x) = \mathrm e^{x} +\mathrm e^3$"
    $f'(x) = \mathrm e^{x} +0$

    $\boxed{f'(x) = \mathrm e^{x}}$

    !!! info "Bonus"
        $f'(x)$ est strictement positive sur $\mathbb R$, donc $f$ est strictement croissante sur $\mathbb R$.

??? question "**33.4.** $f(x) = x\mathrm e^{x}$"
    $f'(x) = 1×\mathrm e^{x} +x\mathrm e^{x}$

    $\boxed{f'(x) = (1+x)\mathrm e^{x}}$

    !!! info "Bonus"
        $f'(x)$ est strictement positive sur $]-1\,;\,+\infty[$, donc $f$ est strictement croissante sur $]-1\,;\,+\infty[$.
    
        $f'(x)$ est strictement négative sur $]+\infty\,;\,-1[$, donc $f$ est strictement décroissante sur $]+\infty\,;\,-1[$.
    

Donner également le sens de variation de $f$

??? question "**34.1.** $f(x) = \mathrm e^{3x}$"
    $f'(x) = 3×\mathrm e^{3x}$

    $\boxed{f'(x) = 3\mathrm e^{3x}}$

    $f'(x)$ est strictement positive sur $\mathbb R$, donc $f$ est strictement croissante sur $\mathbb R$.

??? question "**34.2.** $f(x) = \mathrm e^{-2x}$"
    $f'(x) = -2×\mathrm e^{-2x}$

    $\boxed{f'(x) = -2\mathrm e^{-2x}}$

    $f'(x)$ est strictement négative sur $\mathbb R$, donc $f$ est strictement décroissante sur $\mathbb R$.

??? question "**34.3.** $f(x) = \mathrm e^{-x+4}$"
    $f'(x) = -1×\mathrm e^{-x+4}$

    $\boxed{f'(x) = -\mathrm e^{-x+4}}$

    $f'(x)$ est strictement négative sur $\mathbb R$, donc $f$ est strictement décroissante sur $\mathbb R$.

??? question "**34.4.** $f(x) = 5\mathrm e^{x+6}$"
    $f'(x) = 5×1×\mathrm e^{x+6}$

    $\boxed{f'(x) = 5\mathrm e^{x+6}}$

    $f'(x)$ est strictement positive sur $\mathbb R$, donc $f$ est strictement croissante sur $\mathbb R$.

### 35, 36, 37 : Résoudre sur les réels

??? question "**35.1.** $\mathrm e^{x} = \mathrm e^{-2}$"
    $\mathrm e^{x} = \mathrm e^{-2}$ est équivalent à

    $$\boxed{x = -2}$$

??? question "**35.2.** $\mathrm e^{x} = \mathrm e$"
    $\mathrm e^{x} = \mathrm e$ est équivalent à

    $\mathrm e^{x} = \mathrm e^1$, puis à

    $$\boxed{x = 1}$$

??? question "**35.3.** $\mathrm e^{x+2} = \mathrm e^3$"
    $\mathrm e^{x+2} = \mathrm e^3$ est équivalent à

    $x+2 = 3$, puis à

    $$\boxed{x = 1}$$

??? question "**35.4.** $\mathrm e^{2x+1} = \mathrm e$"
    $\mathrm e^{2x+1} = \mathrm e$ est équivalent à

    $2x+1 = 1$, puis à

    $$\boxed{x = 0}$$

??? question "**35.5.** $\mathrm e^{x} = 1$"
    $\mathrm e^x = 1$ est équivalent à

    $\mathrm e^x = \mathrm e^0$, puis à

    $$\boxed{x = 0}$$

??? question "**35.6.** $\mathrm e^{x} + 4= 0$"
    $\mathrm e^x + 4 = 0$ n'a aucune solution réelle, en effet $\mathrm e^x + 4>4$

    $$\boxed{x \in \emptyset}$$

??? question "**35.7.** $\mathrm e^{x^2} = \mathrm e$"
    $\mathrm e^{x^2} = \mathrm e^1$ est équivalent à

    $x^2 = 1$, puis à

    $$\boxed{x \in \{-1, +1\}}$$

??? question "**35.8.** $\mathrm e^{x^2+1} = 1$"
    $\mathrm e^{x^2+1} = \mathrm e^0$ est équivalent à

    $x^2 +1= 0$, qui n'a pas de solution réelle, ainsi

    $$\boxed{x \in \emptyset}$$


??? question "**36.1.** $\mathrm e^{-x} = 1$"
    $\mathrm e^{-x} = 1$ est successivement équivalente à
    
    $$
    \begin{align*}
    \mathrm e^{-x} &= \mathrm e^{0}\\
    -x &= 0\\
    &\boxed{x = 0}\\
    \end{align*}
    $$

??? question "**36.2.** $\mathrm e^{2x-3} = \mathrm e$"
    $\mathrm e^{2x-3} = \mathrm e^1$ est successivement équivalente à
    
    $$
    \begin{align*}
    2x-3 &= 1\\
    2x &= 1+3\\
    2x &= 4\\
    x &= 4/2\\
    &\boxed{x = 2}\\
    \end{align*}
    $$

??? question "**36.3.** $5\mathrm e^{3x+1} = 5$"
    $5\mathrm e^{3x+1} = 5$ est successivement équivalente à
    
    $$
    \begin{align*}
    \mathrm e^{3x+1} &= 1\\
    \mathrm e^{3x+1} &= \mathrm e^0\\
    3x+1 &= 0\\
    3x &= -1\\
    &\boxed{x = \frac{-1}3}\\
    \end{align*}
    $$

??? question "**36.4.** $-2\mathrm e^{x^2} = 3$"
    $\mathrm e^{x^2}>0$, donc $-2\mathrm e^{x^2}<0$ et ainsi
    
    $-2\mathrm e^{x^2}$ ne peut pas être égal à $3$ pour $x$ réel.
    
    $$\boxed{x \in \emptyset}$$


??? question "**37.1.** $\mathrm e^{2x} > \mathrm e^{-2}$"
    $\mathrm e^{2x} > \mathrm e^{-2}$ est successivement équivalent à
    
    $$
    \begin{align*}
    2x &> -2\\
    &\boxed{x > -1}\\
    \end{align*}
    $$

??? question "**37.2.** $\mathrm e^{-3x} < \mathrm e$"
    $\mathrm e^{-3x} < \mathrm e^1$ est successivement équivalent à
    
    $$
    \begin{align*}
    -3x &< 1\\
    &\boxed{x > \frac{-1}3}\\
    \end{align*}
    $$

??? question "**37.3.** $\mathrm e^{3x-5} \geqslant \mathrm e^{-3}$"
    $\mathrm e^{3x-5} \geqslant \mathrm e^{-3}$ est successivement équivalent à
    
    $$
    \begin{align*}
    3x-5 & \geqslant -3\\
    3x & \geqslant -3+5\\
    3x & \geqslant 2\\
    & \boxed{x \geqslant \frac{2}3}\\
    \end{align*}
    $$

??? question "**37.4.** $\mathrm e^{-2x-1} \leqslant 1$"
    $\mathrm e^{-2x-1} \leqslant \mathrm e^{0}$ est successivement équivalent à
    
    $$
    \begin{align*}
    -2x-1 & \leqslant 0\\
    -2x & \leqslant +1\\
    & \boxed{x \geqslant \frac{1}2}\\
    \end{align*}
    $$

## Généralités

### 49 à 54 : Simplifier au maximum

??? question "**51.1.** $a(x) = \mathrm e^{2x}×(\mathrm e^x)^2×\mathrm e^{-3x}$"
    $$
    \begin{align*}
    a(x) &= \mathrm e^{2x}×(\mathrm e^x)^2×\mathrm e^{-3x}\\
    a(x) &= \mathrm e^{2x+x×2+(-3x)}\\
    & \boxed{a(x) = \mathrm e^{x}}\\
    \end{align*}
    $$

??? question "**51.2.** $b(x) = \frac{\mathrm e^{x^2}}{\mathrm e^x}$"
    $$
    \begin{align*}
    b(x) &= \frac{\mathrm e^{x^2}}{\mathrm e^x}\\
    b(x) &= \mathrm e^{x^2-x}\\
    & \boxed{b(x) = \mathrm e^{x(x-1)}}\\
    \end{align*}
    $$

??? question "**51.3.** $c(x) = \frac{\mathrm e^{x-1}×\mathrm e^{4x}}{\mathrm e^x}$"
    $$
    \begin{align*}
    c(x) &= \frac{\mathrm e^{x-1}×\mathrm e^{4x}}{\mathrm e^x}\\
    c(x) &= \mathrm e^{(x-1)+(4x)-(x)}\\
    & \boxed{c(x) = \mathrm e^{4x-1}}\\
    \end{align*}
    $$

??? question "**51.4.** $d(x) = \frac{\mathrm e^{-2x}}{\mathrm e^{-3x}×\mathrm e^{x+1}}$"
    $$
    \begin{align*}
    d(x) &= \frac{\mathrm e^{-2x}}{\mathrm e^{-3x}×\mathrm e^{x+1}}\\
    d(x) &= \mathrm e^{(-2x)-((-3x)+(x+1))}\\
    d(x) &= \mathrm e^{-2x+3x-x-1}\\
    & \boxed{d(x) = \mathrm e^{-1}}\\
    \end{align*}
    $$


### 56 à 60 : Développer et réduire

!!! info "Rappel : identités remarquables"
    Pour $k, a, b, c, d\in\mathbb R$, on a :

    $$
    \begin{align*}
    k(a+b) &= ka && + & kb\\
    k(a-b) &= ka && - & kb\\
    \end{align*}
    $$

    et

    $$
    (a+b)(c+d) = ac + ad + bc + bd\\
    $$

    et

    $$
    \begin{align*}
    (a+b)^2    &= a^2 &&+&2ab && + && b^2\\
    (a-b)^2    &= a^2 &&-&2ab && + && b^2\\
    (a+b)(a-b) &= a^2  && &    && -&& b^2\\
    \end{align*}
    $$


??? question "**56.1.** $A = \mathrm e^4(\mathrm e^3+\mathrm e^7)$"
    $$
    \begin{align*}
    A &= \mathrm e^4(\mathrm e^3+\mathrm e^7)\\
    A &= \mathrm e^4×\mathrm e^3+\mathrm e^4×\mathrm e^7\\
    A &= \mathrm e^{4+3} + \mathrm e^{4+7}\\
    & \boxed{A = \mathrm e^{7} + \mathrm e^{11}}\\
    \end{align*}
    $$

??? question "**56.2.** $B = (\mathrm e^{2} + \mathrm e^{6})(\mathrm e^{3} + \mathrm e)$"
    $$
    \begin{align*}
    B &= (\mathrm e^{2} + \mathrm e^{6})(\mathrm e^{3} + \mathrm e^1)\\
    B &= \mathrm e^{2}×\mathrm e^{3}+\mathrm e^{2}×\mathrm e^{1}+\mathrm e^{6}×\mathrm e^{3}+\mathrm e^{6}×\mathrm e^{1}\\
    B &= \mathrm e^{2+3}+\mathrm e^{2+1}+\mathrm e^{6+3}+\mathrm e^{6+1}\\
    & \boxed{B = \mathrm e^{5}+\mathrm e^{3}+\mathrm e^{9}+\mathrm e^{7}}\\
    \end{align*}
    $$

??? question "**56.3.** $C = (\mathrm e^{8} - \mathrm e^{2})(\mathrm e^{6} + 1)$"
    $$
    \begin{align*}
    C &= (\mathrm e^{8} - \mathrm e^{2})(\mathrm e^{6} + 1)\\
    C &= \mathrm e^{8}×\mathrm e^{6}+\mathrm e^{8}×1-\mathrm e^{2}×\mathrm e^{6}-\mathrm e^{2}×1\\
    C &= \mathrm e^{8+6}+\mathrm e^{8}-\mathrm e^{2+6}-\mathrm e^{2}\\
    & \boxed{C = \mathrm e^{14}-\mathrm e^{2}}\\
    \end{align*}
    $$

??? question "**56.4.** $D = (\mathrm e^{-2} + \mathrm e^{3})(\mathrm e^{-2} - \mathrm e^{8})$"
    $$
    \begin{align*}
    D &= (\mathrm e^{-2} + \mathrm e^{3})(\mathrm e^{-2} - \mathrm e^{8})\\
    D &= \mathrm e^{-2}×\mathrm e^{-2} -\mathrm e^{-2}×\mathrm e^{8} +\mathrm e^{3}×\mathrm e^{-2} -\mathrm e^{3}×\mathrm e^{8}\\
    D &= \mathrm e^{-2-2}-\mathrm e^{-2+8}+\mathrm e^{3-2}-\mathrm e^{3+8}\\
    & \boxed{D = \mathrm e^{-4}-\mathrm e^{6}+\mathrm e -\mathrm e^{11}}\\
    \end{align*}
    $$


??? question "**57.1.** $A = (\mathrm e^3+\mathrm e^5)^2$"
    $$
    \begin{align*}
    A &= (\mathrm e^3+\mathrm e^5)^2\\
    A &= (\mathrm e^3)^2+ 2\mathrm e^3×\mathrm e^5+ (\mathrm e^5)^2\\
    & \boxed{A = \mathrm e^{6} + 2\mathrm e^8 + \mathrm e^{10}}\\
    \end{align*}
    $$

??? question "**57.2.** $B = (\mathrm e^{+2} - \mathrm e^{-2})^2$"
    $$
    \begin{align*}
    B &= (\mathrm e^{+2} - \mathrm e^{-2})^2\\
    B &= (\mathrm e^{+2})^2 -2×\mathrm e^{+2}×\mathrm e^{-2}+ (\mathrm e^{-2})^2\\
    B &= \mathrm e^{+4} -2×\mathrm e^{0} + \mathrm e^{-4}\\
    & \boxed{B = \mathrm e^{+4} -2+ \mathrm e^{-4}}\\
    \end{align*}
    $$

??? question "**57.3.** $C = (\mathrm e^{+6} - \mathrm e^{-4})(\mathrm e^{+6} + \mathrm e^{-4})$"
    $$
    \begin{align*}
    C &= (\mathrm e^{+6} - \mathrm e^{-4})(\mathrm e^{+6} + \mathrm e^{-4})\\
    C &= (\mathrm e^{+6})^2 - (\mathrm e^{-4})^2\\
    C &= \mathrm e^{+6×2} -\mathrm e^{-4×2}\\
    & \boxed{C = \mathrm e^{+12} - \mathrm e^{-8}}\\
    \end{align*}
    $$

??? question "**57.4.** $D = (2\mathrm e^{4} - 3\mathrm e^{-1})^2$"
    $$
    \begin{align*}
    D &= (2\mathrm e^{4} - 3\mathrm e^{-1})^2\\
    D &= (2\mathrm e^{4})^2 -2×2\mathrm e^{4}×3\mathrm e^{-1}+ (3\mathrm e^{-1})^2\\
    D &= 2^2\mathrm e^{4×2} -2×2×3×\mathrm e^{4-1} + 3^2\mathrm e^{-1×2}\\
    & \boxed{D = 4\mathrm e^{8} -12\mathrm e^{3}+ 9\mathrm e^{-2}}\\
    \end{align*}
    $$


Pour $t\in\mathbb R$

??? question "**58.1.** $A = (\mathrm e^t - 1)(\mathrm e^t + 1)$"
    $$
    \begin{align*}
    A &= (\mathrm e^t - 1)(\mathrm e^t + 1)\\
    A &= (\mathrm e^t)^2 - 1^2\\
    & \boxed{A = \mathrm e^{2t} - 1}\\
    \end{align*}
    $$

??? question "**58.2.** $B = (\mathrm e^t +3)^2$"
    $$
    \begin{align*}
    B &= (\mathrm e^t +3)^2\\
    B &= (\mathrm e^t)^2 +2×\mathrm e^t×3+ 3^2\\
    & \boxed{B = \mathrm e^{2t} +6\mathrm e^t +9}\\
    \end{align*}
    $$

??? question "**58.3.** $C = (\mathrm e^{2t} -2)^2$"
    $$
    \begin{align*}
    C &= (\mathrm e^{2t} -2)^2\\
    C &= (\mathrm e^{2t})^2 -2×\mathrm e^{2t}×2+ 2^2\\
    C &= \mathrm e^{2t×2} -2×2×\mathrm e^{2t}+ 4\\
    & \boxed{C = \mathrm e^{4t} -4\mathrm e^{2t} +4}\\
    \end{align*}
    $$

Pour $x\in\mathbb R$

??? question "**59.1.** $D = (\mathrm e^x +\mathrm e^{-2x})^2$"
    $$
    \begin{align*}
    D &= (\mathrm e^x +\mathrm e^{-2x})^2\\
    D &= (\mathrm e^x)^2 +2×\mathrm e^x × \mathrm e^{-2x} +(\mathrm e^{-2x})^2\\
    D &= \mathrm e^{x×2} +2×\mathrm e^{x-2x} + \mathrm e^{(-2x)×2}\\
    & \boxed{D = \mathrm e^{2x} +2\mathrm e^{-x} + \mathrm e^{-4x}}\\
    \end{align*}
    $$

??? question "**59.2.** $E = (\mathrm e^{3x} -\mathrm e^{5x})^2$"
    $$
    \begin{align*}
    E &= (\mathrm e^{3x} -\mathrm e^{5x})^2\\
    E &= (\mathrm e^{3x})^2 -2×\mathrm e^{3x} × \mathrm e^{5x} +(\mathrm e^{5x})^2\\
    E &= \mathrm e^{3x×2} -2×\mathrm e^{3x+5x} + \mathrm e^{5x×2}\\
    & \boxed{E = \mathrm e^{6x} -2\mathrm e^{8x} + \mathrm e^{10x}}\\
    \end{align*}
    $$

??? question "**59.3.** $F = (\mathrm e^{-2x} -\mathrm e^{x})(\mathrm e^{-2x} +\mathrm e^{x})$"
    $$
    \begin{align*}
    F &= (\mathrm e^{-2x} -\mathrm e^{x})(\mathrm e^{-2x} +\mathrm e^{x})\\
    F &= (\mathrm e^{-2x})^2 -(\mathrm e^{x})^2\\
    F &= \mathrm e^{-2x×2} - \mathrm e^{x×2}\\
    & \boxed{F = \mathrm e^{-4x} - \mathrm e^{2x}}\\
    \end{align*}
    $$

??? question "**60.1.** $O(x) = (\mathrm e^{x} +\mathrm e^{-x})^2 + (\mathrm e^{x} -\mathrm e^{-x})^2$"
    $$
    \begin{align*}
    O(x) &= (\mathrm e^{x} +\mathrm e^{-x})^2 + (\mathrm e^{x} -\mathrm e^{-x})^2\\
    O(x) &= ( (\mathrm e^{x})^2 +2\mathrm e^{x}\mathrm e^{-x} +(\mathrm e^{-x})^2) + ( (\mathrm e^{x})^2 -2\mathrm e^{x}\mathrm e^{-x} +(\mathrm e^{-x})^2)\\
    O(x) &= ( \mathrm e^{2x} +2 +\mathrm e^{-2x}) + ( \mathrm e^{2x} -2 +\mathrm e^{-2x})\\
    & \boxed{O(x) = 2(\mathrm e^{2x} + \mathrm e^{-2x})}\\
    \end{align*}
    $$

??? question "**60.2.** $P(x) = (\mathrm e^{x} +\mathrm e^{-x})^2 - (\mathrm e^{x} -\mathrm e^{-x})^2$"
    $$
    \begin{align*}
    P(x) &= (\mathrm e^{x} +\mathrm e^{-x})^2 - (\mathrm e^{x} -\mathrm e^{-x})^2\\
    P(x) &= ( (\mathrm e^{x})^2 +2\mathrm e^{x}\mathrm e^{-x} +(\mathrm e^{-x})^2) - ( (\mathrm e^{x})^2 -2\mathrm e^{x}\mathrm e^{-x} +(\mathrm e^{-x})^2)\\
    P(x) &= ( \mathrm e^{2x} +2 +\mathrm e^{-2x}) - ( \mathrm e^{2x} -2 +\mathrm e^{-2x})\\
    & \boxed{P(x) = 4}\\
    \end{align*}
    $$

### 61, 62 : Démontrer une égalité

!!! tip "Indices possibles"
    - Développer et réduire chaque membre indépendamment pour justifier l'égalité.
    - Pour l'égalité entre deux quotients, on a : $\left(\frac ab = \frac cd\right) \iff (b\neq 0, d\neq 0, ad = bc)$


??? question "**61.1.** $\frac{\mathrm e^{x}-1}{\mathrm e^{x}} = 1 - \mathrm e^{-x}$, pour $x\in\mathbb R$"
    On va développer et réduire le membre de gauche $G$ et le membre de droite $D$.

    $$
    \begin{align*}
    G &= \frac{\mathrm e^{x}-1}{\mathrm e^{x}}\\
    G &= \frac{\mathrm e^{x}}{\mathrm e^{x}} -\frac{1}{\mathrm e^{x}}\\
    G &= 1 - \mathrm e^{-x}\\
    &\text{puis}\\
    D &= 1 - \mathrm e^{-x}\\
    &\text{ainsi}\\
    &\boxed{G=D}
    \end{align*}
    $$

!!! warning "Erreur d'énoncé dans le livre"
    L'égalité du **61.2** est valable pour $x\in\mathbb R^*$

    Sinon, on aurait $\frac12 = \frac00$ qui est fausse !
??? question "**61.2.** $\frac{1}{\mathrm e^{x}+1} = \frac{\mathrm e^{x}-1}{\mathrm e^{2x}-1}$, pour $x\in\mathbb R^*$"
    On regarde l'égalité des produits en croix $A$ et $B$, et on vérifie avant les dénominateurs !

    - Pour $x\in\mathbb R^*, \mathrm e^{x}+1 \neq 0$
    - Pour $x\in\mathbb R^*, \mathrm e^{2x}-1 \neq 0$ ; ici $x\neq 0$ est **essentiel** !!!

    $$
    \begin{align*}
    A &= 1×(\mathrm e^{x}+1)\\
    A &= \mathrm e^{x}+1\\
    B &= (\mathrm e^{x}+1)(\mathrm e^{x}-1)\\
    &\text{puis}\\
    B &= (\mathrm e^{x})^2 - 1^1\\
    B &= \mathrm e^{2x} - 1\\
    &\text{ainsi}\\
    &\boxed{A=B}
    \end{align*}
    $$


??? question "**62.** $(\mathrm e^{x}+\mathrm e^{-x})(\mathrm e^{2x})^2 = \mathrm e^{3x}(\mathrm e^{2x}+1)$, pour $x\in\mathbb R$"
    On va développer et réduire le membre de gauche $G$ et le membre de droite $D$.

    $$
    \begin{align*}
    G &= (\mathrm e^{x}+\mathrm e^{-x})(\mathrm e^{2x})^2\\
    G &= (\mathrm e^{x}+\mathrm e^{-x})×\mathrm e^{4x}\\
    G &= \mathrm e^{x}×\mathrm e^{4x}+\mathrm e^{-x}×\mathrm e^{4x}\\
    G &= \mathrm e^{x+4x}+\mathrm e^{-x+4x}\\
    G &= \mathrm e^{5x}+\mathrm e^{3x}\\
    &\text{puis}\\
    D &= \mathrm e^{3x}(\mathrm e^{2x}+1)\\
    D &= \mathrm e^{3x}×\mathrm e^{2x}+\mathrm e^{3x}×1\\
    D &= \mathrm e^{3x+2x}+\mathrm e^{3x}\\
    D &= \mathrm e^{5x}+\mathrm e^{3x}\\
    &\text{ainsi}\\
    &\boxed{G=D}
    \end{align*}
    $$

### 63 : Factoriser les expressions

Soit $x\in\mathbb R$

??? question "$A = \mathrm e^{4x} + \mathrm e^{x}$"
    $$
    \begin{align*}
    A &= \mathrm e^{4x} + \mathrm e^{x}\\
    A &= \mathrm e^{x}×\mathrm e^{3x} + \mathrm e^{x}×1\\
    & \boxed{A = \mathrm e^{x}(\mathrm e^{3x} + 1)}\\
    \end{align*}
    $$
