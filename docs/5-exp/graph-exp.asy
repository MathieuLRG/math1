import graph;

size (8cm, 0);

real f(real x) {return exp(x);}
path Cf=graph(f, -4.8, 1.6);
draw(Cf);

xaxis(Label("$x$",position=EndPoint, align=NE),
      Ticks(scale(.7)*Label(),NoZero,Size=.8mm, size=.4mm),
      Arrow);
yaxis(Label("$y$",position=EndPoint, align=NW),
      Ticks(scale(.7)*Label(align=E),NoZero,Size=.8mm, size=.4mm),
      Arrow);

pair pA = (0, 1);
pair pB = (1, exp(1));
pair pBx = (1, 0);
pair pBy = (0, exp(1));

dot(pA, blue);
dot(pB, blue);
label("$\mathrm e$", pBy, 3W);
label("$0$", (0, 0), SW);

draw (pB--pBx, dotted + blue + 0.5bp);
draw (pB--pBy, dotted + blue + 0.5bp);

draw((-1.5, -0.5)--(1.5, 2.5), dashed + blue + 0.5bp);

label("$y = 1 + x$", (1.5, 2), E, blue);
label("$y = \mathrm e^x$", (1.5, 4), E);
