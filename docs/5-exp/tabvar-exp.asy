size (8cm, 0);

real x1=0, x2=2, x3=6, x4=9, x5=13;
real y1=0, y2=-1, y3=-2, y4=-6;
real eps = 0.2;
draw((x1, y1)--(x5, y1)--(x5, y4)--(x1, y4)--cycle);
draw((x1, y2)--(x5, y2));
draw((x1, y3)--(x5, y3));
draw((x2, y1)--(x2, y4));

label("$x$", ((x1+x2)/2, (y1+y2)/2));
label("$\mathrm{exp}(x)$", ((x1+x2)/2, (y2+y3)/2));
label("$\mathrm{exp}$", ((x1+x2)/2, (y3+y4)/2));

label("$-\infty$",(x2, (y1+y2)/2), E);
label("$0$",(x3, (y1+y2)/2));
label("$1$",(x4, (y1+y2)/2));
label("$+\infty$",(x5, (y1+y2)/2), W);

label("$+$",((x3+x4)/2, (y2+y3)/2));

draw((x2+eps, y4+eps)--(x5-eps, y3-eps), black, Arrow(HookHead));
label("$1$", (x3, -4.5), N);
label("$\mathrm e$", (x4, -3.5), N);