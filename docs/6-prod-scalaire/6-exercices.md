# Exercices

## Applications directes

### 18, 19 : Vrai ou Faux

**18.1** Si $\vec u \cdot \vec v= 0$ alors $\vec u = \vec 0$ ou $\vec v = \vec 0$

??? success "Réponse"
    **Faux**, il suffit d'avoir deux vecteurs orthogonaux, par exemple : avec $\vec u = \binom 10$ et $\vec v = \binom 01$, on a 

    $$\vec u \cdot \vec v= 1×0+0×1 = 0$$

    or les deux cevteurs $\vec u$ et $\vec v$ sont non nuls.

**18.2** Si les normes de deux vecteurs sont des nombres entiers, alors leur produit scalaire est aussi un nombre entier.

??? success "Réponse"
    **Faux**, avec deux vecteurs de norme $1$, on a $\vec u \cdot \vec v = \cos(\vec u\,,\vec v)$ qui peut être non entier, comme lorsque l'angle formé vaut $\frac{\pi}4$ et dont le cosinus vaut $\frac{\sqrt2}2$ qui est irrationnel.

**19.1** Si $\overrightarrow {AB} \cdot \overrightarrow {CD} = 0$, alors $\overrightarrow {BA} \cdot \overrightarrow {CD} = 0$

??? success "Réponse"
    **Vrai**, on a $\overrightarrow {BA} \cdot \overrightarrow {CD} = (-\overrightarrow {AB}) \cdot \overrightarrow {CD} = -(\overrightarrow {AB} \cdot \overrightarrow {CD}) = -0 = 0$

## 23 à 27 : Calculer $\overrightarrow {AB} \cdot \overrightarrow {AC}$

**23.1** Avec $AB = 1$, $AC=5$, et $\left(\overrightarrow {AB}\,,\overrightarrow {AC}\right) = 40°$

??? success "Réponse"
    On a $\overrightarrow {AB} \cdot \overrightarrow {AC} = 1 × 5 × \cos(40°) \approx +3.83$

!!! info "**Pour l'exercice 25**"
    $H$ est le projeté orthogonal du point $C$ sur la droite $(AB)$.

**25.1** Avec $AB = 4$, $AH=3$, et $H\in[AB)$

??? success "Réponse"
    On a $\overrightarrow {AB} \cdot \overrightarrow {AC} = 4 × 3 × (+1) = +12$


**26.1** Avec $\overrightarrow {AB} = \binom{0}{-2}$ et $\overrightarrow {AC} = \binom{5}{-1}$

??? success "Réponse"
    On a $\overrightarrow {AB} \cdot \overrightarrow {AC} = 0×5 + (-2)×(-1) = 0+2 = +2$


**27.1** Avec $AB = 3$, $AC = 4$ et $BC=6$

??? success "Réponse"
    On a $\overrightarrow {AB} \cdot \overrightarrow {AC} = \frac12 (AB^2 + AC^2 - BC^2) = \frac12(3^2+4^2-6^2)=\frac12(9+16-36)=-5.5$

## 28 : Simplifier

??? question "$(3\vec u)\cdot(2\vec v)$"
    On a $(3\vec u)\cdot(2\vec v) = 6 \vec u \cdot \vec v$


## 29 : calculer des produits scalaires

$ABCD$ est un carré de centre $O$ et de côté $c$.

??? question "$\overrightarrow {AB} \cdot \overrightarrow {AC}$"
    On utilise le projeté orthogonal de $C$ sur $(AB)$, on obtient :

    $$\overrightarrow {AB} \cdot \overrightarrow {AC} = \overrightarrow {AB} \cdot \overrightarrow {AB} = c×c×(+1) = +c^2$$

## 30 : Démonstration

!!! tip "Indice"
    Pour montrer que deux droites sont perpendiculaires, on peut montrer qu'un produit scalaire est nul.


## 31 : Équation de droite

Déterminer une équation cartésienne de la droite $D$ passant par $A$ et de vecteur normal $\vec n$.

??? question "Avec $A(2\,;-1)$ et $\vec n=\binom{3}{-2}$"
    Une équation sera de la forme $\mathcal D : ax+by+c = 0$, avec $a=3$ et $b=-2$ d'après le vecteur normal donné. Ainsi

    $$\mathcal D : 3x-2y+c = 0$$

    $\mathcal D$ passe par $A(2\,;-1)$, donc on a $3×2-2×(-1) + c = 0$, d'où on tire $c = -(6+1) = -7$, et enfin

    $$\mathcal D : 3x-2y-7 = 0$$

## Formules

### 42 : Python

Écrire une fonction `norme` qui prend en paramètre les coordonnées `x, y` d'un vecteur et qui renvoie la norme de ce vecteur.

??? success "Réponse"
    ```python
    from math import sqrt
    def norme(x, y):
        return sqrt(x * x + y * y)
    ```

Écrire une fonction `produit_scalaire` qui prend les coordonnées `x, y, x_, y_` de deux vecteurs et qui renvoie le produit scalaire des deux vecteurs.

??? success "Réponse"
    ```python
    def produit_scalaire(x, y, x_, y_):
        return x * x_ + y * y_
    ```


### 43 : Calculs

Avec $\vec u = \binom{1-\sqrt2}{1}$ et $\vec v = \binom{1+\sqrt2}{-4}$

??? question "Calculer $\vec u \cdot \vec v$"
    $$\begin{align*}
    \vec u \cdot \vec v &= (1-\sqrt2)×(1+\sqrt2) + 1×(-4)\\
    \vec u \cdot \vec v &= \left(1^2-(\sqrt2)^2\right)+(-4)\\
    \vec u \cdot \vec v &= 1-2-4\\
    \vec u \cdot \vec v &= -5\\
    \end{align*}
    $$

??? question "Calculer $(3\vec u) \cdot \vec v$"
    $(3\vec u) \cdot \vec v = 3(\vec u \cdot \vec v) = 3×(-5) = -15$

??? question "Calculer $(4\vec u) \cdot (-2\vec v)$"
    $(4\vec u) \cdot (-2\vec v) = 4×(-2)×(\vec u \cdot \vec v) = 4×(-2)×(-5) = +40$

??? question "Calculer $\vec u \cdot (\sqrt 2\vec u -\vec v)$"
    On a $\sqrt 2\vec u = \binom{\sqrt 2(1-\sqrt 2)}{\sqrt 2 \times 1} = \binom{\sqrt 2 - 2}{\sqrt 2}$

    D'où $\sqrt 2\vec u -\vec v = \binom{(\sqrt 2 - 2) - (1+\sqrt 2)}{\sqrt 2 - \sqrt 2} = \binom{-3}{0}$

    Ainsi $\vec u \cdot (\sqrt 2\vec u -\vec v) = (1-\sqrt 2)\times(-3) + 1\times 0 = 3\sqrt2 - 3$

### 44 : Avec un triangle équilatéral

$ABC$ est un triangle équilatéral dont les côtés mesurent $2~\text{cm}$.

$I$ est le pied de la hauteur issue de $A$.

??? question "Calculer $\overrightarrow {BC} \cdot \overrightarrow {BA}$"
    $\overrightarrow {BC} \cdot \overrightarrow {BA} = \overrightarrow {BC} \cdot \overrightarrow {BI} = 2×1×(+1) = +2$

??? question "Calculer $\overrightarrow {BA} \cdot \overrightarrow {BI}$"
    $\overrightarrow {BA} \cdot \overrightarrow {BI} = \overrightarrow {BI} \cdot \overrightarrow {BI} = 1×1×(+1) = +1$

??? question "Calculer $\overrightarrow {AI} \cdot \overrightarrow {AC}$"
    $\overrightarrow {AI} \cdot \overrightarrow {AC} = \overrightarrow {AI} \cdot \overrightarrow {AI} = \sqrt 3×\sqrt 3×(+1) = +3$

    Variante

    $\overrightarrow {AI} \cdot \overrightarrow {AC} =(\overrightarrow {AC}+\overrightarrow {CI}) \cdot \overrightarrow {AC} = (\overrightarrow {AC} \cdot \overrightarrow {AC}) + (\overrightarrow {CI} \cdot \overrightarrow {AC}) = 2×2×(+1) + (\overrightarrow {CI} \cdot \overrightarrow {IC}) = 4 + 1×1×(-1) = +3$

## Vecteur normal

### 74 : Démonstration

On considère $ABC$ un triangle avec $A(4\,;-3)$, $B(5\,;3)$ et $C(-2\,;3)$

??? question "Déterminer une équation de la hauteur issue de $A$"
    La hauteur issue de $A$ est perpendiculaire à $BC$, donc un vecteur normal est $\overrightarrow {BC} = \binom{-2-5}{3-3} = \binom{-7}{0}$, une équation cartésienne est donc $-7x+0y+c=0$.

    Or cette hauteur passe par $A(4\,;-3)$, on déduit

    $-7×4+0×(-3) + c = 0$, d'où $c=-28$, et une équation de la hauteur étant $\mathcal H_A: -7x-28=0$, ou bien  $\mathcal H_A: x+4=0$

... TODO