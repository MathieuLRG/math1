# Compléments de cours

## Liens utiles

Pour la spécialité **mathématiques** en première

```markmap

# Spé Maths 1ère

## par M. Chambon : Compléments et exercices

### [Suites numériques](./1-suites/1-def/)

### [Second degré](./2-degre2/1-def/)

### [Trigonométrie](./3-trigo/1-def/)

### [Dérivation](./4-deriv/1-formules/)

### [Exponentielle](./5-exp/1-def/)

### [Produit scalaire](./6-prod-scalaire/1-def/)

## Cours et exercices du manuel

### [Le livre scolaire](https://www.lelivrescolaire.fr/page/6826700)

## Auto évaluation

### [H5P : Équations variées](#automatismes)

### [Pyromaths](https://www.pyromaths.org/)

## par M. Chambon : autour de Python

### [Le cours sur Python](https://ens-fr.gitlab.io/algo1/)

### [Exercices, questions Flash](https://ens-fr.gitlab.io/flash/)

## Calculatrice

### [NumWorks en version locale](./numworks/simulator.html)

```

## Automatismes

!!! info "Les automatismes pour les équations"
    Il est très important de savoir résoudre rapidement les équations simples. Cela donne une aisance pour aborder les calculs plus complexes. Les 3/4 premiers se font de tête, les derniers se font avec une feuille de brouillon pour aider.

    1. [Équations](./h5p/eq_N1.html) de la forme $ax=b$, sans fractions
    2. [Équations](./h5p/eq_N1f.html) de la forme $ax=b$, **avec** fractions :warning:
    3. [Équations](./h5p/eq_N2.html) de la forme $ax+b=c$, sans fractions
    4. [Équations](./h5p/eq_N2f.html) de la forme $ax+b=c$, **avec** fractions :warning:
    5. [Équations](./h5p/eq_N3.html) de la forme $ax+b=cx+d$, sans fractions
    6. [Équations](./h5p/eq_N3f.html) de la forme $ax+b=cx+d$, **avec** fractions :warning:

!!! info "Les automatismes, avec Pyromaths"
    Il est très important pour tous les élèves, surtout ceux qui ont du mal à mener un calcul, ou qui ont d'autres lacunes, de travailler les automatismes.
    
    Avec Pyromaths, vous pouvez vous entrainer en toute autonomie, un corrigé détaillé est disponible. Vous pouvez recommencer les mêmes exercices, le sujet et le corrigé seront automatiquement modifiés avec de nouveaux nombres.

    Les élèves en grande difficulté peuvent aussi donc revoir les bases du collège.



!!! cite "Culture mathématique et scientifique"

    - <https://scienceetonnante.com/>
    - <https://www.3blue1brown.com/>


    ![polygones](assets/régulier.gif)

!!! question "Amusette"
    Quelle est la hauteur de la table ?

    ![](./assets/table.png){ .bordure }

    ??? success "Réponse"
        Avec des notations évidentes, on a les équations :

        - `table + chat - tortue = 170`
        - `table + tortue - chat = 130`

        En additionnant les deux lignes, on obtient :

        - `2 * table = 170 + 130`

        D'où `table = 300 / 2 = 150`

        La table fait 150 cm de haut.


        
